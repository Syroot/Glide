# Glide

This repository hosts a .NET 2.0-based wrapper of the 3Dfx Glide APIs 2 and 3, written in C#, and supporting down to
Windows 98.

The project exists out of pure experimentation interest as running .NET applications would normally be an unnecessary
resource hog to systems which support real 3Dfx hardware. However, software-based 3Dfx wrappers like nGlide can also be
accessed with this. In fact, the initial authors do not own real 3Dfx hardware and test against nGlide and an 86Box
emulated machine.

## Library

The wrapper library maps native symbols as raw as possible, with only the following changes:
- .NET naming style is applied (e.g. removing method prefixes or changing snake cased parameter names).
- Official documentation is ported and adjusted to match .NET XML summary style, and inapplicable info is removed.
- If useful, enums are used instead of primitive types to lower the chance of passing invalid values.
- Safe overloads are provided for method signatures which would require unsafe code otherwise.
- Some typedefs are used as type aliases. They do not affect logic or documentation, but simplify comparing signatures.

## Apps

A test project includes the original SDK samples, ported to C#.