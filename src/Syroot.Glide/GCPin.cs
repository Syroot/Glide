﻿using System;
using System.Runtime.InteropServices;

namespace Syroot.Glide
{
    /// <summary>
    /// Represents a pinned <see cref="GCHandle"/> which is automatically freed on disposal.
    /// </summary>
    public sealed class GCPin : IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly GCHandle _handle;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="GCPin"/> class, pinning the given <paramref name="value"/>.
        /// </summary>
        /// <param name="value">The object that will be pinned.</param>
        public GCPin(object value) => _handle = GCHandle.Alloc(value, GCHandleType.Pinned);

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Implicityl returns address of the <paramref name="value"/>.
        /// </summary>
        /// <param name="value">The pinned handle.</param>
        public static implicit operator IntPtr(GCPin value) => value._handle.AddrOfPinnedObject();

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the address of the pinned handle.
        /// </summary>
        public IntPtr Address => _handle.AddrOfPinnedObject();

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public void Dispose() => _handle.Free();
    }
}
