﻿namespace Syroot.Glide.Glide3
{
    /// <summary>
    /// Represents parameters which can be queried by <see cref="Gr.Get"/>.
    /// </summary>
    public enum ParamName : uint
    {
        BitsDepth = 1,
        BitsRgba,
        BitsGamma,
        FifoFullness,
        FogTableEntries,
        GammaTableEntries,
        GlideStateSize,
        GlideVertexLayoutSize,
        IsBusy,
        LfbPixelPipe,
        MaxTextureSize,
        MaxTextureAspectRatio,
        MemoryFb,
        MemoryTmu,
        MemoryUma,
        NonPowerOfTwoTextures,
        NumBoards,
        NumFb,
        NumSwapHistoryBuffer,
        NumTmu,
        PendingBufferSwaps,
        RevisionFb,
        RevisionTmu,
        StatsLines,
        StatsPixelsAFuncFail,
        StatsPixelsChromaFail,
        StatsPixelsDepthFuncFail,
        StatsPixelsIn,
        StatsPixelsOut,
        StatsPoints,
        StatsTrianglesIn,
        StatsTrianglesOut,
        SwapHistory,
        SupportsPassthru,
        TextureAlign,
        VideoPosition,
        Viewport,
        WDepthMinMax,
        ZDepthMinMax
    }
}
