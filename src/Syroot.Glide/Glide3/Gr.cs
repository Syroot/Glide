﻿using System;
using System.Runtime.InteropServices;
using Context = System.UInt32;

namespace Syroot.Glide.Glide3
{
    /// <summary>
    /// Represents raw P/Invoke access to the graphics API of the glide3x library.
    /// </summary>
    public static class Gr
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Returns Glide state.
        /// </summary>
        /// <param name="paramName">Encoded selectors for the environmental parameters to be returned.</param>
        /// <param name="paramLength">Length of the return buffer, in bytes.</param>
        /// <param name="paramValue">Pointer to a buffer in which the parameters will be returned.</param>
        /// <remarks>
        /// This method retrieves the values of selected Glide state variables.
        /// </remarks>
        /// <returns>
        /// If successful, it returns the number of bytes written into the <paramref name="paramValue"/> buffer. If it
        /// fails (invalid context, invalid <paramref name="paramName"/> or <see langword="null"/>
        /// <paramref name="paramValue"/>), <c>0</c> is returned and the contents of the <paramref name="paramValue"/>
        /// array is unchanged.
        /// </returns>
        /// <seealso cref="Enable"/>
        /// <seealso cref="GetProcAddress"/>
        /// <seealso cref="GetString"/>
        [DllImport("glide3x", EntryPoint = "_grGet@12")]
        public static extern unsafe uint Get(ParamName paramName, uint paramLength, int* paramValue);
        /// <summary>
        /// Returns Glide state.
        /// </summary>
        /// <param name="paramName">Encoded selectors for the environmental parameters to be returned.</param>
        /// <param name="paramLength">Length of the return buffer, in bytes.</param>
        /// <param name="paramValue">Pointer to a buffer in which the parameters will be returned.</param>
        /// <remarks>
        /// This method retrieves the values of selected Glide state variables.
        /// </remarks>
        /// <returns>
        /// If successful, it returns the number of bytes written into the <paramref name="paramValue"/> buffer. If it
        /// fails (invalid context, invalid <paramref name="paramName"/> or <see langword="null"/>
        /// <paramref name="paramValue"/>), <c>0</c> is returned and the contents of the <paramref name="paramValue"/>
        /// array is unchanged.
        /// </returns>
        /// <seealso cref="Enable"/>
        /// <seealso cref="GetProcAddress"/>
        /// <seealso cref="GetString"/>
        [DllImport("glide3x", EntryPoint = "_grGet@12")]
        public static extern uint Get(ParamName paramName, uint paramLength, IntPtr paramValue);
        /// <summary>
        /// Returns Glide state.
        /// </summary>
        /// <param name="paramName">Encoded selectors for the environmental parameters to be returned.</param>
        /// <remarks>
        /// This method retrieves the values of selected Glide state variables.
        /// </remarks>
        /// <returns>
        /// If successful, it returns the buffer in which the parameters will be stored. If it fails (invalid context,
        /// invalid <paramref name="paramName"/>, an empty array is returned.
        /// </returns>
        /// <seealso cref="Enable"/>
        /// <seealso cref="GetProcAddress"/>
        /// <seealso cref="GetString"/>
        public static int[] Get(ParamName paramName)
        {
            const uint maxParams = 4;
            const uint bufferLength = maxParams * sizeof(int);

            int[] buffer = new int[maxParams];
            uint bytesWritten;
            using (GCPin bufferPin = new GCPin(buffer))
                bytesWritten = Get(paramName, bufferLength, bufferPin);

            if (bytesWritten == 0)
            {
                return new int[0];
            }
            else
            {
                int[] paramValue = new int[bytesWritten / sizeof(int)];
                Buffer.BlockCopy(buffer, 0, paramValue, 0, (int)bytesWritten);
                return paramValue;
            }
        }

        /// <summary>
        /// Initialize the Glide library.
        /// </summary>
        /// <remarks>
        /// This method initializes the Glide library, performing tasks such as finding any installed graphics
        /// subsystems, allocating memory, and initializing state variables. It must be called before any other Glide
        /// routines are called (the one exception being <see cref="Get"/> called with
        /// <see cref="ParamName.NumBoards"/>).
        /// </remarks>
        /// <see cref="Get"/>
        /// <see cref="GlideShutdown"/>
        /// <see cref="SstWinOpen"/>
        /// <see cref="SstSelect"/>
        [DllImport("glide3x", EntryPoint = "_grGlideInit@0")]
        public static extern void GlideInit();

        /// <summary>
        /// Shut down the Glide library.
        /// </summary>
        /// <remarks>
        /// This method frees up any system resources allocated by Glide, including memory, and interrupt vectors.
        /// It should be called immediately before program termination.
        /// </remarks>
        /// <seealso cref="GlideInit"/>
        [DllImport("glide3x", EntryPoint = "_grGlideShutdown@0")]
        public static extern void GlideShutdown();

        /// <summary>
        /// Query for possible frame buffer configurations.
        /// </summary>
        /// <param name="resTemplate">Template containing query constraints.</param>
        /// <param name="output">Pointer to an array of <see cref="ScreenResolution"/> structures that will receive the
        /// resolution information.</param>
        /// <remarks>
        /// This method returns all available frame buffer configurations that match the constraints specified in the
        /// <paramref name="resTemplate"/>. The constraints are specified as either <see cref="Resolution.None"/>
        /// / <see cref="Refresh.None"/> / <c>-1</c> or a specific value in each of the four fields in the
        /// <see cref="ScreenResolution"/> structure.
        /// </remarks>
        /// <returns>
        /// The number of bytes required to contain the available resolution information. Typically, a Glide application
        /// program calls this method with a <c>0</c> <paramref name="output"/> parameter initially and uses the return 
        /// value to allocate space, then calls the method again to return the information.
        /// </returns>
        /// <seealso cref="Get"/>
        /// <seealso cref="GetString"/>
        /// <seealso cref="SstWinOpen"/>
        [DllImport("glide3x", EntryPoint = "_grQueryResolutions@8")]
        public static extern unsafe int QueryResolutions(ScreenResolution* resTemplate, ScreenResolution* output);
        /// <summary>
        /// Query for possible frame buffer configurations.
        /// </summary>
        /// <param name="resTemplate">Template containing query constraints.</param>
        /// <param name="output">Pointer to an array of <see cref="ScreenResolution"/> structures that will receive the
        /// resolution information.</param>
        /// <remarks>
        /// This method returns all available frame buffer configurations that match the constraints specified in the
        /// <paramref name="resTemplate"/>. The constraints are specified as either <see cref="Resolution.None"/>
        /// / <see cref="Refresh.None"/> / <c>-1</c> or a specific value in each of the four fields in the
        /// <see cref="ScreenResolution"/> structure.
        /// </remarks>
        /// <returns>
        /// The number of bytes required to contain the available resolution information. Typically, a Glide application
        /// program calls this method with a <see cref="IntPtr.Zero"/> <paramref name="output"/> parameter initially and
        /// uses the return value to allocate space, then calls the method again to return the information.
        /// </returns>
        /// <seealso cref="Get"/>
        /// <seealso cref="GetString"/>
        /// <seealso cref="SstWinOpen"/>
        [DllImport("glide3x", EntryPoint = "_grQueryResolutions@8")]
        public static extern int QueryResolutions(IntPtr resTemplate, IntPtr output);
        /// <summary>
        /// Query for possible frame buffer configurations.
        /// </summary>
        /// <param name="resTemplate">Template containing query constraints.</param>
        /// <remarks>
        /// This method returns all available frame buffer configurations that match the constraints specified in the
        /// <paramref name="resTemplate"/>. The constraints are specified as either <see cref="Resolution.None"/>
        /// / <see cref="Refresh.None"/> / <c>-1</c> or a specific value in each of the four fields in the
        /// <see cref="ScreenResolution"/> structure.
        /// </remarks>
        /// <returns>
        /// The array of <see cref="ScreenResolution"/> structures that will contain the available resolution information.
        /// </returns>
        /// <seealso cref="Get"/>
        /// <seealso cref="GetString"/>
        /// <seealso cref="SstWinOpen"/>
        public static ScreenResolution[] QueryResolutions(ScreenResolution resTemplate)
        {
            using GCPin resTemplatePin = new GCPin(resTemplate);
            int listSize = QueryResolutions(resTemplatePin, IntPtr.Zero);
            ScreenResolution[] list = new ScreenResolution[listSize / Marshal.SizeOf(typeof(ScreenResolution))];
            if (list.Length == 0)
            {
                return list;
            }
            else
            {
                using GCPin listPin = new GCPin(list);
                QueryResolutions(resTemplatePin, listPin);
                return list;
            }
        }

        /// <summary>
        /// Activate a context.
        /// </summary>
        /// <param name="context">A context handle.</param>
        /// <remarks>
        /// To gracefully handle the loss of resources (e.g. to another 3D application being scheduled by the Windows
        /// operating system), an application is required to periodically (typically once per frame) query with this
        /// method to determine if Glide's resources have been reallocated by the system. <paramref name="context"/> is
        /// a context handle returned from a successful call to <see cref="SstWinOpen"/>.
        /// </remarks>
        /// <returns>
        /// If none of the rendering context's state and resources have been disturbed since the last call, this method
        /// returns <see langword="true"/>. In this case no special actions by the application are required. If it
        /// returns <see langword="false"/>, then the application must assume that the rendering state has changed and
        /// must be reestablished (by re-downloading textures, explicitly resetting the rendering state, etc.) before
        /// further rendering commands are issued.
        /// </returns>
        /// <seealso cref="SstWinOpen"/>
        [DllImport("glide3x", EntryPoint = "_grSelectContext@4")]
        public static extern bool SelectContext(Context context);

        /// <summary>
        /// Make a graphics subsystem current.
        /// </summary>
        /// <param name="whichSst">The ordinal number of the graphics subsystem to make current. This value must be
        /// between <c>0</c> and the number of installed subsystems returned by <see cref="Get"/> called with
        /// <see cref="ParamName.NumBoards"/>.</param>
        /// <remarks>
        /// This method selects a particular installed graphics subsystem as active. If the value passed is greater than
        /// the number of installed graphics subsystems, the behavior will be undefined.
        /// </remarks>
        /// <seealso cref="Get"/>
        /// <seealso cref="SstWinOpen"/>
        [DllImport("glide3x", EntryPoint = "_grSstSelect@4")]
        public static extern void SstSelect(int whichSst);

        /// <summary>
        /// Close a graphics context.
        /// </summary>
        /// <param name="context">The graphics context to close.</param>
        /// <returns>
        /// The method returns <see langword="false"/> if <paramref name="context"/> is not a valid handle to a graphics
        /// context. Otherwise, it resets the state of Glide to the one following <see cref="GlideInit"/>, so that
        /// <see cref="SstWinOpen"/> can be called to open a new context.
        /// </returns>
        /// <seealso cref="SstWinOpen"/>
        [DllImport("glide3x", EntryPoint = "_grSstWinClose@4")]
        public static extern bool SstWinClose(Context context);

        /// <summary>
        /// Opens the graphics display device.
        /// </summary>
        /// <param name="hWnd">Specifies a handle to the window. The interpretation of this value depends on the system
        /// environment. Applications run on Voodoo Graphics or Voodoo² hardware must specifiy <c>0</c> as well. Win32
        /// full screen applications running on Voodoo Rush system must specify a window handle; a <c>0</c> value will
        /// cause the application's real window handle (i.e. what is returned by <c>GetActiveWindow</c>) to be used.
        /// Since Win32 pure console applications do not have a window handle, they can be used only with Voodoo
        /// Graphics or Voodoo² systems and a <c>0</c> window handle is required.</param>
        /// <param name="resolution">Specifies which screen resolution to use. Refer to
        /// <see cref="Resolution"/> for available video resolutions, e.g.,
        /// <see cref="Resolution.Res640x480"/> and <see cref="Resolution.Res800x600"/>. In addition,
        /// the resolution <see cref="Resolution.None"/> is permitted (but not recommended) for Voodoo Rush.
        /// This signals Glide to use the user specified window (see the <paramref name="hWnd"/>). Specifying
        /// <see cref="Resolution.None"/> on a Voodoo Graphics or Voodoo² system will cause the call to fail.</param>
        /// <param name="refresh">Specifies the refresh rate to use. Refer to <see cref="Refresh"/> for
        /// available video refresh rates, e.g., <see cref="Refresh.Ref60Hz"/> and
        /// <see cref="Refresh.Ref72Hz"/>. The parameter is ignored when a Win32 application is running in a
        /// window (Voodoo Rush systems only).</param>
        /// <param name="colorFormat">Specifies the packed color RGBA ordering for linaer frame buffer writes and
        /// parameters for the following APIs: <see cref="BufferClear"/>, <see cref="ChromakeyValue"/>,
        /// <see cref="ConstantColorValue"/>, <see cref="FogColorValue"/>.</param>
        /// <param name="originLocation">Specifies the direction of the Y coordinate axis.
        /// <see cref="Origin.UpperLeft"/> places the screen space origin at the upper left corner of the
        /// screen with positive Y going down (a la IBM VGA). <see cref="Origin.LowerLeft"/> places the screen origin at
        /// the lower left corner of the screen with positive Y going up (a la SGI GL).</param>
        /// <param name="numBuffers">Specifies the number of rendering buffers to use. Supports values <c>2</c> (double-
        /// buffering) or <c>3</c> (triple-buffering). If there is not enough memory to support the desired resolution
        /// (e.g. 800x600 triple buffered on a 2MB system), an error will occur.</param>
        /// <param name="numAuxBuffers">Specifies the number of auxiliary buffers required by an application. The
        /// auxiliary buffers are used either for depth or alpha buffering. Permitted values are <c>0</c> or <c>1</c>.
        /// For full screen applications, this parameter allows Voodoo Graphics, Voodoo² and Voodoo Rush systems to
        /// validate whether the available video memory will support the application's requirements for color and
        /// auxiliary buffers at a specified resolution. For a windowed application running on Voodoo Rush, this
        /// parameter allows an application to run in a larger 3D window if a depth buffer is not necessary (depth and
        /// back buffers share the same off-screen video memory).</param>
        /// <remarks>
        /// This method initializes the graphics to a known state using the given parameters. It supports Voodoo
        /// graphics, Voodoo², and Voodoo Rush, and either full-screen or windowed operation in the latter. By default
        /// all special effects of the hardware (depth buffering, fog, chroma-key, alpha blending, alpha testing, etc.)
        /// are disabled and must be individually enabled. All global state constants (chroma-key value, alpha test
        /// reference, constant depth value, constant alpha value, etc.) and pixel rendering statistic counters are
        /// initialized to <c>0x00</c>.
        /// </remarks>
        /// <returns>
        /// Upon success, an opaque graphics context handle is returned; otherwise <c>0</c> is returned. If this method
        /// is called on an already open window, <c>0</c> will be returned.
        /// </returns>
        /// <seealso cref="SstWinClose"/>
        [DllImport("glide3x", EntryPoint = "_grSstWinOpen@28")]
        public static extern Context SstWinOpen(uint hWnd, Resolution resolution, Refresh refresh,
            ColorFormat colorFormat, Origin originLocation, int numBuffers, int numAuxBuffers);
    }
}
