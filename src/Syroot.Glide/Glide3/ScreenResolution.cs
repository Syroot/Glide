﻿using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide3
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ScreenResolution
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public Resolution Resolution;
        public Refresh Refresh;
        public int NumColorBuffers;
        public int NumAuxBuffers;
    }
}
