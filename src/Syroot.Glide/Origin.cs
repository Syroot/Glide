﻿namespace Syroot.Glide
{
    /// <summary>
    /// Represents Y origin values.
    /// </summary>
    public enum Origin
    {
        /// <summary>For LFB writes, lock will always choose <see cref="Origin.UpperLeft"/>.</summary>
        Any = -1,
        /// <summary>Addressing originates in the upper left hand corner of the screen.</summary>
        UpperLeft,
        /// <summary>Addressing originates in the lower left hand corner of the screen.</summary>
        LowerLeft
    }
}
