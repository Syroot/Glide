﻿namespace Syroot.Glide
{
    public enum Refresh
    {
        None = -1,
        Ref60Hz,
        Ref70Hz,
        Ref72Hz,
        Ref75Hz,
        Ref80Hz,
        Ref90Hz,
        Ref100Hz,
        Ref85Hz,
        Ref120Hz
    }
}
