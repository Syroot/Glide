﻿namespace Syroot.Glide.Glide2
{
    /// <summary>
    /// Represents the scaling factor <i>f</i> used in source value generation.
    /// </summary>
    public enum CombineFactor
    {
        /// <summary>0</summary>
        Zero,
        /// <summary>X_local / 255</summary>
        Local,
        /// <summary>A_other / 255</summary>
        OtherAlpha,
        /// <summary>A_local / 255</summary>
        LocalAlpha,
        /// <summary>A_texture / 255</summary>
        TextureAlpha,
        LodFraction,
        /// <summary>1</summary>
        One = 8,
        /// <summary>1 - X_local / 255</summary>
        OneMinusLocal,
        /// <summary>1 - A_other / 255</summary>
        OneMinusOtherAlpha,
        /// <summary>1 - A_local / 255</summary>
        OneMinusLocalAlpha,
        /// <summary>1 - A_texture / 255</summary>
        OneMinusTextureAlpha,
        OneMinusLodFraction,
        /// <summary>Unspecified</summary>
        None = Zero,
        /// <summary>beta (s. <see cref="Gr.TexDetailControl"/> for further information)</summary>
        DetailFactor = TextureAlpha,
        /// <summary>1 - beta (s. <see cref="Gr.TexDetailControl"/> for further information)</summary>
        OneMinusDetailFactor = OneMinusTextureAlpha,
    }
}
