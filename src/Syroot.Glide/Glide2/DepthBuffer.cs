﻿namespace Syroot.Glide.Glide2
{
    public enum DepthBuffer
    {
        Disable,
        ZBuffer,
        WBuffer,
        ZBufferCompareToBias,
        WBufferCompareToBias
    }
}
