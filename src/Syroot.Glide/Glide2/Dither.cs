﻿namespace Syroot.Glide.Glide2
{
    /// <summary>
    /// Represents the forms of dithering used when converting 24-bit RGB values to the 16-bit RGB color buffer format.
    /// </summary>
    public enum Dither
    {
        /// <summary>Forces a simple truncation, which may result in noticeable banding.</summary>
        Disable,
        /// <summary>Uses a 2x2 ordered dither matrix.</summary>
        Dither2x2,
        /// <summary>Uses a 4x4 ordered dither matrix.</summary>
        Dither4x4
    }
}
