﻿namespace Syroot.Glide.Glide2
{
    public enum TextureClamp
    {
        Wrap,
        Clamp
    }
}
