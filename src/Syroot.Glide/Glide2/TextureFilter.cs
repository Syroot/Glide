﻿namespace Syroot.Glide.Glide2
{
    /// <summary>
    /// Represents texture filtering modes.
    /// </summary>
    public enum TextureFilter
    {
        PointSampled,
        Bilinear
    }
}
