﻿using System;

namespace Syroot.Glide.Glide2
{
    [Flags]
    public enum MipMapLevelMask
    {
        Even = 1 << 0,
        Odd = 1 << 1,
        Both = Even | Odd
    }
}
