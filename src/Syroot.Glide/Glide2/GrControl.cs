﻿namespace Syroot.Glide.Glide2
{
    public enum GrControl
    {
        /// <summary>Activate 3D display.</summary>
        Activate = 1,
        /// <summary>Activate 2D display.</summary>
        Deactivate,
        /// <summary>Resize back buffers and auxiliary buffers (SST-96 only).</summary>
        Resize,
        /// <summary>Validate location after window move (SST-96 only).</summary>
        Move
    }
}
