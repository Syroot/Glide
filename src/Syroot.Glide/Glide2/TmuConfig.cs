﻿using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2
{
    [StructLayout(LayoutKind.Sequential)]
    public struct TmuConfig
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        /// <summary>Rev o Texelfx chip.</summary>
        public int TmuRev;
        /// <summary>1, 2, or 4 MB.</summary>
        public int TmuRam;
    }
}
