﻿namespace Syroot.Glide.Glide2
{
    /// <summary>
    /// Represents types of texture tables.
    /// </summary>
    public enum TexTableType
    {
        /// <summary>Narrow-channel compression table 0.</summary>
        Ncc0,
        /// <summary>Narrow-channel compression table 1.</summary>
        Ncc1,
        /// <summary>256 entry color palette.</summary>
        Palette
    }
}
