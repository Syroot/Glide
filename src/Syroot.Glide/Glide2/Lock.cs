﻿using System;

namespace Syroot.Glide.Glide2
{
    /// <summary>
    /// Represents a bit field created by the combination of <see cref="ReadOnly"/> or <see cref="WriteOnly"/> and
    /// <see cref="Idle"/> or <see cref="NoIdle"/>.
    /// </summary>
    [Flags]
    public enum Lock
    {
        /// <summary><see cref="LfbInfo.LfbPtr"/> should only be used for read access; writing to this pointer will have
        /// undefined effects on the graphics subsystem.</summary>
        ReadOnly = 0,
        /// <summary><see cref="LfbInfo.LfbPtr"/> should only be used for write access; reading from this pointer will
        /// yield undefined data.</summary>
        WriteOnly = 1 << 0,

        /// <summary>The 3D engine will be idled before <see cref="Gr.LfbLock"/> returns. This is the default behavior
        /// if no idle request flag is specified.</summary>
        Idle = 0,
        /// <summary>The 3D engine will not be idled; there is no guarantee of serialization of linear frame buffer
        /// accesses and triangle rendering or buffer clearing operations.</summary>
        NoIdle = 1 << 1
    }
}
