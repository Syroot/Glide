﻿namespace Syroot.Glide.Glide2
{
    /// <summary>
    /// Represents the other value used in source value generation.
    /// </summary>
    public enum CombineOther
    {
        /// <summary>Iterated vertex value (Gouraud shading).</summary>
        Iterated,
        /// <summary>Value from texture map.</summary>
        Texture,
        /// <summary>Constant value.</summary>
        Constant,
        /// <summary>Unspecified value.</summary>
        None = Constant
    }
}
