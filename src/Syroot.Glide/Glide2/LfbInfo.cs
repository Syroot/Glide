﻿using System;
using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2
{
    [StructLayout(LayoutKind.Sequential)]
    public struct LfbInfo
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public int Size;
        public IntPtr LfbPtr;
        public uint StrideInBytes;
        public LfbWriteMode WriteMode;
        public Origin Origin;
    }
}
