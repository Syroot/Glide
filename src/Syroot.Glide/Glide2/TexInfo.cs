﻿using System;
using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2
{
    [StructLayout(LayoutKind.Sequential)]
    public struct TexInfo
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public Lod SmallLod;
        public Lod LargeLod;
        public Aspect AspectRatio;
        public TextureFormat Format;
        public IntPtr Data;
    }
}
