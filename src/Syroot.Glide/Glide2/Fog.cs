﻿namespace Syroot.Glide.Glide2
{
    public enum Fog
    {
        Disable,
        WithIteratedAlpha,
        WithTable,
        WithIteratedZ,
        Mult2 = 0x100,
        Add2 = 0x200
    }
}
