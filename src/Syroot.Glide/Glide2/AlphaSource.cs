﻿namespace Syroot.Glide.Glide2
{
    /// <summary>
    /// Represents how A_local and output alpha are computed based on the mode.
    /// </summary>
    public enum AlphaSource
    {
        /// <summary>
        /// <para>Alpha Output: constant color alpha</para>
        /// <para>A_local: constant color alpha</para>
        /// </summary>
        CcAlpha,
        /// <summary>
        /// <para>Alpha Output: iterated vertex alpha</para>
        /// <para>A_local: iterated vertexalpha</para>
        /// </summary>
        IteratedAlpha,
        /// <summary>
        /// <para>Alpha Output: texture alpha</para>
        /// <para>A_local: none</para>
        /// </summary>
        TextureAlpha,
        /// <summary>
        /// <para>Alpha Output: texture alpha * iterated alpha</para>
        /// <para>A_local: iterated vertex alpha</para>
        /// </summary>
        TextureAlphaTimesIteratedAlpha
    }
}
