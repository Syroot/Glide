﻿namespace Syroot.Glide.Glide2
{
    public enum Cull
    {
        Disable,
        Negative,
        Positive
    }
}
