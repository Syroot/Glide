﻿using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Sst96Config
    {
        /// <summary>How much?</summary>
        public int FbRam;
        public int NTexelFx;
        public TmuConfig TmuConfig;
    }
}
