﻿namespace Syroot.Glide.Glide2
{
    public enum SstType
    {
        /// <seealso cref="VoodooConfig"/>
        Voodoo,
        /// <seealso cref="Sst96Config"/>
        Sst96,
        /// <seealso cref="AT3DConfig"/>
        AT3D,
        /// <seealso cref="VoodooConfig"/>
        Voodoo2
    }
}
