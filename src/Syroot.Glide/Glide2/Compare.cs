﻿namespace Syroot.Glide.Glide2
{
    /// <summary>
    /// Represents value comparison functions.
    /// </summary>
    public enum Compare
    {
        /// <summary>Never passes.</summary>
        Never,
        /// <summary>Passes if the incoming value is less than the other value.</summary>
        Less,
        /// <summary>Passes if the incoming value is equal to the other value.</summary>
        Equal,
        /// <summary>Passes if the incoming value is less than or equal to the other value.</summary>
        LEqual,
        /// <summary>Passes if the incoming value is greater than the other value.</summary>
        Greater,
        /// <summary>Passes if the incoming value is not equal to the other value.</summary>
        NotEqual,
        /// <summary>Passes if the incoming value is greater than or equal to the other value.</summary>
        GEqual,
        /// <summary>Always passes.</summary>
        Always,
    }
}
