﻿namespace Syroot.Glide.Glide2
{
    /// <summary>
    /// Represents the local color used in source color generation.
    /// </summary>
    public enum CombineLocal
    {
        /// <summary>Iterated vertex color (Gouraud shading).</summary>
        Iterated,
        /// <summary>Constant color.</summary>
        Constant,
        /// <summary>High 8 bits from iterated vertex Z.</summary>
        Depth,
        /// <summary>Unspecified color.</summary>
        None = Constant
    }
}
