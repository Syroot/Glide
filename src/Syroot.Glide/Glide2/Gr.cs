﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using Syroot.Glide.Glide2.Utilities;

namespace Syroot.Glide.Glide2
{
    /// <summary>
    /// Represents raw P/Invoke access to the graphics API of the glide2x library.
    /// </summary>
    public static class Gr
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        /// <summary>Number of entries in a fog table.</summary>
        public const int FogTableSize = 64;
        /// <summary>Represents an invalid mipmap handle.</summary>
        public const uint NullMipMapHandle = UInt32.MaxValue;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Draws an anti-aliased line segment.
        /// </summary>
        /// <param name="a">First vertex describing the line segment.</param>
        /// <param name="b">Second vertex describing the line segment.</param>
        /// <remarks>
        /// <para>Glide draws an anti-aliased line segment between the two vertices by setting up the alpha iterator so
        /// that it represents pixel coverage. <see cref="AlphaCombine"/> must select iterated alpha and
        /// <see cref="AlphaBlendFunction"/> should select <see cref="Blend.SrcAlpha"/>,
        /// <see cref="Blend.OneMinusSrcAlpha"/> as the RGB blend functions and <see cref="Blend.Zero"/>,
        /// <see cref="Blend.Zero"/> as the alpha blend functions if sorting from back to front and
        /// <see cref="Blend.AlphaSaturate"/>, <see cref="Blend.One"/> as the RGB blend functions and
        /// <see cref="Blend.AlphaSaturate"/>, <see cref="Blend.One"/> as the alpha blend functions if sorting from
        /// front to back. Opaque anti-aliased primitives <b>must</b> set alpha = 255 in the vertex data. Transparent
        /// anti-aliased primitives are drawn by setting alpha to values less than 255; this alpha value is multiplied
        /// by the pixel coverage to obtain the final alpha value for alpha blending.</para>
        /// <para>Resultant lines will be somewhat "fatter" than expected.</para>
        /// </remarks>
        /// <seealso cref="AADrawPoint"/>
        /// <seealso cref="AADrawTriangle"/>
        /// <seealso cref="AlphaBlendFunction"/>
        /// <seealso cref="AlphaCombine"/>
        [DllImport("glide2x", EntryPoint = "_grAADrawLine@8")]
        public static extern void AADrawLine(ref Vertex a, ref Vertex b);

        /// <summary>
        /// Draws an anti-aliased point.
        /// </summary>
        /// <param name="a">The point to draw.</param>
        /// <remarks>
        /// Glide draws an anti-aliased point by rendering four pixels and setting up the alpha iterator so that it
        /// represents pixel coverage. <see cref="AlphaCombine"/> must select iterated alpha and
        /// <see cref="AlphaBlendFunction"/> should select <see cref="Blend.SrcAlpha"/>,
        /// <see cref="Blend.OneMinusSrcAlpha"/> as the RGB blend functions and <see cref="Blend.Zero"/>,
        /// <see cref="Blend.Zero"/> as the alpha blend functions if sorting from back to front and
        /// <see cref="Blend.AlphaSaturate"/>, <see cref="Blend.One"/> as the RGB blend functions and
        /// <see cref="Blend.AlphaSaturate"/>, <see cref="Blend.One"/> as the alpha blend functions if sorting from
        /// front to back. Opaque anti-aliased primitives <b>must</b> set alpha = 255 in the vertex data. Transparent
        /// anti-aliased primitives are drawn by setting alpha to values less than 255; this alpha value is multiplied
        /// by the pixel coverage to obtain the final alpha value for alpha blending.
        /// </remarks>
        /// <seealso cref="AADrawLine"/>
        /// <seealso cref="AADrawTriangle"/>
        /// <seealso cref="AlphaBlendFunction"/>
        /// <seealso cref="AlphaCombine"/>
        [DllImport("glide2x", EntryPoint = "_grAADrawPoint@4")]
        public static extern void AADrawPoint(ref Vertex a);

        /// <summary>
        /// Specifies the alpha blending function.
        /// </summary>
        /// <param name="rgbSf">Specifies the red, green, and blue source blending factors.</param>
        /// <param name="rgbDf">Specifies the red, green, and blue destination blending factors.</param>
        /// <param name="alphaSf">Specifies the alpha source blending factor.</param>
        /// <param name="alphaDf">Specifies the alpha destination blending factor.</param>
        /// <remarks>
        /// <para>Alpha blending blends the RGBA values for rendered pixels (source) with the RGBA values that are
        /// already in the frame buffer (destination). This method defines the operation of blending.
        /// <paramref name="rgbSf"/> and <paramref name="alphaSf"/> specifies which of nine methods is used to scale the
        /// source color and alpha components. <paramref name="rgbDf"/> and <paramref name="alphaDf"/> specifies which
        /// of eight methods is used to scale the destination color and alpha components.</para>
        /// <para>To disable alpha blending, call <c>AlphaBlendFunction(Blend.One, Blend.Zero, Blend.One, 
        /// Blend.Zero)</c>.</para>
        /// <para>The source of incoming alpha and color are determined by <see cref="AlphaCombine"/> and
        /// <see cref="ColorCombine"/> respectively.</para>
        /// <para>Alpha blending that requires a destination alpha is mutually exclusive of either depth buffering or
        /// triple buffering. Attempting to use <see cref="Blend.DstAlpha"/>, <see cref="Blend.OneMinusDstAlpha"/>, or
        /// <see cref="Blend.AlphaSaturate"/> when depth buffering or triple buffering are enabled will have undefined
        /// results.</para>
        /// <para>For alpha source and destination blend function factor parameters, Voodoo Graphics supports only
        /// <see cref="Blend.Zero"/> and <see cref="Blend.One"/>.</para>
        /// <para><see cref="Blend.PreFogColor"/> is useful when applying fog to a scene generated in multiple passes.
        /// See the Glide Programming Guide for more information.</para>
        /// </remarks>
        /// <seealso cref="AADrawLine"/>
        /// <seealso cref="AADrawPoint"/>
        /// <seealso cref="AADrawTriangle"/>
        /// <seealso cref="AlphaCombine"/>
        /// <seealso cref="ColorCombine"/>
        [DllImport("glide2x", EntryPoint = "_grAlphaBlendFunction@16")]
        public static extern void AlphaBlendFunction(Blend rgbSf, Blend rgbDf, Blend alphaSf, Blend alphaDf);

        /// <summary>
        /// Configures the alpha combine unit.
        /// </summary>
        /// <param name="func">Specifies the function used in source alpha generation.</param>
        /// <param name="factor">Specifies the scaling factor used in alpha generation.</param>
        /// <param name="local">Specifies the local alpha used in source alpha generation.</param>
        /// <param name="other">Specifies the other alpha used in source alpha generation.</param>
        /// <param name="invert">Specifies whether the generated alpha should be bitwise inverted as a final step.</param>
        /// <remarks>
        /// <para>This method configures the alpha combine unit of the graphics subsystem's hardware pipeline. This
        /// provides a low level mechanism for controlling all rendering modes within the hardware without manipulating
        /// individual register bits. The alpha combine unit computes the source alpha for the remainder of the
        /// rendering pipeline. The default mode is <c>AlphaCombine(CombineFunction.ScaleOther, CombineFactor.One,
        /// CombineLocal.None, CombineOther.Constant, false)</c>.</para>
        /// <para>The alpha combine unit computes the function specified by the combine function on the inputs specified
        /// by the local combine source, other combine source, and the combine scale factor. The result is clamped to
        /// [0..255], and then a bitwise inversion may be applied, controlled by the <paramref name="invert"/>
        /// parameter.</para>
        /// <para>The constant color parameters are the colors passed to <see cref="ConstantColorValue"/>. If the
        /// texture has no alpha component, then texture alpha is 255.</para>
        /// <para>This method also keeps track of required vertex parameters for the rendering routines.
        /// <see cref="CombineFactor.None"/>, <see cref="CombineLocal.None"/>, and <see cref="CombineOther.None"/> are
        /// provided to indicate that no parameters are required. Currently they are the same as
        /// <see cref="CombineFactor.Zero"/>, <see cref="CombineLocal.Constant"/>, and
        /// <see cref="CombineOther.Constant"/> respectively.</para>
        /// <para>The local alpha value specified by the <paramref name="local"/> parameter and the other alpha value
        /// specified by the <paramref name="other"/> parameter are used by the color combine unit.</para>
        /// <para>Inverting the bits in a color is the same as computing (1.0 - color) for floating point color values 
        /// in the range [0..1] or (255 - color) for 8-bit color values in the range [0..255].</para>
        /// </remarks>
        /// <seealso cref="ColorCombine"/>
        /// <seealso cref="ConstantColorValue"/>
        /// <seealso cref="DrawTriangle"/>
        [DllImport("glide2x", EntryPoint = "_grAlphaCombine@20")]
        public static extern void AlphaCombine(CombineFunction func, CombineFactor factor, CombineLocal local,
            CombineOther other, bool invert);

        /// <summary>
        /// Specifies the alpha test function.
        /// </summary>
        /// <param name="function">The new alpha comparison function.</param>
        /// <remarks>
        /// <para>The alpha test discards pixels depending on the outcome of a comparison between the incoming alpha
        /// value and a constant reference value. This method specifies the comparison function and
        /// <see cref="AlphaTestReferenceValue"/> specifies the constant reference value.</para>
        /// <para>The incoming alpha value is compared to the constant alpha test reference value using the function
        /// specified by <paramref name="function"/>. If the comparison passes, the pixel is drawn, conditional on
        /// subsequent tests, such as depth buffer and chroma-key. If the comparison fails, the pixel is not drawn. The
        /// default function is <see cref="Compare.Always"/>.</para>
        /// <para>Alpha testing is performed on all pixel writes, including those resulting from scan conversion of
        /// points, lines, and triangles, and from direct linear frame buffer writes. Alpha testing is implicitly
        /// disabled during linear frame buffer writes if linear frame buffer bypass is enabled
        /// (see <see cref="LfbLock"/>).</para>
        /// <para>The incoming alpha is the output of the alpha combine unit which is configured with
        /// <see cref="AlphaCombine"/>.</para>
        /// </remarks>
        /// <seealso cref="AlphaCombine"/>
        /// <seealso cref="AlphaTestReferenceValue"/>
        /// <seealso cref="LfbLock"/>
        [DllImport("glide2x", EntryPoint = "_grAlphaTestFunction@4")]
        public static extern void AlphaTestFunction(Compare function);

        /// <summary>
        /// Specifies the alpha test reference value.
        /// </summary>
        /// <param name="value">The new alpha test reference value.</param>
        /// <remarks>
        /// <para>The alpha test discards pixels depending on the outcome of a comparison between the pixel's incoming
        /// alpha value and a constant reference value. <see cref="AlphaTestFunction"/> specifies the comparison
        /// function and this method specifies the constant reference value. The default reference value is <c>0x00</c>.</para>
        /// <para>The incoming alpha value is compared to the <paramref name="value"/> using the function specified by
        /// <see cref="AlphaTestFunction"/>. If the comparison passes, the pixel is drawn, conditional on subsequent
        /// tests such as depth buffer and chroma-key. If the comparison fails, the pixel is not drawn.</para>
        /// <para>Alpha testing is performed on all pixel writes, including those resulting from scan conversion of
        /// points, lines, and triangles, and from direct linear frame buffer writes. Alpha testing is implicitly
        /// disabled during linear frame buffer writes if linear frame buffer bypass is enabled
        /// (see <see cref="LfbLock"/>).</para>
        /// <para>The incoming alpha is the output of the alpha combine unit which is configured with
        /// <see cref="AlphaCombine"/>.</para>
        /// </remarks>
        /// <seealso cref="AlphaCombine"/>
        /// <seealso cref="AlphaTestFunction"/>
        /// <seealso cref="LfbLock"/>
        [DllImport("glide2x", EntryPoint = "_grAlphaTestReferenceValue@4")]
        public static extern void AlphaTestReferenceValue(byte value);

        /// <summary>
        /// Clears the buffers to the specified values.
        /// </summary>
        /// <param name="color">The color value used for clearing the draw buffer.</param>
        /// <param name="alpha">The alpha value used for clearing the alpha buffer (ignored if alpha buffering is not
        /// enabled, i.e. a destination alpha is not specified in <see cref="AlphaBlendFunction"/>.</param>
        /// <param name="depth">16-bit unsigned value used for clearing the depth buffer (ignored if depth buffering is
        /// not enabled).</param>
        /// <remarks>
        /// <para>This method clears the appropriate buffers with the given values. <see cref="ClipWindow"/> defines the
        /// area within the buffer to be cleared. Any buffers that are enabled are cleared by this method. For example,
        /// if depth buffering is enabled, the depth buffer will be cleared. If an application does not want a buffer to
        /// be cleared, then it should mask off writes to the buffer using <see cref="DepthMask"/> and / or
        /// <see cref="ColorMask"/> as appropriate.</para>
        /// <para>Although color, alpha, and depth parameters are always specified, the parameters actually used will
        /// depend on the current configuration of the hardware; the irrelevant parameters are ignored.</para>
        /// <para>The <paramref name="depth"/> parameter can be one of the constants <see cref="ZDepthValue.Nearest"/>,
        /// <see cref="ZDepthValue.Farthest"/>, <see cref="WDepthValue.Nearest"/>, <see cref="WDepthValue.Farthest"/>,
        /// or a direct representation of a value in the depth buffer. In the latter case the value is either a
        /// <c>1 / Z</c> value (for <see cref="DepthBuffer.ZBuffer"/>) or a 16-bit floating point format W value
        /// (for <see cref="DepthBuffer.WBuffer"/>). The 16-bit floating point format is described in detail in the
        /// Glide Programming Manual.</para>
        /// <para>A buffer clear fills pixels at twice the rate of triangle rendering, therefore the performance cost of
        /// clearing the buffer is half the cost of rendering a rectangle. Clearing buffers is not always necessary and
        /// should be avoided if possible. When depth buffering is disabled and every visible pixel is rendered each
        /// frame, simply draw each frame on top of whatever was previously in the frame buffer. When depth buffering is
        /// enabled, a sorted background that covers the entire area can be drawn with the depth buffer compare function
        /// set to <see cref="Compare.Always"/> so that all pixel colors and depth values are replaced, and then normal
        /// depth buffering can be resumed.</para>
        /// </remarks>
        /// <seealso cref="ClipWindow"/>
        /// <seealso cref="ColorMask"/>
        /// <seealso cref="DepthMask"/>
        /// <seealso cref="RenderBuffer"/>
        [DllImport("glide2x", EntryPoint = "_grBufferClear@12")]
        public static extern void BufferClear(uint color, byte alpha, ushort depth);

        /// <summary>
        /// Exchanges front and back buffers.
        /// </summary>
        /// <param name="swapInterval">The number of vertical retraces to wait before swapping the front and back
        /// buffers.</param>
        /// <remarks>
        /// <para>This method exchanges the front and back buffers in the graphics subsystem after
        /// <paramref name="swapInterval"/> vertical retraces. If the <paramref name="swapInterval"/> is <c>0</c>, then
        /// the buffer swap does not wait for vertical retrace. Otherwise, the buffers are swapped after
        /// <paramref name="swapInterval"/> vertical retraces. For example, if the monitor frequency is 60 Hz, a
        /// <paramref name="swapInterval"/> of 3 results in a maximum frame rate of 20 Hz.</para>
        /// <para>The exchange takes palce during the next vertical retrace of the monitor, rather than immediately
        /// after this method is called. If the application is double buffering, the Voodoo Graphics subsystem will
        /// stop rendering and wait until the swap occurs before executing more commands. If the application is triple
        /// buffering and the third rendering buffer is available, rendering commands will take place immediately in the
        /// third buffer.</para>
        /// <para>A <paramref name="swapInterval"/> of <c>0</c> may result in visual artifacts, such as "tearing", since
        /// a buffer swap can occur during the middle of a screen refresh cycle. This setting is very useful in
        /// performance monitoring situations, as true rendering performance can be measured without including the time
        /// buffer swaps spend waiting for vertical retrace.</para>
        /// <para>This method waits until there are fewer than 7 pending buffer swap requests in the Voodoo Graphics
        /// command FIFO before returning.</para>
        /// </remarks>
        /// <seealso cref="BufferNumPending"/>
        [DllImport("glide2x", EntryPoint = "_grBufferSwap@4")]
        public static extern void BufferSwap(int swapInterval);

        /// <summary>
        /// Enables / disables hardware chroma-keying.
        /// </summary>
        /// <param name="mode">Specifies whether chroma-keying should be enabled or disabled.</param>
        /// <remarks>
        /// <para>Enables and disables chroma-keying. When chroma-keying is enabled, color values are compared to a
        /// global chroma-key reference value (set by <see cref="ChromakeyValue"/>. If the pixel's color is the same as
        /// the chroma-key reference value, the pixel is discarded. The chroma-key comparison takes place before the
        /// color combine function. By default, chroma-keying is disabled.</para>
        /// <para>The chroma-key comparison compares the chroma-key reference value to the <c>other</c> color specified
        /// in the configuration of the color combine unit.</para>
        /// </remarks>
        /// <seealso cref="ColorCombine"/>
        /// <seealso cref="ChromakeyValue"/>
        [DllImport("glide2x", EntryPoint = "_grChromakeyMode@4")]
        public static extern void ChromakeyMode(Chromakey mode);

        /// <summary>
        /// Sets the global chroma-key reference value.
        /// </summary>
        /// <param name="value">The new chroma-key reference value.</param>
        /// <remarks>
        /// <para>Sets the global chroma-key reference value as a packed RGBA value. The color format should be the same
        /// format as specified in the <c>colorFormat</c> parameter to <see cref="SstWinOpen"/>.</para>
        /// <para>The chroma-key comparison compares the chroma-key reference value to the <c>other</c> color specified
        /// in the configuration of the color combine unit. The comparison is performed between colors with 24-bit
        /// precision; thus <paramref name="value"/> must be set accordingly. See Table 10.1 in the Glide Programming
        /// Guide for details on how color formats are expanded to 24 bits.</para>
        /// </remarks>
        /// <seealso cref="ColorCombine"/>
        /// <seealso cref="ChromakeyMode"/>
        [DllImport("glide2x", EntryPoint = "_grChromakeyValue@4")]
        public static extern void ChromakeyValue(uint value);

        /// <summary>
        /// Sets the size and location of the hardware clipping window.
        /// </summary>
        /// <param name="minX">The lower X screen coordinate of the clipping window.</param>
        /// <param name="minY">The lower Y screen coordinate of the clipping window.</param>
        /// <param name="maxX">The upper X screen coordinate of the clipping window.</param>
        /// <param name="maxY">The upper Y screen coordinate of the clipping window.</param>
        /// <remarks>
        /// <para>This method specifies the hardware clipping window. Any pixels outside the clipping window are
        /// rejected. Values are inclusive for minimum X and Y values and excluive for maximum X and Y values. The
        /// clipping window also specifies the area <see cref="BufferClear"/> clears.</para>
        /// <para>At startup, the default values for the clip window are the full size of the screen, e.g.
        /// (0, 0, 640, 480) for 640x480 mode and (0, 0, 800, 600) for 800x600 mode. To disable clipping, simply set the
        /// size of the clip window to the screen size. The clipping window should not be used for general purpose
        /// primitive clipping; since clipped pixels are processed but discarded, proper geometric clipping should be
        /// done by the application for best performance. The clipping window should be used to prevent stray pixels
        /// that appear from imprecise geometric clipping. Note that if the pixel pipeline is disabled
        /// (see <see cref="LfbLock"/>, clipping is not performed on linear frame buffer writes.</para>
        /// </remarks>
        /// <seealso cref="BufferClear"/>
        /// <seealso cref="LfbLock"/>
        [DllImport("glide2x", EntryPoint = "_grClipWindow@16")]
        public static extern void ClipWindow(uint minX, uint minY, uint maxX, uint maxY);

        /// <summary>
        /// Configures the color combine unit.
        /// </summary>
        /// <param name="func">Specifies the function used in source color generation.</param>
        /// <param name="factor">Specifies the scaling factor <i>f</i> used in source color generation.</param>
        /// <param name="local">Specifies the local color used in source color generation.</param>
        /// <param name="other">Specifies the other color used in source color generation.</param>
        /// <param name="invert">Specifies whether the generated source color shuold be bitwise inverted as a final
        /// step.</param>
        /// <remarks>
        /// <para>This method configures the color combine unit of the Voodoo Graphics subsystem's hardawre pipeline.
        /// This provides a low level mechanism for controlling all modes of the color combine unit wihtout manipulating
        /// individual register bits.</para>
        /// <para>The color combine unit computes the function specified by the combine function on the inputs specified
        /// by the local combine source, other combine source, and the combine scale factor. The result is clamped to
        /// [0..255], and then a bitwise inversion may be applied, controlled by the <paramref name="invert"/>
        /// parameter. The resulting color goes to the alpha and depth units. The default color combine mode is
        /// <c>ColorCombine(CombineFunction.ScaleOther, CombineFactor.One, CombineLocal.Iterated, CombineOther.Iterated,
        /// false)</c>.</para>
        /// <para><see cref="CombineLocal.Constant"/> and <see cref="CombineOther.Constant"/> select the constant color
        /// specified in a previous call to <see cref="ConstantColorValue"/>. The iterated color selected by
        /// <see cref="CombineLocal.Iterated"/> or <see cref="CombineOther.Iterated"/> are the red, green, blue, and
        /// alpha values associated with a drawing primitive's vertices.</para>
        /// <para>This method also keeps track of required vertex parameters for the rendering routines.
        /// <see cref="CombineFactor.None"/>, <see cref="CombineLocal.None"/>, and <see cref="CombineOther.None"/> are
        /// provided to indicate that no parameters are required. Currently they are the same as
        /// <see cref="CombineFactor.Zero"/>, <see cref="CombineLocal.Constant"/>, and
        /// <see cref="CombineOther.Constant"/> respectively.</para>
        /// <para>Inverting the bits in a color is the same as computing (1.0 - color) for floating point color values 
        /// in the range [0..1] or (255 - color) for 8-bit color values in the range [0..255].</para>
        /// </remarks>
        /// <seealso cref="AlphaCombine"/>
        /// <seealso cref="ConstantColorValue"/>
        /// <seealso cref="DrawTriangle"/>
        [DllImport("glide2x", EntryPoint = "_grColorCombine@20")]
        public static extern void ColorCombine(CombineFunction func, CombineFactor factor, CombineLocal local,
            CombineOther other, bool invert);

        /// <summary>
        /// Sets the global constant color.
        /// </summary>
        /// <param name="color">The new constant color.</param>
        /// <remarks>
        /// Glide refers to a global constant color combine unit and alpha combine unit if
        /// <see cref="CombineLocal.Constant"/> or <see cref="CombineOther.Constant"/> are specified. This constant
        /// color is set with this method. The color format should be in the same format as specified in the
        /// <c>colorFormat</c> parameter to <see cref="SstWinOpen"/>. The default value is <c>0xFFFFFFFF</c>.</remarks>
        /// <seealso cref="AlphaCombine"/>
        /// <seealso cref="ColorCombine"/>
        [DllImport("glide2x", EntryPoint = "_grConstantColorValue@4")]
        public static extern void ConstantColorValue(uint color);

        /// <summary>
        /// Sets the cull mode.
        /// </summary>
        /// <param name="mode">The new culling mode.</param>
        /// <remarks>
        /// <para>Specifies the type of backface culling, if any, that Glide performs when rendering a triangle. Glide
        /// computes the signed area of a triangle prior to rendering, and the sign of this area can be used for
        /// backface culling operations. If the sign of the area matches the <paramref name="mode"/>, then the triangle
        /// is rejected. This method assumes that <see cref="Cull.Positive"/> corresponds to a counter-clockwise
        /// oriented triangle when the origin is <see cref="Origin.LowerLeft"/> and a clockwise oriented triangle when
        /// the origin is <see cref="Origin.UpperLeft"/>.</para>
        /// <para>This method has no effect on points and lines, but does effect all triangle rendering primitives
        /// including polygons.</para>
        /// </remarks>
        /// <seealso cref="DrawTriangle"/>
        [DllImport("glide2x", EntryPoint = "_grCullMode@4")]
        public static extern void CullMode(Cull mode);

        /// <summary>
        /// Sets the depth bias level.
        /// </summary>
        /// <param name="level">The new depth bias level.</param>
        /// <remarks>
        /// <para>This method allows an application to specify a depth bias used when rendering coplanar polygons.
        /// Specifically, if two polygons are coplanar but do not share vertices, e.g. a surface detail polygon sits on
        /// top of a larger polygon, artifacts such as "poke through" may result. To remedy such artifacts, an
        /// application should increment or decrement the depth bias level, as appropriate for the depth buffer mode and
        /// function, per coplanar polygon. For left handed coordinate systems where <c>0x0000</c> corresponds to
        /// "nearest to viewer" and <c>0xFFFF</c> corresponds to "farthest from viewer", depth bias levels should be
        /// decremented on successive rendering of coplanar polygons.</para>
        /// <para>Depth biasing is mutually exclusive of linear frame buffer writes.</para>
        /// <para>In depth buffering modes <see cref="DepthBuffer.ZBufferCompareToBias"/> and
        /// <see cref="DepthBuffer.WBufferCompareToBias"/>, the depth bias level specifies the value to compare depth
        /// buffer values against, and is not added to the source depth value when writing to the depth buffer. See
        /// <see cref="DepthBufferMode"/> for more informatoin.</para>
        /// </remarks>
        /// <seealso cref="DepthBufferMode"/>
        /// <seealso cref="DepthMask"/>
        [DllImport("glide2x", EntryPoint = "_grDepthBiasLevel@4")]
        public static extern void DepthBiasLevel(short level);

        /// <summary>
        /// Specifies the depth buffer comparison function.
        /// </summary>
        /// <param name="function">The new depth comparison function.</param>
        /// <remarks>
        /// <para>This method specifies the function used to compare each rendered pixel's depth value with the depth
        /// value present in the depth buffer. The comparison is performed only if depth testing is enabled with
        /// <see cref="DepthBuffer"/>. The choice of depth buffer function is typically dependent upon the depth
        /// buffer mode currently active.</para>
        /// <para>The default comparison function is <see cref="Compare.Less"/>.</para>
        /// </remarks>
        /// <seealso cref="DepthBuffer"/>
        /// <seealso cref="DepthMask"/>
        /// <seealso cref="DepthBiasLevel"/>
        /// <seealso cref="LfbConstantDepth"/>
        [DllImport("glide2x", EntryPoint = "_grDepthBufferFunction@4")]
        public static extern void DepthBufferFunction(Compare function);

        /// <summary>
        /// Sets the depth buffering mode.
        /// </summary>
        /// <param name="mode">The new depth buffering mode.</param>
        /// <remarks>
        /// <para>This method specifies the type of depth buffering to be performed. If <see cref="DepthBuffer.ZBuffer"/>
        /// or <see cref="DepthBuffer.ZBufferCompareToBias"/> is selected, then the Voodoo Graphics subsystem will
        /// perform 16-bit fixed point Z buffering. If <see cref="DepthBuffer.WBuffer"/> or
        /// <see cref="DepthBuffer.WBufferCompareToBias"/> is selected, then the Voodoo Graphics subsystem will perform
        /// 16-bit floating point W buffering. By default the depth buffer node is <see cref="DepthBuffer.Disable"/>.
        /// Refer to the Glide Programming Guie for more information about W and Z buffering.</para>
        /// <para>If <see cref="DepthBuffer.ZBufferCompareToBias"/> or <see cref="DepthBuffer.WBufferCompareToBias"/> is
        /// selected, then the bias specified with <see cref="DepthBiasLevel"/> is used as a pixel's depth value for
        /// comparison purposes only. Depth buffer values are compared against the depth bias level and if the compare
        /// passes and the depth buffer mask is enabled, the pixel's unbiased depth value is written to the depth
        /// buffer. This mode is useful for clearing beneath cockpits and other types of overlays without affecting
        /// either the color or depth values for the cockpit or overlay.</para>
        /// <para>Since alpha, depth, and triple buffering are mutually exclusive of each other, enabling depth
        /// buffering when using eitiher the alpha or triple buffer will have undefined results.</para>
        /// </remarks>
        /// <seealso cref="DepthBufferFunction"/>
        /// <seealso cref="DepthMask"/>
        /// <seealso cref="DepthBiasLevel"/>
        /// <seealso cref="LfbConstantDepth"/>
        [DllImport("glide2x", EntryPoint = "_grDepthBufferMode@4")]
        public static extern void DepthBufferMode(DepthBuffer mode);

        /// <summary>
        /// Enables / disables writing into the depth buffer.
        /// </summary>
        /// <param name="enable">The new depth buffer mask.</param>
        /// <remarks>
        /// <para>This method specifies whether the depth buffer is enabled for writing. If <paramref name="enable"/> is
        /// <see langword="false"/>, depth buffer writing is disabled. Otherwise, it is enabled. Initially, depth buffer
        /// writing is disabled.</para>
        /// <para>Since the alpha, depth, and triple buffers share the same memory, this method should be called only if
        /// depth buffering is being used.</para>
        /// <para>This method is ignored during linear frame buffer writes if the pixel pipeline is bypassed
        /// (see <see cref="LfbLock"/>).</para>
        /// </remarks>
        /// <seealso cref="BufferClear"/>
        /// <seealso cref="DepthBufferFunction"/>
        /// <seealso cref="DepthBufferMode"/>
        /// <seealso cref="DepthBiasLevel"/>
        /// <seealso cref="LfbConstantDepth"/>
        /// <seealso cref="LfbLock"/>
        [DllImport("glide2x", EntryPoint = "_grDepthMask@4")]
        public static extern void DepthMask(bool enable);

        /// <summary>
        /// Sets the dithering mode.
        /// </summary>
        /// <param name="mode">The new dithering mode.</param>
        /// <remarks>
        /// <para>This method selects the form of dithering used when converting 24-bit RGB values to the 16-bit RGB
        /// color buffer format.</para>
        /// <para>The default dithering <paramref name="mode"/> is <see cref="Dither.Dither4x4"/>. This method is not
        /// affected by <see cref="DisableAllEffects"/>.</para>
        /// </remarks>
        [DllImport("glide2x", EntryPoint = "_grDitherMode@4")]
        public static extern void DitherMode(Dither mode);

        /// <summary>
        /// Draws a one-pixel-wide arbitrarily oriented line.
        /// </summary>
        /// <param name="a">First endpoint and attributes of the line.</param>
        /// <param name="b">Second endpoint and attributes of the line.</param>
        /// <remarks>
        /// Renders a one-pixel-wide arbitrarily oriented line with the given endpoints. All current Glide attributes
        /// will affect the appearance of the line.
        /// </remarks>
        /// <seealso cref="DrawPoint"/>
        /// <seealso cref="DrawTriangle"/>
        [DllImport("glide2x", EntryPoint = "_grDrawLine@8")]
        public static extern void DrawLine(ref Vertex a, ref Vertex b);

        /// <summary>
        /// Draws a point.
        /// </summary>
        /// <param name="a">Location and attributes of the point.</param>
        /// <remarks>
        /// Renders a single point. All current Glide attributes will affect the appearance of the point. If many points
        /// need to be rendered to the screen, e.g. a sprite, use linear frame buffer writes instead.
        /// </remarks>
        /// <seealso cref="DrawLine"/>
        /// <seealso cref="DrawTriangle"/>
        /// <seealso cref="LfbLock"/>
        [DllImport("glide2x", EntryPoint = "_grDrawPoint@4")]
        public static extern void DrawPoint(ref Vertex a);

        /// <summary>
        /// Draws a triangle.
        /// </summary>
        /// <param name="a">Location and attributes of the first vertex defining the triangle.</param>
        /// <param name="b">Location and attributes of the second vertex defining the triangle.</param>
        /// <param name="c">Location and attributes of the third vertex defining the triangle.</param>
        /// <remarks>
        /// <para>Renders an arbitrarily oriented triangle. All current Glide attributes will affect the appearance of
        /// the triangle. Triangles are rendered with the following filling rules:</para>
        /// <para>1. Zero area triangles render zero pixels.</para>
        /// <para>2. Pixels are rendered if and only if their center lies within the triangle.</para>
        /// <para>A pixel center is within a triangle if it is inside all three of the edges. If a pixel center lies
        /// exactly on an edge, it is considered to be inside for the left and horizontal bottom (lower Y coordinate)
        /// edges and outside for the right and horizontal top (higher Y coordinate) edges. If a pixel is outside any
        /// edge, it is considered to be outside the triangle.</para>
        /// <para>These filling rules guarantee that perfect meshes will draw every pixel within th mesh once and only
        /// once.</para>
        /// </remarks>
        /// <seealso cref="DrawLine"/>
        /// <seealso cref="DrawPoint"/>
        /// <seealso cref="DrawPolygon"/>
        [DllImport("glide2x", EntryPoint = "_grDrawTriangle@12")]
        public static extern void DrawTriangle(ref Vertex a, ref Vertex b, ref Vertex c);

        /// <summary>
        /// Sets the global fog color.
        /// </summary>
        /// <param name="value">The new global fog color.</param>
        /// <remarks>
        /// <para>This method specifies the global fog color to be used during fog blending operations. The color
        /// format should be in the same format as specified in the <c>colorFormat</c> parameter to
        /// <see cref="SstWinOpen"/>.</para>
        /// <para>The fog operation blends the fog color (C_fog) with each rasterized pixel's color (C_in) using a
        /// blending factor <i>f</i>. Factor <i>f</i> is derived either from iterated alpha or a user downloaded fog
        /// table based on the pixel's W component, depending on the current <see cref="FogMode"/>.</para>
        /// <para>The new color is computed as <i>C_out = f * C_fog + (1 - f) * C_in</i>.</para>
        /// <para>Fog is applied after color combining and before alpha blending.</para>
        /// </remarks>
        /// <seealso cref="DisableAllEffects"/>
        /// <seealso cref="FogMode"/>
        /// <seealso cref="FogTable"/>
        [DllImport("glide2x", EntryPoint = "_grFogColorValue@4")]
        public static extern void FogColorValue(uint value);

        /// <summary>
        /// Enables / disables per-pixel fog-blending operations.
        /// </summary>
        /// <param name="mode">The new fog mode.</param>
        /// <remarks>
        /// <para>This method enables / disables fog blending operations. <see cref="Fog.WithIteratedAlpha"/> and
        /// <see cref="Fog.WithTable"/> can be used in combination with <see cref="Fog.Add2"/> or <see cref="Fog.Mult2"/>
        /// to tailor the fog equation, as shown below.</para>
        /// <para>The fog operations blends the fog color (C_fog) with each rasterized pixel's color (C_in) using a
        /// blending actor <i>f</i>. A value of <i>f = 0</i> indicates minimum fog density and a value of <i>f = 255</i>
        /// indicates maximum fog density.</para>
        /// <para>The new color is computed as <i>C_out = f * C_fog + (1 - f) * C_in</i>.</para>
        /// <para>Factor <i>f</i> is determined by <paramref name="mode"/>. If <paramref name="mode"/> is
        /// <see cref="Fog.WithIteratedAlpha"/>, then <i>f</i> is equal to the integer bits of iterated alpha. If
        /// <paramref name="mode"/> is <see cref="Fog.WithTable"/>, then <i>f</i> is computed by interpolating between
        /// fog table entries, where the fog table is indexed with a floating point representation of the pixel's W
        /// component.</para>
        /// <para>Fog is applied after color combining and before alpha blending.</para>
        /// <para>Mode modifiers <see cref="Fog.Add2"/> and <see cref="Fog.Mult2"/> are useful when applying fog to
        /// scenes that require several passes to generate. See the Glide Programming Guide for more information.</para>
        /// </remarks>
        /// <seealso cref="FogColorValue"/>
        /// <seealso cref="FogTable"/>
        [DllImport("glide2x", EntryPoint = "_grFogMode@4")]
        public static extern void FogMode(Fog mode);

        /// <summary>
        /// Downloads a fog table.
        /// </summary>
        /// <param name="table">The new fog table.</param>
        /// <remarks>
        /// <para>This method downloads a new table of 8-bit values that are logically viewed as fog opacity values
        /// corresponding to various depths. The table entries control the amount of blending between the fog color and
        /// the pixel's color. A value of <c>0x00</c> indicates no fog blending and a value of <c>0xFF</c> indicates
        /// complete fog.</para>
        /// <para>The fog operation blends the fog color (C_fog) with each rasterized pixel's color (C_in) using a
        /// blending factor <i>f</i>. Factor <i>f</i> depends upon the most recent call to <see cref="FogMode"/>. If the
        /// <see cref="FogMode"/> is set to <see cref="Fog.WithTable"/>, the factor <i>f</i> is computed by
        /// interpolating between fog table entries, where the fog table is indexed with a floating point representation
        /// of the pixel's W component. The order of the entries within the fog table correspond roughly to their
        /// distance from the viewer. The exact world W corresponding to fog table entry <i>i</i> can be found by
        /// calling <see cref="Gu.FogTableIndexToW"/> or by computing <c>Math.Pow(2.0, 3 + (i >> 2)) / (8 - (i &amp; 3))</c></para>
        /// <para>The new color is computed as <i>C_out = f * C_fog + (1 - f) * C_in</i>.</para>
        /// <para>An exponential fog table can be generated by computing <c>1 - e ^ (kw)) * 255</c> where <c>k</c> is
        /// the fog density and <c>w</c> is world distance. It is usually best to normalize the fog table so that the
        /// last entry is <c>255</c>.</para>
        /// <para>The difference between consecutive entries in the fog table must be less than 64.</para>
        /// <para>Fog is applied after color combining and before alpha blending.</para>
        /// <para>There are several Glide Utility APIs for generating fog tables.</para>
        /// </remarks>
        /// <seealso cref="FogMode"/>
        /// <seealso cref="FogColorValue"/>
        /// <seealso cref="Gu.FogTableIndexToW"/>
        [DllImport("glide2x", EntryPoint = "_grFogTable@4")]
        public static extern unsafe void FogTable(byte* table);
        /// <summary>
        /// Downloads a fog table.
        /// </summary>
        /// <param name="table">The new fog table.</param>
        /// <remarks>
        /// <para>This method downloads a new table of 8-bit values that are logically viewed as fog opacity values
        /// corresponding to various depths. The table entries control the amount of blending between the fog color and
        /// the pixel's color. A value of <c>0x00</c> indicates no fog blending and a value of <c>0xFF</c> indicates
        /// complete fog.</para>
        /// <para>The fog operation blends the fog color (C_fog) with each rasterized pixel's color (C_in) using a
        /// blending factor <i>f</i>. Factor <i>f</i> depends upon the most recent call to <see cref="FogMode"/>. If the
        /// <see cref="FogMode"/> is set to <see cref="Fog.WithTable"/>, the factor <i>f</i> is computed by
        /// interpolating between fog table entries, where the fog table is indexed with a floating point representation
        /// of the pixel's W component. The order of the entries within the fog table correspond roughly to their
        /// distance from the viewer. The exact world W corresponding to fog table entry <i>i</i> can be found by
        /// calling <see cref="Gu.FogTableIndexToW"/> or by computing <c>Math.Pow(2.0, 3 + (i >> 2)) / (8 - (i &amp; 3))</c></para>
        /// <para>The new color is computed as <i>C_out = f * C_fog + (1 - f) * C_in</i>.</para>
        /// <para>An exponential fog table can be generated by computing <c>1 - e ^ (kw)) * 255</c> where <c>k</c> is
        /// the fog density and <c>w</c> is world distance. It is usually best to normalize the fog table so that the
        /// last entry is <c>255</c>.</para>
        /// <para>The difference between consecutive entries in the fog table must be less than 64.</para>
        /// <para>Fog is applied after color combining and before alpha blending.</para>
        /// <para>There are several Glide Utility APIs for generating fog tables.</para>
        /// </remarks>
        /// <seealso cref="FogMode"/>
        /// <seealso cref="FogColorValue"/>
        /// <seealso cref="Gu.FogTableIndexToW"/>
        [DllImport("glide2x", EntryPoint = "_grFogTable@4")]
        public static extern void FogTable(IntPtr table);
        /// <summary>
        /// Downloads a fog table.
        /// </summary>
        /// <param name="table">The new fog table.</param>
        /// <remarks>
        /// <para>This method downloads a new table of 8-bit values that are logically viewed as fog opacity values
        /// corresponding to various depths. The table entries control the amount of blending between the fog color and
        /// the pixel's color. A value of <c>0x00</c> indicates no fog blending and a value of <c>0xFF</c> indicates
        /// complete fog.</para>
        /// <para>The fog operation blends the fog color (C_fog) with each rasterized pixel's color (C_in) using a
        /// blending factor <i>f</i>. Factor <i>f</i> depends upon the most recent call to <see cref="FogMode"/>. If the
        /// <see cref="FogMode"/> is set to <see cref="Fog.WithTable"/>, the factor <i>f</i> is computed by
        /// interpolating between fog table entries, where the fog table is indexed with a floating point representation
        /// of the pixel's W component. The order of the entries within the fog table correspond roughly to their
        /// distance from the viewer. The exact world W corresponding to fog table entry <i>i</i> can be found by
        /// calling <see cref="Gu.FogTableIndexToW"/> or by computing <c>Math.Pow(2.0, 3 + (i >> 2)) / (8 - (i &amp; 3))</c></para>
        /// <para>The new color is computed as <i>C_out = f * C_fog + (1 - f) * C_in</i>.</para>
        /// <para>An exponential fog table can be generated by computing <c>1 - e ^ (kw)) * 255</c> where <c>k</c> is
        /// the fog density and <c>w</c> is world distance. It is usually best to normalize the fog table so that the
        /// last entry is <c>255</c>.</para>
        /// <para>The difference between consecutive entries in the fog table must be less than 64.</para>
        /// <para>Fog is applied after color combining and before alpha blending.</para>
        /// <para>There are several Glide Utility APIs for generating fog tables.</para>
        /// </remarks>
        /// <seealso cref="FogMode"/>
        /// <seealso cref="FogColorValue"/>
        /// <seealso cref="Gu.FogTableIndexToW"/>
        [DllImport("glide2x", EntryPoint = "_grFogTable@4")]
        public static extern void FogTable([MarshalAs(UnmanagedType.LPArray, SizeConst = FogTableSize)] byte[] table);

        /// <summary>
        /// Gets the current state of the current Voodoo Graphics subsystem.
        /// </summary>
        /// <param name="state">Pointer to a <see cref="GrState"/> structure where the state is to be stored.</param>
        /// <remarks>
        /// This method makes a copy of the current state of the current Voodoo Graphics subsystem. This allows an
        /// application to save the state and then restore it later using <see cref="GlideSetState"/>.
        /// </remarks>
        /// <seealso cref="GlideSetState"/>
        [DllImport("glide2x", EntryPoint = "_grGlideGetState@4")]
        public static extern void GlideGetState(out GrState state);

        /// <summary>
        /// Returns the version of Glide.
        /// </summary>
        /// <param name="version"><see cref="StringBuilder"/> to receive the text string describing the Glide version.
        ///  A sample version string is "Glide Version 2.2".</param>
        /// <seealso cref="GlideInit"/>
        [DllImport("glide2x", EntryPoint = "_grGlideGetVersion@4")]
        public static extern void GlideGetVersion(StringBuilder version);
        /// <summary>
        /// Returns the version of Glide.
        /// </summary>
        /// <returns>
        /// A text string that describes the Glide version. A sample version string is "Glide Version 2.2".
        /// </returns>
        /// <seealso cref="GlideInit"/>
        public static string GlideGetVersion()
        {
            StringBuilder sb = new StringBuilder(80);
            GlideGetVersion(sb);
            return sb.ToString();
        }

        /// <summary>
        /// Initializes the Glide library.
        /// </summary>
        /// <remarks>
        /// This method initializes the Glide library, performing tasks such as finding any installed Voodoo Graphics
        /// subsystems, allocating memory, and initializing state variables. It must be called before any other Glide
        /// routines are called.<para/>
        /// <see cref="SstQueryBoards"/> can be called before this method.
        /// </remarks>
        /// <seealso cref="GlideGetVersion"/>
        /// <seealso cref="GlideShutdown"/>
        /// <seealso cref="SstWinOpen"/>
        /// <seealso cref="SstQueryBoards"/>
        /// <seealso cref="SstQueryHardware"/>
        /// <seealso cref="SstSelect"/>
        [DllImport("glide2x", EntryPoint = "_grGlideInit@0")]
        public static extern void GlideInit();

        /// <summary>
        /// Toggles whether a transaprent 3Dfx watermark will be displayed at the bottom right of the screen.
        /// </summary>
        /// <param name="enable">Whether to display the watermark.</param>
        /// <remarks>
        /// This method is not documented officially.
        /// </remarks>
        [DllImport("glide2x", EntryPoint = "_grGlideShamelessPlug@4")]
        public static extern void GlideShamelessPlug(bool enable);

        /// <summary>
        /// Sets the state of the currently active Voodoo Graphics subsystem.
        /// </summary>
        /// <param name="state">Pointer to a <see cref="GrState"/> structure containing the new state.</param>
        /// <remarks>
        /// This method sets the state of the currently active Voodoo Graphics subsystem. This API is typically paired
        /// with calls to <see cref="GlideGetState"/> so that an application can save and restore the state.
        /// </remarks>
        /// <seealso cref="GlideGetState"/>
        [DllImport("glide2x", EntryPoint = "_grGlideSetState@4")]
        public static extern void GlideSetState(ref GrState state);

        /// <summary>
        /// Shuts down the Glide library.
        /// </summary>
        /// <remarks>
        /// This method frees up any system resources allocated by Glide, including memory, and interrupt vectors. It
        /// should be called immediately before program termination.
        /// </remarks>
        /// <seealso cref="GlideInit"/>
        [DllImport("glide2x", EntryPoint = "_grGlideShutdown@0")]
        public static extern void GlideShutdown();

        /// <summary>
        /// Sets the constant alpha value for linear frame buffer writes.
        /// </summary>
        /// <param name="alpha">The new constant alpha value.</param>
        /// <remarks>
        /// <para>Some linear frame buffer write modes, specifically <see cref="LfbWriteMode.Rgb555"/>,
        /// <see cref="LfbWriteMode.Rgb888"/>, <see cref="LfbWriteMode.Rgb555Depth"/>, and
        /// <see cref="LfbWriteMode.ZA16"/>, do not contain alpha information. This method specifies the alpha value for
        /// these linear frame buffer write modes. This alpha value is used if alpha testing and blending operations are
        /// performed during linear frame buffer writes. The default constant alpha value is <c>0xFF</c>.</para>
        /// <para>If a linear frame buffer format contains alpha information, then the alpha supplied with the linear
        /// frame buffer write is used, and the constant alpha value set with this method is ignored.</para>
        /// </remarks>
        /// <seealso cref="AlphaTestFunction"/>
        /// <seealso cref="AlphaBlendFunction"/>
        [DllImport("glide2x", EntryPoint = "_grLfbConstantAlpha@4")]
        public static extern void LfbConstantAlpha(byte alpha);

        /// <summary>
        /// Sets the constant depth value for linear frame buffer writes.
        /// </summary>
        /// <param name="depth">The new constant depth value.</param>
        /// <remarks>
        /// <para>Some linear frame buffer write modes, specifically <see cref="LfbWriteMode.Rgb555"/>,
        /// <see cref="LfbWriteMode.Rgb565"/>, <see cref="LfbWriteMode.Rgb1555"/>, <see cref="LfbWriteMode.Rgb888"/>,
        /// <see cref="LfbWriteMode.Rgb8888"/>, and <see cref="LfbWriteMode.Rgb8888"/>, do not possess depth
        /// information. This method specifies the depth value for these linear frame buffer write modes. This depth
        /// value is used for depth buffering and fog operations and is assumed to be in a format suitable for the
        /// current depth buffering mode. The default constant depth value is <c>0x00</c>.</para>
        /// <para>If a linaer frame buffer format contains depth information, then the depth supplied withi the linear
        /// frame buffer write is used, and the constant depth value set with this method is ignored.</para>
        /// </remarks>
        /// <seealso cref="DepthBufferMode"/>
        /// <seealso cref="FogMode"/>
        [DllImport("glide2x", EntryPoint = "_grLfbConstantDepth@4")]
        public static extern void LfbConstantDepth(ushort depth);

        /// <summary>
        /// Locks a frame buffer in preparation for direct linear frame buffer access.
        /// </summary>
        /// <param name="type">Lock type.</param>
        /// <param name="buffer">Buffer to lock.</param>
        /// <param name="writeMode">Requested destination pixel format.</param>
        /// <param name="origin">Requested Y origin of linear frame buffer.</param>
        /// <param name="pixelPipeline">If <see langword="true"/>, send linear frame buffer writes through the pixel
        /// pipeline.</param>
        /// <param name="info">Structure to be filled with pointer and stride info.</param>
        /// <remarks>
        /// <para>When a Glide application desires direct access to a color or auxiliary buffer, it must lock that
        /// buffer in order to gain access to a pointer to the frame buffer data. This lock may assert a critical code
        /// section which affects process scheduling and precludes the use of GUI debuggers; therefore, time spent doing
        /// direct accesses should be minimized and the lock should be released as soon as possible using the
        /// <see cref="LfbUnlock"/> API. An application may hold multiple simultaneous locks to various buffers,
        /// depending on the underlying hardawre. Application software should always check the return value of
        /// <see cref="LfbLock"/> and take into account the possibility that a lock may fail.</para>
        /// <para>A lock <paramref name="type"/> is a bit field created by the bit-wise OR of one read/write flag and an
        /// optional idle request flag.</para>
        /// <para>An application may attempt to lock any Glide buffer. Currently supported buffer designations are
        /// <see cref="GrBuffer.FrontBuffer"/>, <see cref="GrBuffer.BackBuffer"/>, and <see cref="GrBuffer.AuxBuffer"/>.</para>
        /// <para>Some 3Dfx hardware supports multiple write formats to the linear frame buffer space. An application
        /// may request a particular write format by passing a <paramref name="writeMode"/> argument other than
        /// <see cref="LfbWriteMode.Any"/>. If the destination pixel format specified is not supported on the target
        /// hardware, then the lock will fail.</para>
        /// <para>If the application specifies <see cref="LfbWriteMode.Any"/> and the lock succeeds, the destination
        /// pixel format will be returned in <see cref="LfbInfo.WriteMode"/>. This default destination pixel format
        /// will always be the pixel format that most closely matches the true pixel storage format in the frame buffer.
        /// On Voodoo Graphics and Voodoo Rush, this will always be <see cref="LfbWriteMode.Rgb565"/> for color buffers
        /// and <see cref="LfbWriteMode.ZA16"/> for the auxiliary buffer. The <paramref name="writeMode"/> argument is
        /// ignored for read-only locks.</para>
        /// <para>Some 3Dfx hardware supports a user specified Y origin for LFB writes. An application may request a
        /// particular Y origin by passing an <paramref name="origin"/> argument other than <see cref="Origin.Any"/>. If
        /// the <paramref name="origin"/> specified is not supported on the target hardware, then the lock will fail. If
        /// the application specifies <see cref="Origin.Any"/> and the lock succeeds, the LFB Y origin will be returned
        /// in <see cref="LfbInfo.Origin"/>. The default Y origin will always be <see cref="Origin.UpperLeft"/> for LFB
        /// writes.</para>
        /// <para>Some 3Dfx hardware allows linear frame buffer writes to be processed through the same set of functions
        /// as those pixels generated by the triangle rasterizer. This feature is enabled by passing a value of
        /// <see langword="true"/> in the <paramref name="pixelPipeline"/> argument. If the underlying hardware is
        /// incapable of processing pixels through the pixel pipeline, then the lock will fail. When enabled, color,
        /// alpha, and depth data from the linear frame buffer write will be processed as if it were generated by the
        /// triangle iterators. If the selected <paramref name="writeMode"/> lacks depth information, then the value is
        /// derived from <see cref="LfbConstantDepth"/>. If the <paramref name="writeMode"/> lacks alpha information,
        /// then the value is derived from <see cref="LfbConstantAlpha"/>. Linear frame buffer writes through the pixel
        /// pipeline may not be enabled for auxiliary buffer locks. The <paramref name="pixelPipeline"/> argument is
        /// ignored for read-only locks.</para>
        /// <para>An application may not call any Glide routines other than <see cref="LfbLock"/> and
        /// <see cref="LfbUnlock"/> while any lock is active. Any such calls will result in undefined behavior.</para>
        /// <para>Upon successful completion, the user provided <see cref="LfbInfo"/> structure will be filled in with
        /// information pertaining to the locked buffer. The <see cref="LfbInfo.Size"/> element must be initialized by
        /// the user to the size of the <see cref="LfbInfo"/> structure, e.g.
        /// <c>info.Size = Marshal.SizeOf(typeof(LfbInfo))</c>. This <see cref="LfbInfo.Size"/> element will be used to
        /// provide backward compatibility for future revisions of the API. An unrecognized size will cause the lock to
        /// fail. The <see cref="LfbInfo.LfbPtr"/> element is assigned a valid linear pointer to be used for accessing
        /// the requested buffer. The <see cref="LfbInfo.StrideInBytes"/> element is assigned the byte distance between
        /// scan lines.</para>
        /// </remarks>
        /// <returns><see langword="true"/> if succeeded.</returns>
        /// <seealso cref="LfbUnlock"/>
        /// <seealso cref="LfbConstantAlpha"/>
        /// <seealso cref="LfbConstantDepth"/>
        /// <seealso cref="LfbReadRegion"/>
        /// <seealso cref="LfbWriteRegion"/>
        [DllImport("glide2x", EntryPoint = "_grLfbLock@24")]
        public static extern bool LfbLock(Lock type, GrBuffer buffer, LfbWriteMode writeMode, Origin origin,
            bool pixelPipeline, ref LfbInfo info);

        /// <summary>
        /// Efficiently copies a pixel rectangle into a linaer frame buffer.
        /// </summary>
        /// <param name="srcBuffer">Source frame buffer. Valid values are <see cref="GrBuffer.FrontBuffer"/>,
        /// <see cref="GrBuffer.BackBuffer"/>, and <see cref="GrBuffer.AuxBuffer"/>.</param>
        /// <param name="srcX">Source X coordinate.</param>
        /// <param name="srcY">Source Y coordinates. The origin is always assumed to be at the upper left.</param>
        /// <param name="srcWidth">Width of source rectangle to be copied from the frame buffer.</param>
        /// <param name="srcHeight">Height of source rectangle to be copied from the frame buffer.</param>
        /// <param name="dstStride">Stride, in bytes, of destination user memory buffer.</param>
        /// <param name="dstData">Pointer to destination user memory buffer.</param>
        /// <remarks>
        /// <para>This API copies a rectangle from a region of a frame buffer into a buffer in user memory; this is the
        /// only way to read back from the frame buffer on Scaline Interleaved systems.</para>
        /// <para>A <paramref name="srcWidth"/> by <paramref name="srcHeight"/> rectangle of pixels is copied from the
        /// buffer specified by <paramref name="srcBuffer"/>, starting at the location (<paramref name="srcX"/>,
        /// <paramref name="srcY"/>). The pixels are copied to user memory starting at <paramref name="dstData"/>, with
        /// a stride in bytes defined by <paramref name="dstStride"/>.</para>
        /// <para>The frame buffer Y origin is always assumed to be at the upper left. The pixel data will aways be
        /// 16-bit 565 RBG.</para>
        /// <para>The <paramref name="dstStride"/> must be greater than or equal to <paramref name="srcWidth"/> * 2.</para>
        /// </remarks>
        /// <seealso cref="LfbLock"/>
        /// <seealso cref="LfbUnlock"/>
        /// <seealso cref="LfbConstantAlpha"/>
        /// <seealso cref="LfbConstantDepth"/>
        /// <seealso cref="LfbWriteRegion"/>
        [DllImport("glide2x", EntryPoint = "_grLfbReadRegion@28")]
        public static extern unsafe bool LfbReadRegion(GrBuffer srcBuffer, uint srcX, uint srcY, uint srcWidth,
            uint srcHeight, uint dstStride, void* dstData);
        /// <summary>
        /// Efficiently copies a pixel rectangle into a linaer frame buffer.
        /// </summary>
        /// <param name="srcBuffer">Source frame buffer. Valid values are <see cref="GrBuffer.FrontBuffer"/>,
        /// <see cref="GrBuffer.BackBuffer"/>, and <see cref="GrBuffer.AuxBuffer"/>.</param>
        /// <param name="srcX">Source X coordinate.</param>
        /// <param name="srcY">Source Y coordinates. The origin is always assumed to be at the upper left.</param>
        /// <param name="srcWidth">Width of source rectangle to be copied from the frame buffer.</param>
        /// <param name="srcHeight">Height of source rectangle to be copied from the frame buffer.</param>
        /// <param name="dstStride">Stride, in bytes, of destination user memory buffer.</param>
        /// <param name="dstData">Pointer to destination user memory buffer.</param>
        /// <remarks>
        /// <para>This API copies a rectangle from a region of a frame buffer into a buffer in user memory; this is the
        /// only way to read back from the frame buffer on Scaline Interleaved systems.</para>
        /// <para>A <paramref name="srcWidth"/> by <paramref name="srcHeight"/> rectangle of pixels is copied from the
        /// buffer specified by <paramref name="srcBuffer"/>, starting at the location (<paramref name="srcX"/>,
        /// <paramref name="srcY"/>). The pixels are copied to user memory starting at <paramref name="dstData"/>, with
        /// a stride in bytes defined by <paramref name="dstStride"/>.</para>
        /// <para>The frame buffer Y origin is always assumed to be at the upper left. The pixel data will aways be
        /// 16-bit 565 RBG.</para>
        /// <para>The <paramref name="dstStride"/> must be greater than or equal to <paramref name="srcWidth"/> * 2.</para>
        /// </remarks>
        /// <seealso cref="LfbLock"/>
        /// <seealso cref="LfbUnlock"/>
        /// <seealso cref="LfbConstantAlpha"/>
        /// <seealso cref="LfbConstantDepth"/>
        /// <seealso cref="LfbWriteRegion"/>
        [DllImport("glide2x", EntryPoint = "_grLfbReadRegion@28")]
        public static extern bool LfbReadRegion(GrBuffer srcBuffer, uint srcX, uint srcY, uint srcWidth, uint srcHeight,
            uint dstStride, IntPtr dstData);
        /// <summary>
        /// Efficiently copies a pixel rectangle into a linaer frame buffer.
        /// </summary>
        /// <param name="srcBuffer">Source frame buffer. Valid values are <see cref="GrBuffer.FrontBuffer"/>,
        /// <see cref="GrBuffer.BackBuffer"/>, and <see cref="GrBuffer.AuxBuffer"/>.</param>
        /// <param name="srcX">Source X coordinate.</param>
        /// <param name="srcY">Source Y coordinates. The origin is always assumed to be at the upper left.</param>
        /// <param name="srcWidth">Width of source rectangle to be copied from the frame buffer.</param>
        /// <param name="srcHeight">Height of source rectangle to be copied from the frame buffer.</param>
        /// <param name="dstStride">Stride, in bytes, of the returned pixel data.</param>
        /// <remarks>
        /// <para>This API copies a rectangle from a region of a frame buffer into a buffer in user memory; this is the
        /// only way to read back from the frame buffer on Scaline Interleaved systems.</para>
        /// <para>A <paramref name="srcWidth"/> by <paramref name="srcHeight"/> rectangle of pixels is copied from the
        /// buffer specified by <paramref name="srcBuffer"/>, starting at the location (<paramref name="srcX"/>,
        /// <paramref name="srcY"/>). The pixels are returned with a stride in bytes defined by
        /// <paramref name="dstStride"/>.</para>
        /// <para>The frame buffer Y origin is always assumed to be at the upper left. The pixel data will aways be
        /// 16-bit 565 RBG.</para>
        /// <para>The <paramref name="dstStride"/> must be greater than or equal to <paramref name="srcWidth"/> * 2.</para>
        /// </remarks>
        /// <returns>The pixel data, as 16-bit 565 RGB.</returns>
        /// <seealso cref="LfbLock"/>
        /// <seealso cref="LfbUnlock"/>
        /// <seealso cref="LfbConstantAlpha"/>
        /// <seealso cref="LfbConstantDepth"/>
        /// <seealso cref="LfbWriteRegion"/>
        public static ushort[] LfbReadRegion(GrBuffer srcBuffer, uint srcX, uint srcY, uint srcWidth, uint srcHeight,
            uint dstStride)
        {
            ushort[] dstData = new ushort[dstStride * srcHeight];
            using GCPin dstDataPin = new GCPin(dstData);
            LfbReadRegion(srcBuffer, srcX, srcY, srcWidth, srcHeight, dstStride, dstDataPin);
            return dstData;
        }

        /// <summary>
        /// Unlocks a frame buffer previously locked with <see cref="LfbLock"/>.
        /// </summary>
        /// <param name="writeOnly">Lock type. Valid values are <see cref="Lock.ReadOnly"/> and
        /// <see cref="Lock.WriteOnly"/>.</param>
        /// <param name="backBuffer">Buffer to unlock. Valid values are <see cref="GrBuffer.FrontBuffer"/>,
        /// <see cref="GrBuffer.BackBuffer"/>, and <see cref="GrBuffer.AuxBuffer"/>.</param>
        /// <remarks>
        /// <para>When an application desires direct access to a color or auxiliary buffer, it must lock that buffer in
        /// order to gain access to a pointer to the frame buffer data. When the application has completed its direct
        /// access transactions and would like to restore 3D and GUI engine access to the buffer, then it must call this
        /// method. It is important to note that after a successful call to this method, accessing the
        /// <see cref="LfbInfo.LfbPtr"/> used in the <see cref="LfbLock"/> call will have undefined results.</para>
        /// <para>An application may not call any Glide routines other than <see cref="LfbLock"/> and
        /// <see cref="LfbUnlock"/> while any lock is active.</para>
        /// </remarks>
        /// <returns><see langword="true"/> if succeeded.</returns>
        /// <seealso cref="LfbLock"/>
        /// <seealso cref="LfbConstantAlpha"/>
        /// <seealso cref="LfbConstantDepth"/>
        [DllImport("glide2x", EntryPoint = "_grLfbUnlock@8")]
        public static extern bool LfbUnlock(Lock writeOnly, GrBuffer backBuffer);

        /// <summary>
        /// Selects the current color bfufer for drawing and clearing.
        /// </summary>
        /// <param name="buffer">Selects the current color buffer. Valid values are <see cref="GrBuffer.FrontBuffer"/>
        /// and <see cref="GrBuffer.BackBuffer"/>.</param>
        /// <remarks>
        /// This method selects the buffer for primitive drawing and buffer clears. The default is
        /// <see cref="GrBuffer.BackBuffer"/>.
        /// </remarks>
        /// <seealso cref="BufferClear"/>
        /// <seealso cref="DrawLine"/>
        /// <seealso cref="DrawPoint"/>
        /// <seealso cref="DrawTriangle"/>
        [DllImport("glide2x", EntryPoint = "_grRenderBuffer@4")]
        public static extern void RenderBuffer(GrBuffer buffer);

        /// <summary>
        /// Renders the given <paramref name="frame"/> of the 3Dfx splash screen in the specified rectangle.
        /// </summary>
        /// <param name="x">The X coordinate of the rectangle.</param>
        /// <param name="y">The Y coordinate of the rectangle.</param>
        /// <param name="width">The width of the rectangle.</param>
        /// <param name="height">The height of t he rectangle.</param>
        /// <param name="frame">The index of the frame to render.</param>
        /// <remarks>
        /// <para>This method is not documented officially.</para>
        /// <para>Only the logo is rendered, the background is transparent.</para>
        /// <para>The <paramref name="frame"/> variable loops if it exceeds the last frame.</para>
        /// </remarks>
        [DllImport("glide2x", EntryPoint = "_grSplash@20")]
        public static extern void Splash(float x, float y, float width, float height, uint frame);

        /// <summary>
        /// Performs SST-1 and SST-96 control functions.
        /// </summary>
        /// <param name="mode">The control mode.</param>
        /// <remarks>
        /// <para>This method determines whether the VGA display or Voodoo Graphics display is visible, depending on the
        /// value of <paramref name="mode"/>.</para>
        /// <para>The variable <paramref name="mode"/> specifies one of four values. The first two values apply to all
        /// systems. When <see cref="GrControl.Activate"/> is specified, the Voodoo Graphics frame buffer will be
        /// displayed in full screen mode. On SST-96 systems, the video tile is enabled.</para>
        /// <para>If <paramref name="mode"/> is <see cref="GrControl.Deactivate"/>, the 2D VGA frame buffer is
        /// displayed. On SST-96 systems, the video tile is disabled.</para>
        /// <para><see cref="GrControl.Resize"/> is ignored under SST-1, and SST-96 in full screen mode. For windowed
        /// Glide applications, this call resizes the back buffers and auxiliary buffers, and is typically made by Win32
        /// applications in response to <c>WM_SIZE</c> messages. This method call may fail if there is not enough
        /// offscreen video memory to accommodate the resized buffers.</para>
        /// <para><see cref="GrControl.Move"/> is ignored under SST-1, and SST-96 in full screen mode. For windowed
        /// Glide applications, this call is used to validate the location and clip region associated with the front
        /// buffer when the user moves a window, and is typically made by Win32 applications in response to
        /// <c>WM_MOVE</c> messages. This call may fail if the underlying DirectDraw implementation fails.</para>
        /// <para>On SST-1, since the 2D and 3D Graphcis exist on different devices (and frame buffers), activating or
        /// deactivating pass through does not require you repaint either the 2D or 3D graphics. On the SST-96, the
        /// application is responsible for repainting the 2D graphics or 3D graphics when you use
        /// <see cref="GrControl.Activate"/> or <see cref="GrControl.Deactivate"/>.</para>
        /// </remarks>
        [DllImport("glide2x", EntryPoint = "_grSstControl@4")]
        public static extern bool SstControl(GrControl mode);

        /// <summary>
        /// Returns when the Voodoo Graphics subsystem is idle.
        /// </summary>
        /// <remarks>
        /// This method returns when the Voodoo Graphics subsystem is no longer busy. The system is busy when either the
        /// hardware FIFO is not empty or the graphics engine is busy.
        /// </remarks>
        /// <seealso cref="SstStatus"/>
        [DllImport("glide2x", EntryPoint = "_grSstIdle@0")]
        public static extern void SstIdle();

        /// <summary>
        /// Establishes a Y origin.
        /// </summary>
        /// <param name="origin">Specifies the direction of the Y coordinate axis. <see cref="Origin.UpperLeft"/> places
        /// the screen space origin at the upper left corner of the screen with positive Y going down.
        /// <see cref="Origin.LowerLeft"/> places the screen space origin at the lower left corner of the screen with
        /// positive Y going up.</param>
        /// <remarks>
        /// <para>This method sets the Y origin for all triangle operations, fast fill, and cliipping rectangles.</para>
        /// <para>This method overrides the Y origin specified in <see cref="SstWinOpen"/>.</para>
        /// </remarks>
        /// <seealso cref="SstWinOpen"/>
        [DllImport("glide2x", EntryPoint = "_grSstOrigin@4")]
        public static extern void SstOrigin(Origin origin);

        /// <summary>
        /// Detects and determines the number of 3Dfx Voodoo Graphics subsystems installed in the host system.
        /// </summary>
        /// <param name="hwConfig">Points to a <see cref="HwConfiguration"/> structure where the system's hardware
        /// configuration will be stored.</param>
        /// <remarks>
        /// This method determines the number of installed Voodoo Graphics subsystems and stores this number in
        /// <see cref="HwConfiguration.NumSst"/>. No other information is stored in the structure at this time. This
        /// method may be called before <see cref="GlideInit"/>. <see cref="SstQueryHardware"/> can be called after
        /// <see cref="GlideInit"/> to fill in the rest of the structure.<para/>
        /// This method does not change the state of any hardware, nor does it render any graphics.
        /// </remarks>
        /// <seealso cref="GlideInit"/>
        /// <seealso cref="SstQueryHardware"/>
        [DllImport("glide2x", EntryPoint = "_grSstQueryBoards@4")]
        public static extern bool SstQueryBoards(ref HwConfiguration hwConfig);

        /// <summary>
        /// Detects and determines the nature of any 3Dfx Voodoo Graphics subsystems installed in the host system.
        /// </summary>
        /// <param name="hwConfig">Points to a <see cref="HwConfiguration"/> structure where the system's hardware
        /// configuration will be stored.</param>
        /// <remarks>
        /// This method determines the system's Voodoo Graphics hardware configuration, specifically the number of
        /// installed Voodoo Graphics subsystems and each of their hardware configurations (memory, scan line 
        /// interleaving, etc.).<para/>
        /// This method should be called after <see cref="GlideInit"/> and before <see cref="SstWinOpen"/>.
        /// </remarks>
        /// <returns> 
        /// If no Voodoo Graphics hardware can be found, the method returns <see langword="false"/>; otherwise it
        /// returns <see langword="true"/>.
        /// </returns>
        /// <seealso cref="GlideInit"/>
        /// <seealso cref="SstWinOpen"/>
        /// <seealso cref="SstQueryBoards"/>
        /// <seealso cref="SstSelect"/>
        [DllImport("glide2x", EntryPoint = "_grSstQueryHardware@4")]
        public static extern bool SstQueryHardware(ref HwConfiguration hwConfig);

        /// <summary>
        /// Makes a Voodoo Graphics subsystem current.
        /// </summary>
        /// <param name="whichSst">The ordinal number of the Voodoo Graphics subsystem to make current. This value must
        /// be between 0 and the number of installed subsystems returned by <see cref="SstQueryHardware"/>.</param>
        /// <remarks>
        /// This method selects a particular installed Voodoo Graphics subsystem as active. If the value passed is
        /// greater than the number of installed Voodoo Graphics subsystems, undefined behavior will result.
        /// </remarks>
        /// <seealso cref="SstWinOpen"/>
        /// <seealso cref="SstQueryHardware"/>
        [DllImport("glide2x", EntryPoint = "_grSstSelect@4")]
        public static extern void SstSelect(int whichSst);

        /// <summary>
        /// Opens the graphics display device.
        /// </summary>
        /// <param name="hWnd">Specifies a handle to the window. The interpretation of this value depends on the system
        /// environment. Applications running on SST-1 graphics hardware must specify <c>0</c>. Win32 full screen
        /// applications running on a SST-96 system must specify a window handle; a <c>0</c> value for
        /// <c>hWnd</c> will cause the application's real window handle (i.e. what is returned by
        /// <c>GetActivateWindow</c>) to be used. Since Win32 pure console applications do not have a window handle,
        /// they can be used only with SST-1 and a <c>0</c> window handle is required. Finally, Glide Win32 applications
        /// that run in a window may either specify <c>0</c> (if there is only one window), or the correct hWnd, cast to
        /// <see cref="System.UInt32"/>.</param>
        /// <param name="resolution">Specifies which screen resolutions to use. Refer to
        /// <see cref="Resolution"/> for available resolutions, e.g. <see cref="Resolution.Res640x480"/>
        /// and <see cref="Resolution.Res800x600"/>. In addition, the resolution
        /// <see cref="Resolution.None"/> is permitted for the SST-96. This signals Glide to use the user
        /// specified window (see the <paramref name="hWnd"/> parameter). Specifying
        /// <see cref="Resolution.None"/> on an SST-1 system will cause the call to fail.</param>
        /// <param name="refresh">Specifies the refresh rate to use. Refer to <see cref="Refresh"/> for
        /// available video refresh rates, e.g. <see cref="Refresh.Ref60Hz"/> and
        /// <see cref="Refresh.Ref72Hz"/>. The parameter is ignored when a Win32 application is running in a
        /// window (SST-96 systems only).</param>
        /// <param name="colorFormat">Specifies the packed color RGBA ordering for linear frame buffer writes and
        /// parameters for the following APIs: <see cref="BufferClear"/>, <see cref="ChromakeyValue"/>,
        /// <see cref="ConstantColorValue"/>, and <see cref="FogColorValue"/>.</param>
        /// <param name="originLocation">Specifies the direction of the Y coordinate axis.
        /// <see cref="Origin.UpperLeft"/> places the screen space origin at the upper left corner of the
        /// screen with positive Y going down (a la IBM VGA). <see cref="Origin.LowerLeft"/> places the screen origin
        /// at the lower left corner of the screen with positive Y going up (a la SGI GL).</param>
        /// <param name="numBuffers">Specifies the number of rendering buffers to use. Supports values <c>2</c> (double-
        /// buffering) or <c>3</c> (triple-buffering). If there is not enough memory to support the desired resolution
        /// (e.g. 800x600 triple buffered on a 2MB system), an error will occur.</param>
        /// <param name="numAuxBuffers">Specifies the number of auxiliary buffers required by an application. The
        /// auxiliary buffers are used either for depth or alpha buffering. Permitted values are <c>0</c> or <c>1</c>.
        /// For full screen applications, this parameter allows both SST-1 and SST-96 to validate whether the available
        /// video memory will support the application's requirements for color and auxiliary buffers at a specified
        /// screen resolution. For a windowed application running on SST-96, this parameter allows an application to run
        /// in a larger 3D window if a depth buffer is not necessary (depth and back buffers share the same off-screen
        /// video memory).</param>
        /// <remarks>
        /// This method initializes the graphics to a known state using the given parameters. It supports both SST-1 and
        /// SST-96, and either full-screen or windowed operation in the latter. By default all special effects of the
        /// hardware (depth buffering, fog, chroma-key, alpha blending, alpha testing, etc.) are disabled and must be
        /// individually enabled. All global state constants (chroma-key value, alpha test reference, constant depth
        /// value, constant alpha value, etc.) and pixel rendering statistic counters are initialized to <c>0x00</c>.
        /// <para/>
        /// <see cref="SstWinOpen"/> initializes various Glide state variables, including hints. Thus,
        /// <see cref="Hints"/> should be called after <see cref="SstWinOpen"/> if you want something other than the
        /// default settings.
        /// </remarks>
        /// <returns>
        /// Upon success, a value of <see langword="true"/> is returned; otherwise a value of <see langword="false"/>
        /// is returned. If <see cref="WinOpen"/> is called on an already open window, <see langword="false"/> will be
        /// returned. This routine replaces the obsolete <see cref="SstOpen"/> call from previous versions of Glide.
        /// </returns>
        /// <seealso cref="SstControlMode"/>
        /// <seealso cref="SstQueryHardware"/>
        /// <seealso cref="SstResetPerfStats"/>
        /// <seealso cref="SstWinClose"/>
        [DllImport("glide2x", EntryPoint = "_grSstWinOpen@28")]
        public static extern bool SstWinOpen(uint hWnd, Resolution resolution, Refresh refresh, ColorFormat colorFormat,
            Origin originLocation, int numBuffers, int numAuxBuffers);

        /// <summary>
        /// Returns the texture memory consumed by a texture.
        /// </summary>
        /// <param name="smallLod">The smallest LOD in the mipmap.</param>
        /// <param name="largeLod">The largest LOD in the mipmap.</param>
        /// <param name="aspect">Aspect ratio of the mipmap.</param>
        /// <param name="format">Format of the mipmap.</param>
        /// <remarks>
        /// The value returned includes memory for both the even and odd mipmap levels. In the case where a mipmap is
        /// split across two TMUs with the even levels in one TMU and the odd levels in the other TMU, use
        /// <see cref="TexTextureMemRequired"/> to compute the memory requirements of each TMU.
        /// </remarks>
        /// <returns>
        /// The amount of memory a mipmap of the specified LOD range, aspect ratio, and format requires. Because of the
        /// packing requirements of some texture formats, the number returned may reflect padding bytes required for the
        /// mipmap.
        /// </returns>
        /// <seealso cref="TexTextureMemRequired"/>
        [DllImport("glide2x", EntryPoint = "_grTexCalcMemRequired@16")]
        public static extern uint TexCalcMemRequired(Lod smallLod, Lod largeLod, Aspect aspect, TextureFormat format);

        /// <summary>
        /// Configures a texture combine unit.
        /// </summary>
        /// <param name="tmu">Texture Mapping Unit to modify. Valid values are <see cref="ChipID.Tmu0"/>,
        /// <see cref="ChipID.Tmu1"/>, and <see cref="ChipID.Tmu2"/>.</param>
        /// <param name="rgbFunction">Specifies the function used in texture color generation.</param>
        /// <param name="rgbFactor">Specifies the scaling factor <i>f</i> used in texture color generation.</param>
        /// <param name="alphaFunction">Specifies the function used in texture alpha generation.</param>
        /// <param name="alphaFactor">Specifies the scaling factor <i>f</i> used in texture alpha generation.</param>
        /// <param name="rgbInvert">Specifies whether the generated texture color should be bitwise inverted as a final
        /// step.</param>
        /// <param name="alphaInvert">Specifies whether the generated texture alpha should be bitwise inverted as a
        /// final step.</param>
        /// <remarks>
        /// <para>This method configures the color and alpha texture combine units of the Voodoo Graphics hardware
        /// pipeline. This provides a low level mechanism for controlling all the modes of the texture combine unit
        /// without mainpulating individual register bits.</para>
        /// <para>The texture combine unit computes the function specified by the <paramref name="rgbFunction"/> and
        /// <paramref name="alphaFunction"/> combining functions and the <paramref name="rgbFactor"/> and
        /// <paramref name="alphaFactor"/> scale factors on the local filtered texel (C_local and A_local) and the
        /// filtered texel from the upstream TMU (C_other and A_other). The result is clamped to [0..255], and then a
        /// bitwise inversion may be applied, controlled by the <paramref name="rgbInvert"/> and
        /// <paramref name="alphaInvert"/> parameters. The final result is then passed downstream, to either another TMU
        /// or the Pixelfx chip.</para>
        /// <para>This method also tracks required vertex parameters for the rendering routines.
        /// <see cref="CombineFactor.None"/> indicates that no parameters are required; it is functionally equivalent to
        /// <see cref="CombineFactor.Zero"/>.</para>
        /// <para>C_local and A_local are the color components generated by indexing and filtering from the mipmap
        /// stored on the selected TMU; C_other and A_other are the incoming color components from the neighboring TMU.</para>
        /// <para>Inverting the bits in a color is the same as computing (1.0 - color) for floating point color values 
        /// in the range [0..1] or (255 - color) for 8-bit color values in the range [0..255].</para>
        /// <para>The TMU closest to the Pixelfx chip is <see cref="ChipID.Tmu0"/>. If a TMU exists upstream from
        /// <see cref="ChipID.Tmu0"/>, it is <see cref="ChipID.Tmu1"/>. If a TMU exists upstream from
        /// <see cref="ChipID.Tmu1"/>, it is <see cref="ChipID.Tmu2"/>.</para>
        /// </remarks>
        /// <seealso cref="DrawTriangle"/>
        /// <seealso cref="TexLodBiasValue"/>
        /// <seealso cref="TexDetailControl"/>
        [DllImport("glide2x", EntryPoint = "_grTexCombine@28")]
        public static extern void TexCombine(ChipID tmu, CombineFunction rgbFunction, CombineFactor rgbFactor,
            CombineFunction alphaFunction, CombineFactor alphaFactor, bool rgbInvert, bool alphaInvert);

        /// <summary>
        /// Sets the detail texturing controls.
        /// </summary>
        /// <param name="tmu">Texture Mapping Unit to modify. Valid values are <see cref="ChipID.Tmu0"/>,
        /// <see cref="ChipID.Tmu1"/>, and <see cref="ChipID.Tmu2"/>.</param>
        /// <param name="lodBias">Controls where the blending between the two textures begins. This value is an LOD bias
        /// value in the range [-32..+31].</param>
        /// <param name="detailScale">Controls the steepness of the blend. Values in the range [0..7] are valid. The
        /// scale is computed as 2 ^ <paramref name="detailScale"/>.</param>
        /// <param name="detailMax">Controls the maximum blending that occurs. Values in the range [0...1] are valid.</param>
        /// <remarks>
        /// <para>Detail texturing refers to the effect where the blend between two textures in a texture combine unit
        /// is a function of the LOD calculated for each pixel. This method controls how the detail blending factor beta
        /// is computed from LOD. The <paramref name="lodBias"/> parameter controls where the blending begins, the
        /// <paramref name="detailScale"/> parameter controls the steepness of the blend (how fast the detail pops in),
        /// and the <paramref name="detailMax"/> parameter controls the maximum blending that occurs. Detail blending
        /// factor beta is calculated as
        /// <code>beta = min(detailMax, max(0, (loadBias - LOD) &lt;&lt; detailScale) / 255.0)</code>
        /// where LOD is the calculated LOD before <see cref="TexLodBiasValue"/> is added. The detail blending factor is
        /// typically used by calling <see cref="TexCombine"/> with an rgbFunction of <see cref="CombineFunction.Blend"/>
        /// and an rgbFactor of <see cref="CombineFactor.DetailFactor"/> to compute:
        /// <code>C_out = beta * detailTexture + (1 - beta) * mainTexture</code></para>
        /// <para>An LOD of <i>n</i> is calculated when a pixel covers approximately <i>2^2n</i> texels. For example,
        /// when a pxiel covers approximately 1 texel, the LOD is 0. When a pixel covers 4 texels, the LOD is 1, and
        /// when a pixel covers 16 texels, the LOD is 2.</para>
        /// <para>Detail blending occurs in the downstream TMU. Since the detail texture and main texture typically have
        /// very different computed LODs, the detail texturing control settings depend on which texture is in the
        /// downstream TMU.</para>
        /// </remarks>
        /// <seealso cref="TexCombine"/>
        /// <seealso cref="TexLodBiasValue"/>
        [DllImport("glide2x", EntryPoint = "_grTexDetailControl@16")]
        public static extern void TexDetailControl(ChipID tmu, int lodBias, byte detailScale, float detailMax);

        /// <summary>
        /// Downloads a complete mipmap to texture memory.
        /// </summary>
        /// <param name="tmu">Texture Mapping Unit to modify. Valid values are <see cref="ChipID.Tmu0"/>,
        /// <see cref="ChipID.Tmu1"/>, and <see cref="ChipID.Tmu2"/>.</param>
        /// <param name="startAddress">Offset into texture memory where the texture will be loaded.</param>
        /// <param name="evenOdd">Which mipmap levels to download.</param>
        /// <param name="info">Format, dimensions, and image data for texture.</param>
        /// <remarks>
        /// <para>This method downloads an entire mipmap to an area of texture memory specified by
        /// <paramref name="startAddress"/>. Valid values for <paramref name="startAddress"/> must be between the values
        /// returned by <see cref="TexMinAddress"/> and <see cref="TexMaxAddress"/> and must be 8-byte aligned.</para>
        /// <para>An error will occur if the mipmap is loaded into an area that crosses a 2 MB boundary. See the
        /// Glide Programming Manual for more informatin.</para>
        /// </remarks>
        /// <seealso cref="TexDownloadMipMapLevel"/>
        /// <seealso cref="TexMinAddress"/>
        /// <seealso cref="TexMaxAddress"/>
        /// <seealso cref="TexTextureMemRequired"/>
        /// <seealso cref="TexSource"/>
        [DllImport("glide2x", EntryPoint = "_grTexDownloadMipMap@16")]
        public static extern void TexDownloadMipMap(ChipID tmu, uint startAddress, MipMapLevelMask evenOdd,
            ref TexInfo info);

        /// <summary>
        /// Downloads an NCC table.
        /// </summary>
        /// <param name="tmu">Texture Mapping Unit to modify. Valid values are <see cref="ChipID.Tmu0"/>,
        /// <see cref="ChipID.Tmu1"/>, and <see cref="ChipID.Tmu2"/>.</param>
        /// <param name="type">Type of texture table. Valid values are <see cref="TexTableType.Ncc0"/> and
        /// <see cref="TexTableType.Ncc1"/>.</param>
        /// <param name="data">Table data.</param>
        /// <remarks>
        /// <para>This method downloads an NCC table to a TMU. There are two NCC tables on each TMU. One of two NCC
        /// tables is used when decompressing texture formats <see cref="TextureFormat.FormatYiq422"/> or
        /// <see cref="TextureFormat.FormatAyiq8422"/>. Which NCC table is used for decompression is specified by
        /// <see cref="TexNccTable"/>.</para>
        /// <para><see cref="TexSource"/> does not download a texture's table - this must be done separately using this
        /// method.</para>
        /// <para>This method does not download an NCC table if the table address is the same as the last table
        /// downloaded. Therefore, if the table's data has changed, it must be copied to a new address before
        /// downloading.</para>
        /// </remarks>
        /// <seealso cref="TexDownloadTablePartial"/>
        /// <seealso cref="TexNccTable"/>
        /// <seealso cref="TexSource"/>
        [DllImport("glide2x", EntryPoint = "_grTexDownloadTable@12")]
        public static extern void TexDownloadTable(ChipID tmu, TexTableType type, ref NccTable data);
        /// <summary>
        /// Downloads a color palette.
        /// </summary>
        /// <param name="tmu">Texture Mapping Unit to modify. Valid values are <see cref="ChipID.Tmu0"/>,
        /// <see cref="ChipID.Tmu1"/>, and <see cref="ChipID.Tmu2"/>.</param>
        /// <param name="type">Type of texture table. Valid values are <see cref="TexTableType.Palette"/>.</param>
        /// <param name="data">Table data.</param>
        /// <remarks>
        /// <para>This method downloads a 256-entry color palette to a TMU. The color palette is referenced when
        /// rendering texture formats <see cref="TextureFormat.FormatP8"/> or <see cref="TextureFormat.FormatAp88"/>.</para>
        /// <para><see cref="TexSource"/> does not download a texture's table - this must be done separately using this
        /// method.</para>
        /// </remarks>
        /// <seealso cref="TexDownloadTablePartial"/>
        /// <seealso cref="TexSource"/>
        [DllImport("glide2x", EntryPoint = "_grTexDownloadTable@12")]
        public static extern void TexDownloadTable(ChipID tmu, TexTableType type, ref TexPalette data);
        /// <summary>
        /// Downloads an NCC table or color palette.
        /// </summary>
        /// <param name="tmu">Texture Mapping Unit to modify. Valid values are <see cref="ChipID.Tmu0"/>,
        /// <see cref="ChipID.Tmu1"/>, and <see cref="ChipID.Tmu2"/>.</param>
        /// <param name="type">Type of texture table.</param>
        /// <param name="data">Table data, either of type <see cref="NccTable"/> or <see cref="TexPalette"/>.</param>
        /// <remarks>
        /// <para>This method downloads either an NCC table or a 256-entry color palette to a TMU. There are two NCC
        /// tables and one color palette on each TMU. The color palette is referenced when rendering texture formats
        /// <see cref="TextureFormat.FormatP8"/> or <see cref="TextureFormat.FormatAp88"/>. One of two NCC tables is
        /// used when decompressing texture formats <see cref="TextureFormat.FormatYiq422"/> or
        /// <see cref="TextureFormat.FormatAyiq8422"/>. Which NCC table is used for decompression is specified by
        /// <see cref="TexNccTable"/>.</para>
        /// <para><see cref="TexSource"/> does not download a texture's table - this must be done separately using this
        /// method.</para>
        /// <para>This method does not download an NCC table if the table address is the same as the last table
        /// downloaded. Therefore, if the table's data has changed, it must be copied to a new address before
        /// downloading.</para>
        /// </remarks>
        /// <seealso cref="TexDownloadTablePartial"/>
        /// <seealso cref="TexNccTable"/>
        /// <seealso cref="TexSource"/>
        [DllImport("glide2x", EntryPoint = "_grTexDownloadTable@12")]
        public static extern void TexDownloadTable(ChipID tmu, TexTableType type, ref TexTable data);

        /// <summary>
        /// Specifies the texture minification and magnification filters.
        /// </summary>
        /// <param name="tmu">Texture Mapping Unit to modify. Valid values are <see cref="ChipID.Tmu0"/>,
        /// <see cref="ChipID.Tmu1"/>, and <see cref="ChipID.Tmu2"/>.</param>
        /// <param name="minFilterMode">The minification filter.</param>
        /// <param name="magFilterMode">The magification filter.</param>
        /// <remarks>
        /// This method specifies the texture filters for minifaction and magnification. The magnification filter is
        /// used when the LOD calculate for a pixel indicates that the pixel covers less than one texel. Otherwise, the
        /// minification filter is used.
        /// </remarks>
        /// <seealso cref="TexSource"/>
        [DllImport("glide2x", EntryPoint = "_grTexFilterMode@12")]
        public static extern void TexFilterMode(ChipID tmu, TextureFilter minFilterMode, TextureFilter magFilterMode);

        /// <summary>
        /// Returns the highest start address for texture downloads.
        /// </summary>
        /// <param name="tmu">Texture Mapping Unit to query. Valid values are <see cref="ChipID.Tmu0"/>,
        /// <see cref="ChipID.Tmu1"/>, and <see cref="ChipID.Tmu2"/>.</param>
        /// <remarks>
        /// This method returns the upper bound on texture memory addresses for a specific TMU.
        /// </remarks>
        /// <returns>
        /// The returned address is the highest valid texture start address and is valid only for the smallest mipmap
        /// level <see cref="Lod.Lod1"/>.
        /// </returns>
        /// <seealso cref="TexMinAddress"/>
        /// <seealso cref="TexDownloadMipMap"/>
        /// <seealso cref="TexDownloadMipMapLevel"/>
        /// <seealso cref="TexSource"/>
        [DllImport("glide2x", EntryPoint = "_grTexMaxAddress@4")]
        public static extern uint TexMaxAddress(ChipID tmu);

        /// <summary>
        /// Returns the lowest start address for texture downloads.
        /// </summary>
        /// <param name="tmu">Texture Mapping Unit to modify. Valid values are <see cref="ChipID.Tmu0"/>,
        /// <see cref="ChipID.Tmu1"/>, and <see cref="ChipID.Tmu2"/>.</param>
        /// <returns>
        /// The lower bound on texture memory addresses for a specific TMU.
        /// </returns>
        /// <seealso cref="TexMaxAddress"/>
        /// <seealso cref="TexDownloadMipMap"/>
        /// <seealso cref="TexDownloadMipMapLevel"/>
        /// <seealso cref="TexSource"/>
        [DllImport("glide2x", EntryPoint = "_grTexMinAddress@4")]
        public static extern uint TexMinAddress(ChipID tmu);

        /// <summary>
        /// Sets the mipmapping mode.
        /// </summary>
        /// <param name="tmu">Texture Mapping Unit to modify. Valid values are <see cref="ChipID.Tmu0"/>,
        /// <see cref="ChipID.Tmu1"/>, and <see cref="ChipID.Tmu2"/>.</param>
        /// <param name="mode">The new mipmapping mode.</param>
        /// <param name="lodBlend"><see langword="true"/> enables blending between levels of detail when doing trilinear
        /// mapping. <see langword="false"/> disables LOD blending.</param>
        /// <remarks>
        /// <para>This method sets the mipmapping mode for the Voodoo Graphics hardware. The Voodoo Graphics hardware
        /// performs mipmapping with no performance penalty. Either no mipmapping, nearest mipmapping, or nearest
        /// dithered mipmapping can be performed. Nearest mipmapping (<see cref="MipMap.Nearest"/>) selects the nearest
        /// mipmap based on LOD. Dithered nearest mipmapping (<see cref="MipMap.NearestDithered"/>) dithers between
        /// adjacent mipmap levels to reduce the effects of mipmap banding but without the cost of trilinear filtering
        /// with LOD blending.</para>
        /// <para><see cref="MipMap.NearestDithered"/> mode can degrade fill-rate performance by 20-30%. This is not
        /// always the case, as it is very application dependent. If this mode is used, performance should be
        /// benchmarked to determine the cost of the increased quality.</para>
        /// <para><see cref="MipMap.Nearest"/> truncates the LOD calculated for each pixel. To round to the nearest LOD,
        /// set the LOD bias value to 0.5 with <see cref="TexLodBiasValue"/>.</para>
        /// <para><see cref="MipMap.Nearest"/> should be used when <paramref name="lodBlend"/> is
        /// <see langword="true"/>.</para>
        /// </remarks>
        /// <seealso cref="TexLodBiasValue"/>
        /// <seealso cref="TexSource"/>
        [DllImport("glide2x", EntryPoint = "_grTexMipMapMode@12")]
        public static extern void TexMipMapMode(ChipID tmu, MipMap mode, bool lodBlend);

        /// <summary>
        /// Specifies the current texture source for rendering.
        /// </summary>
        /// <param name="tmu">Texture Mapping Unit to modify. Valid values are <see cref="ChipID.Tmu0"/>,
        /// <see cref="ChipID.Tmu1"/>, and <see cref="ChipID.Tmu2"/>.</param>
        /// <param name="startAddress">Starting address in texture memory for texture.</param>
        /// <param name="evenOdd">Which mipmap levels have been download at <paramref name="startAddress"/>.</param>
        /// <param name="texInfo">Format and dimensions of the new texture.</param>
        /// <remarks>
        /// <para>This method sets up the area of texture memory that is to be used as a source for subsequent texture
        /// mapping operations. The <paramref name="startAddress"/> specified shuold be the same as the
        /// <paramref name="startAddress"/> argument to <see cref="TexDownloadMipMap"/>, or the starting address used
        /// for the largest mipmap level when using <see cref="TexDownloadMipMapLevel"/>.</para>
        /// <para>An error will occur if a mipmap level is loaded into an area that crosses a 2 MB boundary. See the
        /// Glide Programming Manual for more information.</para>
        /// </remarks>
        /// <seealso cref="TexDownloadMipMap"/>
        /// <seealso cref="TexDownloadMipMapLevel"/>
        /// <seealso cref="TexMinAddress"/>
        /// <seealso cref="TexMaxAddress"/>
        /// <seealso cref="TexTextureMemRequired"/>
        [DllImport("glide2x", EntryPoint = "_grTexSource@16")]
        public static extern void TexSource(ChipID tmu, uint startAddress, MipMapLevelMask evenOdd,
            ref TexInfo texInfo);

        /// <summary>
        /// Returns the texture memory consumed by a texture.
        /// </summary>
        /// <param name="evenOdd">Which mipmap levels are included.</param>
        /// <param name="info">Format and dimensions of the texture.</param>
        /// <returns>
        /// The number of bytes required to store a given texture. The number returned may be added to the
        /// start address for a texture download to determine the next free location in texture memory.
        /// </returns>
        /// <seealso cref="TexCalcMemRequired"/>
        /// <seealso cref="TexDownloadMipMap"/>
        /// <seealso cref="TexDownloadMipMapLevel"/>
        /// <seealso cref="TexMinAddress"/>
        /// <seealso cref="TexMaxAddress"/>
        /// <seealso cref="TexSource"/>
        [DllImport("glide2x", EntryPoint = "_grTexTextureMemRequired@8")]
        public static extern uint TexTextureMemRequired(MipMapLevelMask evenOdd, ref TexInfo info);
    }
}
