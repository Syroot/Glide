﻿namespace Syroot.Glide.Glide2
{
    public enum TextureFormat
    {
        Format8Bit,
        FormatYiq422,
        /// <summary>(0..0xFF) alpha</summary>
        FormatAlpha8,
        /// <summary>(0..0xFF) intensity</summary>
        FormatIntensity8,
        FormatAlphaIntensity44,
        /// <summary>8-bit palette</summary>
        FormatP8,
        FormatRsvd0,
        FormatRrsvd1,
        Format16Bit,
        FormatAyiq8422,
        FormatRgb565,
        FormatArgb1555,
        FormatArgb4444,
        FormatAlphaIntensity88,
        /// <summary>8-bit alpha 8-bit palette</summary>
        FormatAp88,
        FormatRsvd2,
        FormatRgb332 = Format8Bit,
        FormatArgb8332 = Format16Bit
    }
}
