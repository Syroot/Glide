﻿namespace Syroot.Glide.Glide2
{
    public enum ColorCombine
    {
        Zero,
        CcRgb,
        ItRgb,
        ItRgbDelta0,
        DecalTexture,
        TextureTimesCcRgb,
        TextureTimesItRgb,
        TextureTimesItRgbDelta0,
        TextureTimesItRgbAddAlpha,
        TextureTimesAlpha,
        TextureTimesAlphaAddItRgb,
        TextureAddItRgb,
        TextureSubItRgb,
        CcRgbBlendItRgbOnTexAlpha,
        DiffSpecA,
        DiffSpecB,
        One
    }
}
