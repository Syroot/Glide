﻿using System;

namespace Syroot.Glide.Glide2
{
    public enum WDepthValue : ushort
    {
        Nearest = UInt16.MinValue,
        Farthest = UInt16.MaxValue
    }
}
