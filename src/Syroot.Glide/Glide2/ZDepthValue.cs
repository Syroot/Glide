﻿using System;

namespace Syroot.Glide.Glide2
{
    /// <remarks>
    /// The constants <see cref="Nearest"/> and <see cref="Farthest"/> assume that depth values decrease as they get
    /// further away from the eye. However, any linear function of <c>1 / Z</c> can be used for computing depth buffer
    /// values and therefore they can either increase or decrease with distance from the eye.
    /// </remarks>
    public enum ZDepthValue : ushort
    {
        Nearest = UInt16.MaxValue,
        Farthest = UInt16.MinValue
    }
}
