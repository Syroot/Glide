﻿using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2
{
    [StructLayout(LayoutKind.Sequential)]
    public struct VoodooConfig
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        /// <summary>1, 2, or 4 MB.</summary>
        public int FbRam;
        /// <summary>Rev of Pixelfx chip.</summary>
        public int FbIRev;
        /// <summary>How many texelFX chips are there?</summary>
        public int NTexelFx;
        /// <summary>Is it a scan-line interleaved board?</summary>
        public bool SliDetect;
        private TmuConfig _tmuConfig0;
        private TmuConfig _tmuConfig1;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the configuration of the Texelfx chips.
        /// </summary>
        public TmuConfig[] TmuConfig => new[/*GLIDE_NUM_TMU*/] { _tmuConfig0, _tmuConfig1 };
    }
}
