﻿namespace Syroot.Glide.Glide2
{
    /// <summary>
    /// Represents frame buffer pixel formats.
    /// </summary>
    public enum LfbWriteMode
    {
        /// <summary>
        /// <para>RGB:RGB</para>
        /// <para>Frame buffer accepts 16-bit RGB 565 pixel data.</para>
        /// </summary>
        Rgb565,
        /// <summary>
        /// <para>RGB:RGB</para>
        /// <para>Frame buffer accepts 16-bit RGB 555 pixel data. The MSB of each pixel is ignored.</para>
        /// </summary>
        Rgb555,
        /// <summary>
        /// <para>ARGB:ARGB</para>
        /// <para>Frame buffer accepts 16-bit ARGB 1555 pixel data. The alpha component is replicated to 8-bits and
        /// copied to the alpha buffer if the alpha buffer has been enabled with <see cref="Gr.ColorMask"/>.</para>
        /// </summary>
        Rgb1555,
        Reserved1,
        /// <summary>
        /// <para>RGB</para>
        /// <para>Frame buffer accepts 24-bit RGB 888 pixel data packet into 32-bit words. The most significant byte of
        /// each word is ignored. If dithering is enabled, then color will be dithered down to the real frame buffer
        /// storage format if necessary.</para>
        /// </summary>
        Rgb888,
        /// <summary>
        /// <para>ARGB</para>
        /// <para>Frame buffer accepts 32-bit ARGB 8888 pixel data. The alpha component is copied into the alpha buffer
        /// if the alpha buffer  has been enabled with <see cref="Gr.ColorMask"/>. If dithering is enabled, then color
        /// will be dithered down to the real frame buffer storage format if necessary.</para>
        /// </summary>
        Rgb8888,
        Reserved2,
        Reserved3,
        Reserved4,
        Reserved5,
        Reserved6,
        Reserved7,
        /// <summary>
        /// <para>RGB:Depth</para>
        /// <para>Frame buffer accepts 32-bit pixels where the two most significant bytes contain 565 RGB data, and the
        /// two least significant bytes contain 16-bit depth data.</para>
        /// </summary>
        Rgb565Depth,
        /// <summary>
        /// <para>RGB:Depth</para>
        /// <para>Frame buffer accepts 32-bit pixels where the two most significant bytes contain 555 RGB data, the most
        /// significant bit is ignored, and the two least significant bytes contain 16-bit depth data.</para>
        /// </summary>
        Rgb555Depth,
        /// <summary>
        /// <para>ARGB:Depth</para>
        /// <para>Frame buffer accepts 32-bit pixels where the two most significant bytes contain 1555 ARGB data, the
        /// alpha component is replicated to 8-bits and copied to the alpha buffer if alpha buffering has been enabled
        /// with <see cref="Gr.ColorMask"/>.</para>
        /// </summary>
        Rgb1555Depth,
        /// <summary>
        /// <para>Depth:Depth</para>
        /// <para>Frame buffer accepts 16-bit auxiliary buffer values. This is the only write mode that is valid when
        /// locking the auxiliary buffer. Alpha buffer values are taken from the 8 least significant bits of each
        /// sixteen bit word.</para>
        /// </summary>
        ZA16,
        /// <summary>
        /// Lock will return the pixel format that most closely matches the true frame buffer storage format in the
        /// <see cref="LfbInfo.WriteMode"/>.
        /// </summary>
        Any = 0xFF
    }
}
