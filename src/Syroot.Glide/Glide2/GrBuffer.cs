﻿namespace Syroot.Glide.Glide2
{
    public enum GrBuffer
    {
        FrontBuffer,
        BackBuffer,
        AuxBuffer,
        DepthBuffer,
        AlphaBuffer,
        TripleBuffer
    }
}
