﻿namespace Syroot.Glide.Glide2
{
    /// <summary>
    /// Represents blending functions.
    /// </summary>
    public enum Blend
    {
        /// <summary>0</summary>
        Zero,
        /// <summary>A_src / 255</summary>
        SrcAlpha,
        /// <summary>C_src / 255</summary>
        SrcColor,
        /// <summary>A_dst / 255</summary>
        DstAlpha,
        /// <summary>1</summary>
        One,
        /// <summary>1 - A_src / 255</summary>
        OneMinusSrcAlpha,
        /// <summary>1 - C_src / 255</summary>
        OneMinusSrcColor,
        /// <summary>1 - A_dst / 255</summary>
        OneMinusDstAlpha,
        Reserved8,
        Reserved9,
        ReservedA,
        ReservedB,
        ReservedC,
        ReservedD,
        ReservedE,
        /// <summary>min(A_src / 255, 1 - A_dst / 255)</summary>
        AlphaSaturate,
        /// <summary>C_dst / 255</summary>
        DstColor = SrcColor,
        /// <summary>1 - C_dst / 255</summary>
        OneMinusDstColor = OneMinusSrcColor,
        /// <summary>Color before fog is applied.</summary>
        PreFogColor = AlphaSaturate
    }
}
