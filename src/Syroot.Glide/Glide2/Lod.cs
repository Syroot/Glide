﻿namespace Syroot.Glide.Glide2
{
    public enum Lod
    {
        Lod256,
        Lod128,
        Lod64,
        Lod32,
        Lod16,
        Lod8,
        Lod4,
        Lod2,
        Lod1
    }
}
