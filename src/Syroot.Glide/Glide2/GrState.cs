﻿using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2
{
    [StructLayout(LayoutKind.Sequential)]
    public struct GrState
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 312/*GLIDE_STATE_PAD_SIZE*/)]
        public byte[] Pad;
    }
}
