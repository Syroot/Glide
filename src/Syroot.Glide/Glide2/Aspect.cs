﻿namespace Syroot.Glide.Glide2
{
    public enum Aspect
    {
        /// <summary>8W x 1H</summary>
        Ratio8x1,
        /// <summary>4W x 1H</summary>
        Ratio4x1,
        /// <summary>2W x 1H</summary>
        Ratio2x1,
        /// <summary>1W x 1H</summary>
        Ratio1x1,
        /// <summary>1W x 2H</summary>
        Ratio1x2,
        /// <summary>1W x 4H</summary>
        Ratio1x4,
        /// <summary>1W x 8H</summary>
        Ratio1x8
    }
}
