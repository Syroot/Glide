﻿using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2
{
    [StructLayout(LayoutKind.Sequential)]
    public struct TmuVertex
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        /// <summary>S texture coordinate (S over W).</summary>
        public float SoW;
        /// <summary>T texture coordinate (T over W).</summary>
        public float ToW;
        /// <summary>1 / W (used mipmapping - really 0xFFF / W).</summary>
        public float OoW;
    }
}
