﻿using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Vertex
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        /// <summary>X of screen space.</summary>
        public float X;
        /// <summary>Y of screen space.</summary>
        public float Y;
        /// <summary>Z of screen space (ignored).</summary>
        public float Z;
        /// <summary>Red ([0..255.0]).</summary>
        public float R;
        /// <summary>Green ([0..255.0]).</summary>
        public float G;
        /// <summary>Blue ([0..255.0]).</summary>
        public float B;
        /// <summary>65535 / Z (used for Z-buffering).</summary>
        public float OoZ;
        /// <summary>Alpha [0..255.0].</summary>
        public float A;
        /// <summary>1 / W (used for W-buffering, texturing).</summary>
        public float OoW;
        /// <summary>First TMU vertex.</summary>
        public TmuVertex TmuVtx0;
        /// <summary>Second TMU vertex.</summary>
        public TmuVertex TmuVtx1;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Vertex"/> struct with the given values.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Vertex(float x, float y)
        {
            X = x;
            Y = y;
            Z = default;
            R = default;
            G = default;
            B = default;
            OoZ = default;
            A = default;
            OoW = default;
            TmuVtx0 = default;
            TmuVtx1 = default;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vertex"/> struct with the given values.
        /// </summary>
        /// <param name="x">Value of the X coordinate.</param>
        /// <param name="y">Value of the Y coordinate.</param>
        /// <param name="r">Amount of red color (0..255).</param>
        /// <param name="g">Amount of green color (0..255).</param>
        /// <param name="b">Amount of blue color (0..255).</param>
        public Vertex(float x, float y, float r, float g, float b)
        {
            X = x;
            Y = y;
            Z = default;
            R = r;
            G = g;
            B = b;
            OoZ = default;
            A = default;
            OoW = default;
            TmuVtx0 = default;
            TmuVtx1 = default;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vertex"/> struct with the given values.
        /// </summary>
        /// <param name="x">Value of the X coordinate.</param>
        /// <param name="y">Value of the Y coordinate.</param>
        /// <param name="r">Amount of red color (0..255).</param>
        /// <param name="g">Amount of green color (0..255).</param>
        /// <param name="b">Amount of blue color (0..255).</param>
        /// <param name="a">Transparency (0..255).</param>
        public Vertex(float x, float y, float r, float g, float b, float a)
        {
            X = x;
            Y = y;
            Z = default;
            R = r;
            G = g;
            B = b;
            OoZ = default;
            A = a;
            OoW = default;
            TmuVtx0 = default;
            TmuVtx1 = default;
        }
    }
}
