﻿namespace Syroot.Glide.Glide2
{
    /// <summary>
    /// Represents mipmapping modes.
    /// </summary>
    public enum MipMap
    {
        /// <summary>No mip mapping.</summary>
        Disable,
        /// <summary>Use nearest mipmap.</summary>
        Nearest,
        /// <summary><see cref="Nearest"/> + LOD dithering.</summary>
        NearestDithered
    }
}
