﻿using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2
{
    [StructLayout(LayoutKind.Sequential)]
    public struct AT3DConfig
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public int Rev;
    }
}
