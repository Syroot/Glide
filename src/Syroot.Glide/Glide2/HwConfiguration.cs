﻿using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2
{
    [StructLayout(LayoutKind.Sequential)]
    public struct HwConfiguration
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public int NumSst;
        private SstConfig _sst0;
        private SstConfig _sst1;
        private SstConfig _sst2;
        private SstConfig _sst3;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public SstConfig[] Ssts => new[/*MAX_NUM_SST*/] { _sst0, _sst1, _sst2, _sst3 };
    }
}
