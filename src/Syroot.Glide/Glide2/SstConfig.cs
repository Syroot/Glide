﻿using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2
{
    [StructLayout(LayoutKind.Explicit)]
    public struct SstConfig
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        /// <summary>Which hardware is it?</summary>
        [FieldOffset(0)]
        public SstType Type;
        [FieldOffset(4)]
        public VoodooConfig VoodooConfig;
        [FieldOffset(4)]
        public Sst96Config Sst96Config;
        [FieldOffset(4)]
        public AT3DConfig AT3DConfig;
    }
}
