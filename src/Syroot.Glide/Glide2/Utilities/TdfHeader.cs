﻿using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2.Utilities
{
    [StructLayout(LayoutKind.Sequential)]
    public struct TdfHeader
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public uint Width;
        public uint Height;
        public Lod SmallLod;
        public Lod LargeLod;
        public Aspect AspectRatio;
        public TextureFormat Format;
    }
}
