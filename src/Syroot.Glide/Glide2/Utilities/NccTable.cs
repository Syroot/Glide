﻿using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2.Utilities
{
    [StructLayout(LayoutKind.Sequential)]
    public struct NccTable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte[] YRGB;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 * 3)]
        public short[,] IRGB;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 * 3)]
        public short[,] QRGB;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
        public uint[] PackedData;
    }
}
