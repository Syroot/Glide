﻿using System;
using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2.Utilities
{
    [StructLayout(LayoutKind.Sequential)]
    public struct TdfInfo
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public TdfHeader Header;
        public TexTable Table;
        public IntPtr Data;
        /// <summary>Memory required for mip map in bytes.</summary>
        public uint MemRequired;
    }
}
