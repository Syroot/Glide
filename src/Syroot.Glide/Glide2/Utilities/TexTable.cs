﻿using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2.Utilities
{
    [StructLayout(LayoutKind.Explicit)]
    public struct TexTable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        [FieldOffset(0)]
        public NccTable NccTable;
        [FieldOffset(0)]
        public TexPalette Palette;
    }
}
