﻿using System;
using System.Runtime.InteropServices;
using MipMapID = System.UInt32;

namespace Syroot.Glide.Glide2.Utilities
{
    /// <summary>
    /// Represents raw P/Invoke access to the utility API of the glide2x library.
    /// </summary>
    public static class Gu
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Configures the alpha combine unit.
        /// </summary>
        /// <param name="mode">The new alpha combine unit mode.</param>
        /// <remarks>
        /// <para>This method is a higher level interface to the Voodoo Graphics alpha combine unit than
        /// <see cref="Gr.AlphaCombine"/>. The alpha combine unit has two configurable inputs and one output. The output
        /// of the alpha combine unit gets fed into the alpha testing and blending units. The selection of the A_local
        /// input is improtant because it is used in the color combine unit.</para>
        /// <para><see cref="Glide2.AlphaSource"/> describes how A_local and output alpha are computed based on the
        /// mode.</para>
        /// <para>Constant color alpha is the value passed to <see cref="Gr.ConstantColorValue"/>.</para>
        /// <para>If texture has no alpha component, texture alpha is 255.</para>
        /// <para><see cref="AlphaSource"/> is a compatibility layer for <see cref="Gr.AlphaCombine"/>.</para>
        /// </remarks>
        /// <seealso cref="Gr.ConstantColorValue"/>
        /// <seealso cref="Gr.AlphaCombine"/>
        /// <seealso cref="Gr.ColorCombine"/>
        [DllImport("glide2x", EntryPoint = "_guAlphaSource@4")]
        public static extern void AlphaSource(AlphaSource mode);

        /// <summary>
        /// Generates an exponential fog table.
        /// </summary>
        /// <param name="fogTable">The array to receive the generated fog table values.</param>
        /// <param name="density">The fog density, typically between <c>0</c> and <c>1</c>.</param>
        /// <remarks>
        /// <para>This method generates an exponential fog table according to the equation <i>e ^ (-density * w)</i>
        /// where <i>w</i> is the eye-space W coordinate associated with the fog table entry. The resulting fog table
        /// is copied into <paramref name="fogTable"/>.</para>
        /// <para>The fog table is normalized (scaled) such that the last entry is maximum fog (255).</para>
        /// </remarks>
        /// <seealso cref="Gr.FogMode"/>
        /// <seealso cref="Gr.FogTable"/>
        /// <seealso cref="FogGenerateexp2"/>
        /// <seealso cref="FogGenerateLinear"/>
        /// <seealso cref="FogTableIndexToW"/>
        [DllImport("glide2x", EntryPoint = "_guFogGenerateExp@8")]
        public static extern unsafe void FogGenerateExp(byte* fogTable, float density);
        /// <summary>
        /// Generates an exponential fog table.
        /// </summary>
        /// <param name="fogTable">The array to receive the generated fog table values.</param>
        /// <param name="density">The fog density, typically between <c>0</c> and <c>1</c>.</param>
        /// <remarks>
        /// <para>This method generates an exponential fog table according to the equation <i>e ^ (-density * w)</i>
        /// where <i>w</i> is the eye-space W coordinate associated with the fog table entry. The resulting fog table
        /// is copied into <paramref name="fogTable"/>.</para>
        /// <para>The fog table is normalized (scaled) such that the last entry is maximum fog (255).</para>
        /// </remarks>
        /// <seealso cref="Gr.FogMode"/>
        /// <seealso cref="Gr.FogTable"/>
        /// <seealso cref="FogGenerateexp2"/>
        /// <seealso cref="FogGenerateLinear"/>
        /// <seealso cref="FogTableIndexToW"/>
        [DllImport("glide2x", EntryPoint = "_guFogGenerateExp@8")]
        public static extern void FogGenerateExp(IntPtr fogTable, float density);
        /// <summary>
        /// Generates an exponential fog table.
        /// </summary>
        /// <param name="fogTable">The array to receive the generated fog table values.</param>
        /// <param name="density">The fog density, typically between <c>0</c> and <c>1</c>.</param>
        /// <remarks>
        /// <para>This method generates an exponential fog table according to the equation <i>e ^ (-density * w)</i>
        /// where <i>w</i> is the eye-space W coordinate associated with the fog table entry. The resulting fog table
        /// is copied into <paramref name="fogTable"/>.</para>
        /// <para>The fog table is normalized (scaled) such that the last entry is maximum fog (255).</para>
        /// </remarks>
        /// <seealso cref="Gr.FogMode"/>
        /// <seealso cref="Gr.FogTable"/>
        /// <seealso cref="FogGenerateexp2"/>
        /// <seealso cref="FogGenerateLinear"/>
        /// <seealso cref="FogTableIndexToW"/>
        [DllImport("glide2x", EntryPoint = "_guFogGenerateExp@8")]
        public static extern void FogGenerateExp(
            [MarshalAs(UnmanagedType.LPArray, SizeConst = Gr.FogTableSize)] byte[] fogTable, float density);

        /// <summary>
        /// Gets information about the mipmap stored in a <c>.3DF</c> file.
        /// </summary>
        /// <param name="filename">Name of the <c>.3DF</c> file.</param>
        /// <param name="info">Pointer to a <see cref="TdfInfo"/> structure to fill with information about the mipmap.</param>
        /// <remarks>
        /// <para>This method allows an application to determine relevant information about a <c>.3DF</c> file located
        /// on disk. The information is assigned to the appropriate member elements of the <paramref name="info"/>
        /// structure.</para>
        /// <para>After an application has determined the characteristics of a <c>.3DF</c> mipmap, it is reponsible for
        /// allocating system memory for the mipmap. This pointer is stored in the <see cref="TdfInfo.Data"/> pointer
        /// and used by <see cref="TdfLoad"/>.</para>
        /// </remarks>
        /// <seealso cref="TdfLoad"/>
        [DllImport("glide2x", EntryPoint = "_gu3dfGetInfo@8")]
        public static extern bool TdfGetInfo([MarshalAs(UnmanagedType.LPStr)] string filename, out TdfInfo info);

        /// <summary>
        /// Loads a <c>.3DF</c> file into system memory.
        /// </summary>
        /// <param name="filename">Name of the file to load.</param>
        /// <param name="info">Pointer to a <see cref="TdfInfo"/> structure that <see cref="TdfLoad"/> fills in after
        /// loading the file.</param>
        /// <remarks>
        /// <para>This method loads a <c>.3DF</c> file specified by <paramref name="filename"/> into the pointer
        /// specified by <see cref="TdfInfo.Data"/>.</para>
        /// <para>It is assumed the <paramref name="info"/> structure passed has been appropriately configured with a
        /// call to <see cref="TdfGetInfo"/>.</para>
        /// </remarks>
        /// <returns>This method returns <see langword="true"/> if the file was successfully loaded; otherwise it
        /// returns <see langword="false"/>.</returns>
        /// <seealso cref="TdfGetInfo"/>
        [DllImport("glide2x", EntryPoint = "_gu3dfLoad@8")]
        public static extern bool TdfLoad([MarshalAs(UnmanagedType.LPStr)] string filename, ref TdfInfo info);

        /// <summary>
        /// Allocates texture memory for a mipmap.
        /// </summary>
        /// <param name="tmu">Texture Mapping Unit to modify. Valid values are <see cref="ChipID.Tmu0"/>,
        /// <see cref="ChipID.Tmu1"/>, and <see cref="ChipID.Tmu2"/>.</param>
        /// <param name="evenOddMask">Selects whether odd levels, even levels, or all levels of the mipmap are
        /// downloaded.</param>
        /// <param name="width">Width of the largest mipmap level.</param>
        /// <param name="height">Height of the largest mipmap level.</param>
        /// <param name="format">Format of the texture. Valid values are <see cref="TextureFormat.FormatRgb332"/>,
        /// <see cref="TextureFormat.FormatYiq422"/>, <see cref="TextureFormat.FormatAlpha8"/>,
        /// <see cref="TextureFormat.FormatIntensity8"/>, <see cref="TextureFormat.FormatAlphaIntensity44"/>,
        /// <see cref="TextureFormat.FormatRgb565"/>, <see cref="TextureFormat.FormatArgb8332"/>,
        /// <see cref="TextureFormat.FormatArgb1555"/>, <see cref="TextureFormat.FormatArgb4444"/>,
        /// <see cref="TextureFormat.FormatAyiq8422"/>, and <see cref="TextureFormat.FormatAlphaIntensity88"/>.</param>
        /// <param name="mmMode">Type of mipmapping to be performed when this texture is current. This value can be
        /// overridden by a call to <see cref="Gr.TexMipMapMode"/>.</param>
        /// <param name="smallLod">LOD value of the smallest LOD levels in the texture. The value in the LOD constant
        /// determines the size of the largest side of the texture.</param>
        /// <param name="largeLog">LOD value of the largeset LOD levels in the texture. The value in the LOD constant
        /// determines the size of the largest side of the texture. A combination of this and the
        /// <paramref name="aspectRatio"/> parameter determines the size of each of the mipmap levels. For example,
        /// a mipmap with <paramref name="largeLog"/> of <see cref="Lod.Lod64"/> and <paramref name="aspectRatio"/>
        /// of <see cref="Aspect.Ratio8x1"/> would have a 64x8 texture as its largest LOD. If
        /// <paramref name="aspectRatio"/> were <see cref="Aspect.Ratio1x4"/>, an 16x64 texture would be its largest
        /// LOD.</param>
        /// <param name="aspectRatio">Specifies the aspect ratio of the mipmaps as a factor of width to height. For
        /// example, a mipmap 256 texels wide by 128 texels tall has an aspect ratio of <see cref="Aspect.Ratio2x1"/>.
        /// A combination of this parameter and the LOD parameter <paramref name="smallLod"/> determines the size of
        /// each mipmap level.</param>
        /// <param name="sClampMode">Type of texture clamping to be performed when this texture is current. Clamping can
        /// be controlled in both the S and T directions. Note that this parameter should always be set to
        /// <see cref="TextureClamp.Clamp"/> for projected textures.</param>
        /// <param name="tClampMode">Type of texture clamping to be performed when this texture is current. Clamping can
        /// be controlled in both the S and T directions. Note that this parameter should always be set to
        /// <see cref="TextureClamp.Clamp"/> for projected textures.</param>
        /// <param name="minFilterMode">Specifies the type of minifaction filtering to perform.</param>
        /// <param name="magFilterMode">Specifies the type of magnification filtering to perform.</param>
        /// <param name="lodBias">Specifies the LOD bias value in the range [-8..7.75] to be used during mipmapping.
        /// Smaller values make increasingly sharper images, larger values make increasingly blurrier images. This
        /// parameter is rounded to the nearest quarter increment. By default the LOD bias is 0.</param>
        /// <param name="lodBlend">Specifies whether trilinear filtering is to be performed when this texture is
        /// current. Trilinear filtering blends between LOD levels based on LOD fraction, eliminating mipmap banding
        /// artifacts.</param>
        /// <remarks>
        /// <para>This method allocates memory on the specified TMUs and returns a handle to the allocated memory. The
        /// amount of memory allocated will be enough to hold a mipmap of the given format, LOD ranges, and aspect
        /// ratio. If the texture memory can not be allocated, a value of <see cref="Gr.NullMipMapHandle"/> is returned.
        /// After the memory has been allocated, individual mipmap levels can be downloaded one at a time using
        /// <see cref="TexDownloadMipMapLevel"/>, or all mipmap can be downloaded using <see cref="TexDownloadMipMap"/>.</para>
        /// <para>Whenever the texture is specified as a source texture, <paramref name="sClampMode"/>,
        /// <paramref name="tClampMode"/>, <paramref name="minFilterMode"/>, <paramref name="magFilterMode"/>, and
        /// <paramref name="lodBias"/> will automatically take effect.</para>
        /// <para><paramref name="evenOddMask"/> is used to selectively download LOD levels when LOD blending is to be
        /// used. Correct usage is to allocate and download the even levels onto one TMU, and the odd levels onto
        /// another, both with the <paramref name="lodBlend"/> parameter set to <see langword="true"/>. Then the texture
        /// combine mode for the lower numbered TMU is set to <see cref="TextureCombine.TrilinearOdd"/> or
        /// <see cref="TextureCombine.TrilinearEven"/> depending on whether the odd levels or the even levels were
        /// downloaded to it.</para>
        /// </remarks>
        /// <returns>
        /// A handle to the allocated memory.
        /// </returns>
        /// <seealso cref="TexGetCurrentMipMap"/>
        /// <seealso cref="TexGetMipMapInfo"/>
        /// <seealso cref="TexSource"/>
        [DllImport("glide2x", EntryPoint = "_guTexAllocateMemory@60")]
        public static extern MipMapID TexAllocateMemory(ChipID tmu, MipMapLevelMask evenOddMask, int width, int height,
            TextureFormat format, MipMap mmMode, Lod smallLod, Lod largeLog, Aspect aspectRatio,
            TextureClamp sClampMode, TextureClamp tClampMode, TextureFilter minFilterMode, TextureFilter magFilterMode,
            float lodBias, bool lodBlend);

        /// <summary>
        /// Configures the texture combine unit on a Texture Mapping Unit.
        /// </summary>
        /// <param name="tmu">Texture Mapping Unit to modify. Valid values are <see cref="ChipID.Tmu0"/>,
        /// <see cref="ChipID.Tmu1"/>, and <see cref="ChipID.Tmu2"/>.</param>
        /// <param name="func">The new texture combine mode.</param>
        /// <remarks>
        /// <para>This method specifies the function used when combining textures on a TMU with incoming textures from
        /// the neighboring TMU. Texture combining operations allow for interesting effects such as detail and projected
        /// texturing as well as the trilinear filtering of LOD blending.</para>
        /// <para><see cref="TextureCombine"/> describes the available texture combine functions and their effects.</para>
        /// <para>This method also keeps track of which TMUs require texture coordinates for the rendering routines.</para>
        /// <para>Many combine functions that simultaneously use both C_local and C_other can be computed with two
        /// passes on a single TMU system by using the frame buffer to store intermediate results and the alpha blender
        /// to combine the two partial results.</para>
        /// </remarks>
        /// <seealso cref="Gr.AlphaCombine"/>
        /// <seealso cref="Gr.ColorCombine"/>
        /// <seealso cref="Gr.DrawTriangle"/>
        /// <seealso cref="Gr.TexCombine"/>
        /// <seealso cref="Gu.TexSource"/>
        [DllImport("glide2x", EntryPoint = "_guTexCombineFunction@8")]
        public static extern void TexCombineFunction(ChipID tmu, TextureCombine func);

        /// <summary>
        /// Downloads a mipmap to texture memory.
        /// </summary>
        /// <param name="mmID">Handle of the mipmap memory accepting the download.</param>
        /// <param name="src">Pointer to row-major array of pixels of the format, size, and aspect ratio associated with
        /// <paramref name="mmID"/>. Mipmap levels are arranged from largest to smallest size.</param>
        /// <param name="table">Pointer to a Narrow Channel Compression table. This is only valid for 8-bit compressed
        /// textures loaded with <see cref="TdfLoad"/>.</param>
        /// <remarks>
        /// <para>This method downloads an entire mipmap to an area of texture memory previously allocated with
        /// <see cref="TexAllocateMemory"/>. The data to be downloaded must have the pixel format and aspect ratio
        /// associated with <paramref name="mmID"/>.</para>
        /// <para>Warning: Do not use in conjunction with <see cref="Gr.TexMinAddress"/>,
        /// <see cref="Gr.TexMaxAddress"/>, <see cref="Gr.TexNccTable"/>, <see cref="Gr.TexSource"/>,
        /// <see cref="Gr.TexDownloadTable"/>, <see cref="Gr.TexDownloadMipMapLevel"/>,
        /// <see cref="Gr.TexDownloadMipMap"/>, <see cref="Gr.TexMultiBase"/>, and
        /// <see cref="Gr.TexMultiBaseAddress"/>.</para>
        /// </remarks>
        /// <seealso cref="TexAllocateMemory"/>
        /// <seealso cref="TexDownloadMipMapLevel"/>
        /// <seealso cref="TexMemReset"/>
        /// <seealso cref="TexSource"/>
        [DllImport("glide2x", EntryPoint = "_guTexDownloadMipMap@12")]
        public static extern void TexDownloadMipMap(MipMapID mmID, IntPtr src, ref NccTable table);
    }
}
