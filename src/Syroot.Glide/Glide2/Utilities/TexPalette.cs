﻿using System.Runtime.InteropServices;

namespace Syroot.Glide.Glide2.Utilities
{
    [StructLayout(LayoutKind.Sequential)]
    public struct TexPalette
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
        public uint[] Data;
    }
}
