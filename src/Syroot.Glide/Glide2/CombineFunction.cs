﻿namespace Syroot.Glide.Glide2
{
    /// <summary>
    /// Represents the functions used in source value generation.
    /// </summary>
    public enum CombineFunction
    {
        /// <summary>0</summary>
        Zero,
        /// <summary>X_local</summary>
        Local,
        /// <summary>A_local</summary>
        LocalAlpha,
        /// <summary>f * X_other</summary>
        ScaleOther,
        /// <summary>f * X_other + X_local</summary>
        ScaleOtherAddLocal,
        /// <summary>f * X_other + A_local</summary>
        ScaleOtherAddLocalAlpha,
        /// <summary>f * (X_other - X_local)</summary>
        ScaleOtherMinusLocal,
        /// <summary>
        /// <para>f * (X_other - X_local) + X_local</para>
        /// <para>or</para>
        /// <para>f * X_other + (1 - f) * X_local</para>
        /// </summary>
        ScaleOtherMinusLocalAddLocal,
        /// <summary>f * (X_other - X_local) + A_local</summary>
        ScaleOtherMinusLocalAddLocalAlpha,
        /// <summary>
        /// <para>f * (-X_local) + X_local</para>
        /// <para>or</para>
        /// <para>(1 - f) * X_local</para>
        /// </summary>
        ScaleMinusLocalAddLocal,
        /// <summary>f * (-X_local) + A_local</summary>
        ScaleMinusLocalAddLocalAlpha = 0x10,
        /// <summary>0</summary>
        None = Zero,
        /// <summary>f * X_other</summary>
        BlendOther = ScaleOther,
        /// <summary>
        /// <para>f * (X_other - X_local) + X_local</para>
        /// <para>or</para>
        /// <para>f * X_other + (1 - f) * X_local</para>
        /// </summary>
        Blend = ScaleOtherMinusLocalAddLocal,
        /// <summary>
        /// <para>f * (-X_local) + X_local</para>
        /// <para>or</para>
        /// <para>(1 - f) * X_local</para>
        /// </summary>
        BlendLocal = ScaleMinusLocalAddLocal
    }
}
