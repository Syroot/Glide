﻿namespace Syroot.Glide
{
    /// <summary>
    /// Represents packed color RGBA ordering for linear frame buffer writes and parameters.
    /// </summary>
    public enum ColorFormat
    {
        /// <summary>Hex variable organization 0xAARRGGBB</summary>
        Argb,
        /// <summary>Hex variable organization 0xAABBGGRR</summary>
        Abgr,
        /// <summary>Hex variable organization 0xRRGGBBAA</summary>
        Rgba,
        /// <summary>Hex variable organization 0xBBGGRRAA</summary>
        Bgra
    }
}
