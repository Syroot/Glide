﻿namespace Syroot.Glide
{
    public enum Resolution
    {
        None = -1,
        Res320x200,
        Res320x240,
        Res400x256,
        Res512x384,
        Res640x200,
        Res640x350,
        Res640x400,
        Res640x480,
        Res800x600,
        Res960x720,
        Res856x480,
        Res512x256,
        Res1024x768,
        Res1280x1024,
        Res1600x1200,
        Res400x300,
        Min = Res320x200,
        Max = Res1600x1200
    }
}
