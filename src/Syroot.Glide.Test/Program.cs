﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;

namespace Syroot.Glide.Test
{
    /// <summary>
    /// Represents the main class containing the program entry point.
    /// </summary>
    internal class Program
    {
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void Main(string[] args)
        {
            // Find all available ITest implementing classes decorated with the test attribute.
            Console.WriteLine("Loading tests...");
            List<TestInfo> tests = new List<TestInfo>();
            foreach (Type type in Assembly.GetExecutingAssembly().GetExportedTypes())
            {
                if (!type.IsAbstract && typeof(ITest).IsAssignableFrom(type))
                {
                    object[] attribs = type.GetCustomAttributes(typeof(TestAttribute), false);
                    foreach (object attrib in attribs)
                    {
                        if (attrib is TestAttribute testAttrib)
                        {
                            tests.Add(new TestInfo(type, testAttrib.Purpose));
                            break;
                        }
                    }
                }
            }

            // Let the user select a test with a dialog window.
            Console.WriteLine("Select a test from the dialog window to continue...");
            TestInfo? selectedTest = null;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            using (TestDialog dialog = new TestDialog())
            {
                dialog.SetAvailableTests(tests);
                dialog.SetCommandLine(String.Join(" ", args));
                if (dialog.ShowDialog() == DialogResult.OK)
                    selectedTest = dialog.SelectedTest;
            }

            // Run any selected test.
            if (selectedTest != null)
            {
                Console.Clear();
                Console.Title = selectedTest.Type.Name;
                Console.WriteLine($"Running test {selectedTest.Type.Name}:");
                Console.WriteLine(selectedTest.Purpose);
                Console.WriteLine();

                ITest test = (ITest)Activator.CreateInstance(selectedTest.Type);
                try
                {
                    if (test.Execute(args))
                    {
                        Console.WriteLine();
                        Console.WriteLine("Test ended successfully.");
                    }
                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine("Test failed (returned false).");
                        Console.ReadKey();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine("Test failed (unhandled exception):");
                    Console.WriteLine(ex);
                    Console.ReadKey();
                }
            }
        }
    }
}
