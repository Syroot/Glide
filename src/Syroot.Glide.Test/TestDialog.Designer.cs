﻿namespace Syroot.Glide.Test
{
    partial class TestDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestDialog));
            this._lvTests = new System.Windows.Forms.ListView();
            this._colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._colPurpose = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._lbDescription = new System.Windows.Forms.Label();
            this._flpButtons = new System.Windows.Forms.FlowLayoutPanel();
            this._btCancel = new System.Windows.Forms.Button();
            this._btOK = new System.Windows.Forms.Button();
            this._lbCommandLine = new System.Windows.Forms.Label();
            this._flpButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // _lvTests
            // 
            this._lvTests.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._colName,
            this._colPurpose});
            this._lvTests.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lvTests.FullRowSelect = true;
            this._lvTests.HideSelection = false;
            this._lvTests.Location = new System.Drawing.Point(8, 50);
            this._lvTests.Name = "_lvTests";
            this._lvTests.ShowItemToolTips = true;
            this._lvTests.Size = new System.Drawing.Size(418, 272);
            this._lvTests.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this._lvTests.TabIndex = 2;
            this._lvTests.UseCompatibleStateImageBehavior = false;
            this._lvTests.View = System.Windows.Forms.View.Details;
            this._lvTests.SelectedIndexChanged += new System.EventHandler(this.LvTests_SelectedIndexChanged);
            this._lvTests.ClientSizeChanged += new System.EventHandler(this.LvTests_ClientSizeChanged);
            this._lvTests.DoubleClick += new System.EventHandler(this.LvTests_DoubleClick);
            // 
            // _colName
            // 
            this._colName.Text = "Name";
            this._colName.Width = 100;
            // 
            // _colPurpose
            // 
            this._colPurpose.Text = "Purpose";
            this._colPurpose.Width = 300;
            // 
            // _lbDescription
            // 
            this._lbDescription.AutoEllipsis = true;
            this._lbDescription.Dock = System.Windows.Forms.DockStyle.Top;
            this._lbDescription.Location = new System.Drawing.Point(8, 8);
            this._lbDescription.Name = "_lbDescription";
            this._lbDescription.Padding = new System.Windows.Forms.Padding(0, 0, 0, 8);
            this._lbDescription.Size = new System.Drawing.Size(418, 21);
            this._lbDescription.TabIndex = 0;
            this._lbDescription.Text = "Select the test you want to run from the list below, and then click OK.";
            // 
            // _flpButtons
            // 
            this._flpButtons.AutoSize = true;
            this._flpButtons.Controls.Add(this._btCancel);
            this._flpButtons.Controls.Add(this._btOK);
            this._flpButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._flpButtons.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this._flpButtons.Location = new System.Drawing.Point(8, 322);
            this._flpButtons.Margin = new System.Windows.Forms.Padding(0);
            this._flpButtons.Name = "_flpButtons";
            this._flpButtons.Padding = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this._flpButtons.Size = new System.Drawing.Size(418, 31);
            this._flpButtons.TabIndex = 3;
            this._flpButtons.WrapContents = false;
            // 
            // _btCancel
            // 
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Location = new System.Drawing.Point(343, 8);
            this._btCancel.Margin = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this._btCancel.Name = "_btCancel";
            this._btCancel.Size = new System.Drawing.Size(75, 23);
            this._btCancel.TabIndex = 1;
            this._btCancel.Text = "Cancel";
            this._btCancel.UseVisualStyleBackColor = true;
            // 
            // _btOK
            // 
            this._btOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._btOK.Location = new System.Drawing.Point(260, 8);
            this._btOK.Margin = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this._btOK.Name = "_btOK";
            this._btOK.Size = new System.Drawing.Size(75, 23);
            this._btOK.TabIndex = 0;
            this._btOK.Text = "OK";
            this._btOK.UseVisualStyleBackColor = true;
            // 
            // _lbCommandLine
            // 
            this._lbCommandLine.AutoEllipsis = true;
            this._lbCommandLine.Dock = System.Windows.Forms.DockStyle.Top;
            this._lbCommandLine.Location = new System.Drawing.Point(8, 29);
            this._lbCommandLine.Name = "_lbCommandLine";
            this._lbCommandLine.Padding = new System.Windows.Forms.Padding(0, 0, 0, 8);
            this._lbCommandLine.Size = new System.Drawing.Size(418, 21);
            this._lbCommandLine.TabIndex = 1;
            this._lbCommandLine.Text = "Command Line:";
            this._lbCommandLine.Visible = false;
            // 
            // TestDialog
            // 
            this.AcceptButton = this._btOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btCancel;
            this.ClientSize = new System.Drawing.Size(434, 361);
            this.Controls.Add(this._lvTests);
            this.Controls.Add(this._lbCommandLine);
            this.Controls.Add(this._flpButtons);
            this.Controls.Add(this._lbDescription);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(370, 200);
            this.Name = "TestDialog";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Syroot Glide Tests";
            this._flpButtons.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView _lvTests;
        private System.Windows.Forms.Label _lbDescription;
        private System.Windows.Forms.FlowLayoutPanel _flpButtons;
        private System.Windows.Forms.ColumnHeader _colName;
        private System.Windows.Forms.ColumnHeader _colPurpose;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btOK;
        private System.Windows.Forms.Label _lbCommandLine;
    }
}