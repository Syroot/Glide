﻿using System;

namespace Syroot.Glide.Test
{
    /// <summary>
    /// Represents a matrix with 4 rows and 4 columns in row-major notation, performing left-handed operations.
    /// </summary>
    public readonly struct Matrix
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const float _degree = 0.01745328f;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        /// <summary>The identity matrix.</summary>
        public static readonly Matrix Identity = new Matrix(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1);

        /// <summary>The value in the first row and the first column.</summary>
        public readonly float M11;
        /// <summary>The value in the first row and the second column.</summary>
        public readonly float M12;
        /// <summary>The value in the first row and the third column.</summary>
        public readonly float M13;
        /// <summary>The value in the first row and the fourth column.</summary>
        public readonly float M14;
        /// <summary>The value in the second row and the first column.</summary>
        public readonly float M21;
        /// <summary>The value in the second row and the second column.</summary>
        public readonly float M22;
        /// <summary>The value in the second row and the third column.</summary>
        public readonly float M23;
        /// <summary>The value in the second row and the fourth column.</summary>
        public readonly float M24;
        /// <summary>The value in the third row and the first column.</summary>
        public readonly float M31;
        /// <summary>The value in the third row and the second column.</summary>
        public readonly float M32;
        /// <summary>The value in the third row and the third column.</summary>
        public readonly float M33;
        /// <summary>The value in the third row and the fourth column.</summary>
        public readonly float M34;
        /// <summary>The value in the fourth row and the first column.</summary>
        public readonly float M41;
        /// <summary>The value in the fourth row and the second column.</summary>
        public readonly float M42;
        /// <summary>The value in the fourth row and the third column.</summary>
        public readonly float M43;
        /// <summary>The value in the fourth row and the fourth column.</summary>
        public readonly float M44;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix"/> struct with the given values.
        /// </summary>
        /// <param name="m11">The value in the first row and the first column.</param>
        /// <param name="m12">The value in the first row and the second column.</param>
        /// <param name="m13">The value in the first row and the third column.</param>
        /// <param name="m14">The value in the first row and the fourth column.</param>
        /// <param name="m21">The value in the second row and the first column.</param>
        /// <param name="m22">The value in the second row and the second column.</param>
        /// <param name="m23">The value in the second row and the third column.</param>
        /// <param name="m24">The value in the second row and the fourth column.</param>
        /// <param name="m31">The value in the third row and the first column.</param>
        /// <param name="m32">The value in the third row and the second column.</param>
        /// <param name="m33">The value in the third row and the third column.</param>
        /// <param name="m34">The value in the third row and the fourth column.</param>
        /// <param name="m41">The value in the fourth row and the first column.</param>
        /// <param name="m42">The value in the fourth row and the second column.</param>
        /// <param name="m43">The value in the fourth row and the third column.</param>
        /// <param name="m44">The value in the fourth row and the fourth column.</param>
        public Matrix(
            float m11, float m12, float m13, float m14,
            float m21, float m22, float m23, float m24,
            float m31, float m32, float m33, float m34,
            float m41, float m42, float m43, float m44)
        {
            M11 = m11; M12 = m12; M13 = m13; M14 = m14;
            M21 = m21; M22 = m22; M23 = m23; M24 = m24;
            M31 = m31; M32 = m32; M33 = m33; M34 = m34;
            M41 = m41; M42 = m42; M43 = m43; M44 = m44;
        }

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Multiplies two matrices.
        /// </summary>
        /// <param name="left">The first <see cref="Matrix"/> to multiply.</param>
        /// <param name="right">The second <see cref="Matrix"/> to multiply.</param>
        /// <returns>The product of the two matrices.</returns>
        public static Matrix operator *(in Matrix left, in Matrix right)
        {
            Multiply(left, right, out Matrix result);
            return result;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Determines the product of two matrices.
        /// </summary>
        /// <param name="left">The first matrix to multiply.</param>
        /// <param name="right">The second matrix to multiply.</param>
        /// <param name="result">The product of the two matrices.</param>
        public static void Multiply(in Matrix left, in Matrix right, out Matrix result) => result = new Matrix(
            left.M11 * right.M11 + left.M12 * right.M21 + left.M13 * right.M31 + left.M14 * right.M41,
            left.M11 * right.M12 + left.M12 * right.M22 + left.M13 * right.M32 + left.M14 * right.M42,
            left.M11 * right.M13 + left.M12 * right.M23 + left.M13 * right.M33 + left.M14 * right.M43,
            left.M11 * right.M14 + left.M12 * right.M24 + left.M13 * right.M34 + left.M14 * right.M44,
            left.M21 * right.M11 + left.M22 * right.M21 + left.M23 * right.M31 + left.M24 * right.M41,
            left.M21 * right.M12 + left.M22 * right.M22 + left.M23 * right.M32 + left.M24 * right.M42,
            left.M21 * right.M13 + left.M22 * right.M23 + left.M23 * right.M33 + left.M24 * right.M43,
            left.M21 * right.M14 + left.M22 * right.M24 + left.M23 * right.M34 + left.M24 * right.M44,
            left.M31 * right.M11 + left.M32 * right.M21 + left.M33 * right.M31 + left.M34 * right.M41,
            left.M31 * right.M12 + left.M32 * right.M22 + left.M33 * right.M32 + left.M34 * right.M42,
            left.M31 * right.M13 + left.M32 * right.M23 + left.M33 * right.M33 + left.M34 * right.M43,
            left.M31 * right.M14 + left.M32 * right.M24 + left.M33 * right.M34 + left.M34 * right.M44,
            left.M41 * right.M11 + left.M42 * right.M21 + left.M43 * right.M31 + left.M44 * right.M41,
            left.M41 * right.M12 + left.M42 * right.M22 + left.M43 * right.M32 + left.M44 * right.M42,
            left.M41 * right.M13 + left.M42 * right.M23 + left.M43 * right.M33 + left.M44 * right.M43,
            left.M41 * right.M14 + left.M42 * right.M24 + left.M43 * right.M34 + left.M44 * right.M44);

        /// <summary>
        /// Creates a <see cref="Matrix"/> that rotates around the x-axis.
        /// </summary>
        /// <param name="angle">Angle of rotation in degrees. Angles are measured clockwise when looking along the
        /// rotation axis toward the origin.</param>
        /// <param name="result">When the method completes, contains the created rotation <see cref="Matrix"/>.</param>
        public static void RotationX(float degrees, out Matrix result)
        {
            float cos = (float)Math.Cos(degrees * _degree);
            float sin = (float)Math.Sin(degrees * _degree);
            result = new Matrix(
                1, 0, 0, 0,
                0, cos, sin, 0,
                0, -sin, cos, 0,
                0, 0, 0, 1);
        }

        /// <summary>
        /// Creates a <see cref="Matrix"/> that rotates around the y-axis.
        /// </summary>
        /// <param name="angle">Angle of rotation in degrees. Angles are measured clockwise when looking along the
        /// rotation axis toward the origin.</param>
        /// <param name="result">When the method completes, contains the created rotation <see cref="Matrix"/>.</param>
        public static void RotationY(float degrees, out Matrix result)
        {
            float cos = (float)Math.Cos(degrees * _degree);
            float sin = (float)Math.Sin(degrees * _degree);
            result = new Matrix(
                cos, 0, -sin, 0,
                0, 1, 0, 0,
                sin, 0, cos, 0,
                0, 0, 0, 1);
        }

        /// <summary>
        /// Creates a translation matrix using the specified offsets.
        /// </summary>
        /// <param name="value">The offset for all three coordinate planes.</param>
        /// <param name="result">When the method completes, contains the created translation matrix.</param>
        public static void Translation(float x, float y, float z, out Matrix result) => result = new Matrix(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            x, y, z, 1);
    }
}
