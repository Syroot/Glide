﻿using System.Diagnostics;
using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test17.
    /// </summary>
    [Test("Texturing example, decal, RGB lit, white lit, flat lit.")]
    public class Test17 : SimpleSdkTest
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static readonly string[] _textureModeNames = new[]
        {
            "DECAL TEXTURE                   ",
            "FLAT SHADING * TEXTURE          ",
            "ITERATED RGB * TEXTURE          ",
            "INTENSITY LIGHTING * TEXTURE    ",
            "(ITRGB * TEXTURE)+WHITE SPECULAR"
        };

        private Texture _texture = new Texture();
        private TextureMode _textureMode;

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            // Set up render state - decal texture - color combine set in render loop.
            Gr.TexCombine(ChipID.Tmu0, CombineFunction.Local, CombineFactor.None, CombineFunction.None, CombineFactor.None, false, false);

            // Load texture data into system RAM.
            Trace.Assert(LoadTexture("miro.3df", out _texture.Info, out _texture.TableType, out _texture.TableData));
            // Download texture data into TMU.
            Gr.TexDownloadMipMap(ChipID.Tmu0, Gr.TexMinAddress(ChipID.Tmu0), MipMapLevelMask.Both, ref _texture.Info);
            if (_texture.TableType != NoTable)
                Gr.TexDownloadTable(ChipID.Tmu0, _texture.TableType, ref _texture.TableData);

            // Select texture as source of all texturing operations.
            Gr.TexSource(ChipID.Tmu0, Gr.TexMinAddress(ChipID.Tmu0), MipMapLevelMask.Both, ref _texture.Info);

            // Enable bilinear filtering + mipmapping.
            Gr.TexFilterMode(ChipID.Tmu0, TextureFilter.Bilinear, TextureFilter.Bilinear);
            Gr.TexMipMapMode(ChipID.Tmu0, MipMap.Nearest, false);

            ConOutput("m- change lighting mode\n");
            ConOutput("Press any other key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB;
            Vertex vtxC;
            Vertex vtxD;
            Gr.BufferClear(0, 0, (ushort)ZDepthValue.Farthest);

            ConOutput($"Current Texture Mode: {_textureModeNames[(int)_textureMode]}\r");

            switch (_textureMode)
            {
                case TextureMode.Decal:
                    Gr.ColorCombine(CombineFunction.ScaleOther, CombineFactor.One, CombineLocal.None, CombineOther.Texture, false);
                    break;
                case TextureMode.FlatLit:
                    Gr.ColorCombine(CombineFunction.ScaleOther, CombineFactor.Local, CombineLocal.Constant, CombineOther.Texture, false);
                    break;
                case TextureMode.RgbLit:
                    Gr.ColorCombine(CombineFunction.ScaleOther, CombineFactor.Local, CombineLocal.Iterated, CombineOther.Texture, false);
                    break;
                case TextureMode.WhiteLit:
                    Gr.ColorCombine(CombineFunction.ScaleOther, CombineFactor.LocalAlpha, CombineLocal.None, CombineOther.Texture, false);
                    Gr.AlphaCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Iterated, CombineOther.Texture, false);
                    break;
                case TextureMode.SpecAlpha:
                    Gr.ColorCombine(CombineFunction.ScaleOtherAddLocalAlpha, CombineFactor.Local, CombineLocal.Iterated, CombineOther.Texture, false);
                    Gr.AlphaCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Iterated, CombineOther.Texture, false);
                    break;
            }

            // A-B
            // |\|
            // C-D
            vtxA.OoW = 1;
            vtxB = vtxC = vtxD = vtxA;

            vtxA.X = vtxC.X = ScaleX(0.2f);
            vtxB.X = vtxD.X = ScaleX(0.8f);
            vtxA.Y = vtxB.Y = ScaleY(0.2f);
            vtxC.Y = vtxD.Y = ScaleY(0.8f);

            vtxA.TmuVtx0.SoW = vtxC.TmuVtx0.SoW = 0;
            vtxB.TmuVtx0.SoW = vtxD.TmuVtx0.SoW = 255;
            vtxD.TmuVtx0.ToW = vtxB.TmuVtx0.ToW = 0;
            vtxC.TmuVtx0.ToW = vtxD.TmuVtx0.ToW = 255;

            SetColor(ref vtxA, 255, 0, 0, 255);
            SetColor(ref vtxB, 0, 255, 0, 128);
            SetColor(ref vtxC, 0, 0, 255, 128);
            SetColor(ref vtxD, 0, 0, 0, 0);

            Gr.DrawTriangle(ref vtxA, ref vtxD, ref vtxC);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxD);

            Gr.ConstantColorValue(0xFF0000);

            return true;
        }

        /// <inheritdoc/>
        protected override void Input(char c)
        {
            switch (c)
            {
                case 'm':
                case 'M':
                    _textureMode++;
                    if (_textureMode > TextureMode.SpecAlpha)
                        _textureMode = TextureMode.Decal;
                    break;
                default:
                    base.Input(c);
                    break;
            }
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        private enum TextureMode
        {
            Decal,
            FlatLit,
            RgbLit,
            WhiteLit,
            SpecAlpha
        }
    }
}
