﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test11.
    /// </summary>
    [Test("Simple LFB read / write test.")]
    public class Test11 : SdkTest
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _size = 64;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly Random _random = new Random();
        private readonly ushort[] _colorBuf = new ushort[_size * _size];
        private readonly ushort[] _grabBuf = new ushort[_size * _size];

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Run(string[] args)
        {
            if (!TestPrelude(args))
                return;

            ConSet(0, 0, 1, 0.5f, 60, 15, 0xFFFFFF);
            Initialize();

            while (_frames-- != 0 && OkToRender)
            {
                if (_hwConfig.Ssts[0].Type == SstType.Sst96)
                {
                    GetDimsByConst(_resolution, out _scrWidth, out _scrHeight);
                    Gr.ClipWindow(0, 0, (uint)_scrWidth, (uint)_scrHeight);
                }
                Gr.BufferClear(0, 0, 0);

                // Prepare info structure.
                LfbInfo info = new LfbInfo { Size = Marshal.SizeOf(typeof(LfbInfo)) };

                // Lock back buffer.
                if (!Gr.LfbLock(Lock.WriteOnly, GrBuffer.BackBuffer, LfbWriteMode.Rgb565, Origin.UpperLeft, false,
                    ref info))
                {
                    ConOutput("Error, failed to take write lock\n");
                    break;
                }
                if (ScaleX(1f) < _size || ScaleY(1) < _size)
                    return;
                // Generate random start position.
                int startX = _random.Next(_size, (int)ScaleX(1) - _size - 1);
                int startY = _random.Next(_size, (int)ScaleY(1) - _size - 1);
                // Render image to back buffer.
                unsafe
                {
                    byte* pData = (byte*)info.LfbPtr;
                    for (int y = 0; y < _size; y++)
                    {
                        for (int x = 0; x < _size; x++)
                        {
                            ushort* pixel = (ushort*)(pData
                                + (y + startY) * info.StrideInBytes
                                + (x + startX) * 2);
                            *pixel = _colorBuf[y * _size + x];
                        }
                    }
                }
                // Unlock the back buffer.
                Gr.LfbUnlock(Lock.WriteOnly, GrBuffer.BackBuffer);

                // Swap to front buffer.
                Gr.BufferSwap(1);
                Gr.BufferClear(0, 0, 0);
                Thread.Sleep(1000);

                // Lock front buffer.
                if (!Gr.LfbLock(Lock.ReadOnly, GrBuffer.FrontBuffer, LfbWriteMode.Any, Origin.UpperLeft, false,
                    ref info))
                {
                    ConOutput("Error, failed to take read lock\n");
                    break;
                }
                // Grab the source image out of the front buffer.
                unsafe
                {
                    byte* pData = (byte*)info.LfbPtr.ToPointer();
                    for (int y = 0; y < _size; y++)
                    {
                        for (int x = 0; x < _size; x++)
                        {
                            ushort* pixel = (ushort*)(pData
                                + (y + startY) * info.StrideInBytes
                                + (x + startX) * 2);
                            _grabBuf[y * _size + x] = *pixel;
                        }
                    }
                }
                // Unlock the front buffer.
                Gr.LfbUnlock(Lock.ReadOnly, GrBuffer.FrontBuffer);
                ConClear();

                // Compare the source image to the readback image.
                bool match = true;
                for (int i = 0; i < _colorBuf.Length; i++)
                {
                    if (_colorBuf[i] != _grabBuf[i])
                    {
                        match = false;
                        break;
                    }
                }
                ConOutput($"{(match ? "Passed" : "Failed")} readback test\n");
                Gr.BufferSwap(1);
                ConOutput("Press any key to quit\n");
                ConRender();

                Gr.BufferSwap(1);
                Thread.Sleep(1000);

                if (KbHit())
                    _frames = 0;
            }

            Gr.GlideShutdown();
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void Initialize()
        {
            // Set up render state - disable dithring.
            Gr.DitherMode(Dither.Disable);

            // Create source bitmap to be copied to framebuffer.
            for (int y = 0; y < _size; y++)
            {
                for (int x = 0; x < _size; x++)
                {
                    byte r = (byte)(x << 2);
                    byte g = (byte)(y << 2);
                    byte b = (byte)((x + y) << 1);
                    _colorBuf[y * _size + x] = (ushort)((r & 0xF8) << 8);
                    _colorBuf[y * _size + x] |= (ushort)((g & 0xFC) << 3);
                    _colorBuf[y * _size + x] |= (ushort)((b & 0xF8) >> 3);
                }
            }

            ConOutput("Press any key to quit\n");
        }
    }
}
