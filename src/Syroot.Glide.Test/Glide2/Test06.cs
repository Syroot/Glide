﻿using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test06.
    /// </summary>
    [Test("Renders two interpenetrating triangles with W-buffering.")]
    public class Test06 : SimpleSdkTest
    {
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            // Set up render state - flat shading + W-buffering.
            Gr.ColorCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Constant, CombineOther.None, false);
            Gr.DepthBufferMode(DepthBuffer.WBuffer);
            Gr.DepthBufferFunction(Compare.Less);
            Gr.DepthMask(true);

            ConOutput("Press a key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB = new Vertex();
            Vertex vtxC = new Vertex();
            Gr.BufferClear(0, 0, (ushort)WDepthValue.Farthest);

            // OOW values are in the range (1.1 / 65535).
            // This acn be the exact same computer 1/W that you use for texture mapping. This saves on host computation
            // and vertex data transferred across the PCI bus.
            float wDist;

            SetPosition(ref vtxA, 0.25f, 0.21f);
            SetPosition(ref vtxB, 0.75f, 0.21f);
            SetPosition(ref vtxC, 0.5f, 0.79f);
            wDist = 10f;
            vtxA.OoW = vtxB.OoW = vtxC.OoW = 1 / wDist;
            Gr.ConstantColorValue(0x808080);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);

            SetPosition(ref vtxA, 0.86f, 0.21f);
            SetPosition(ref vtxB, 0.86f, 0.79f);
            SetPosition(ref vtxC, 0.14f, 0.5f);
            wDist = 12.5f;
            vtxA.OoW = vtxB.OoW = 1 / wDist;
            wDist = 7.5f;
            vtxC.OoW = 1 / wDist;
            Gr.ConstantColorValue(0x00FF00);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);

            return true;
        }
    }
}
