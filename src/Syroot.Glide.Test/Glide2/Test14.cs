﻿using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test14.
    /// </summary>
    [Test("Depth bias test - Vary depth bias over time with two interpenetrating triangles.")]
    public class Test14 : SimpleSdkTest
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const uint _green = 0x00FF00;
        private const uint _gray = 0x808080;

        private const int _maxZBias = 500;
        private const int _minZBias = -500;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private short _zBias;
        private short _zDelta;

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            // Set up render state.
            Gr.ColorCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Constant, CombineOther.None, false);
            Gr.DepthBufferMode(DepthBuffer.ZBuffer);
            Gr.DepthBufferFunction(Compare.Greater);
            Gr.DepthMask(true);

            _zBias = 0;
            _zDelta = 10;

            ConOutput("Press any key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB = new Vertex();
            Vertex vtxC = new Vertex();
            float zDist;
            if (_zBias > _maxZBias || _zBias < _minZBias)
                _zDelta = (short)-_zDelta;
            _zBias += _zDelta;

            ConOutput($"DepthBiasLevel: {_zBias:+000;-000}\r");

            Gr.BufferClear(0, 0, 0);

            SetPosition(ref vtxA, 0.25f, 0.21f);
            SetPosition(ref vtxB, 0.75f, 0.21f);
            SetPosition(ref vtxC, 0.5f, 0.79f);
            zDist = 10;
            vtxA.OoZ = vtxB.OoZ = vtxC.OoZ = 65535 / zDist;

            Gr.ConstantColorValue(_gray);

            Gr.DepthBiasLevel(_zBias);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);

            SetPosition(ref vtxA, 0.86f, 0.21f);
            SetPosition(ref vtxB, 0.86f, 0.79f);
            SetPosition(ref vtxC, 0.14f, 0.5f);
            zDist = 12.5f;
            vtxA.OoZ = vtxB.OoZ = 65535 / zDist;
            zDist = 7.5f;
            vtxC.OoZ = 65535 / zDist;

            Gr.ConstantColorValue(_green);

            Gr.DepthBiasLevel(0);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);

            return true;
        }
    }
}
