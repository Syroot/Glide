﻿using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test01.
    /// </summary>
    [Test("Draws a diagonal line of points from top-left to bottom-right.")]
    public class Test01 : SimpleSdkTest
    {
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            // Set up render state - flat shading.
            Gr.ColorCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Constant, CombineOther.None, false);
            Gr.ConstantColorValue(0xFFFFFF);

            ConOutput("Press a key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtx = new Vertex();
            Gr.BufferClear(0, 0, (ushort)WDepthValue.Farthest);

            for (int i = 0; i < 100; i++)
            {
                float pos = i / 100f;
                SetPosition(ref vtx, pos, pos);
                Gr.DrawPoint(ref vtx);
            }

            return true;
        }
    }
}
