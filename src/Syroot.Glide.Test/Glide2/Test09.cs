﻿using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test09.
    /// </summary>
    [Test("Chromakey - render a red and blue triangle but chromakey one out.")]
    public class Test09 : SimpleSdkTest
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const uint _red = 0x0000FF;
        private const uint _blue = 0xFF0000;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private uint _chromaColor;

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            // Set up render state - gouraud shading + fog.
            Gr.ColorCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Constant, CombineOther.None, false);
            Gr.ChromakeyMode(Chromakey.Enable);
            _chromaColor = _blue;
            Gr.ChromakeyValue(_chromaColor);

            ConOutput("Press <SPACE> to toggle chromakey color\n");
            ConOutput("Press any other key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            switch (_chromaColor)
            {
                case _red:
                    ConOutput("Chromakey RED \r");
                    break;
                case _blue:
                    ConOutput("Chromakey BLUE\r");
                    break;
            }

            Vertex vtxA = new Vertex();
            Vertex vtxB = new Vertex();
            Vertex vtxC = new Vertex();
            Gr.BufferClear(0, 0, 0);

            SetPosition(ref vtxA, 0.5f, 0.1f);
            SetPosition(ref vtxB, 0.8f, 0.9f);
            SetPosition(ref vtxC, 0.2f, 0.9f);

            Gr.ConstantColorValue(_red);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);

            Gr.SstOrigin(Origin.LowerLeft);
            Gr.ConstantColorValue(_blue);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);
            Gr.SstOrigin(Origin.UpperLeft);

            return true;
        }

        /// <inheritdoc/>
        protected override void Input(char c)
        {
            switch (c)
            {
                case ' ':
                    _chromaColor = _chromaColor == _red ? _blue : _red;
                    Gr.ChromakeyValue(_chromaColor);
                    break;
                default:
                    base.Input(c);
                    break;
            }
        }
    }
}
