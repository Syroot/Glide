﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using Syroot.Glide.Glide2;
using Syroot.Glide.Glide2.Utilities;
using Syroot.Windows;
using static Syroot.Windows.Gdi32;
using static Syroot.Windows.Kernel32;
using static Syroot.Windows.User32;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the base class for any test application, initializing the output window and calling the render loop.
    /// </summary>
    public abstract class SdkTest : ITest
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _fontWidth = 9 * 2;
        private const int _fontHeight = 12 * 2;

        protected const TexTableType NoTable = (TexTableType)(-1);

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static readonly byte[] _fontData = File.ReadAllBytes("fontData.bin"); // 128 bytes stride
        private static readonly GCPin _fontDataPin = new GCPin(_fontData);
        private static readonly ResToRes[] _resTable = new[]
        {
            new ResToRes("320x200", Resolution.Res320x200, 320, 200),
            new ResToRes("320x240", Resolution.Res320x240, 320, 240),
            new ResToRes("512x256", Resolution.Res512x256, 512, 256),
            new ResToRes("512x384", Resolution.Res512x384, 512, 384),
            new ResToRes("640x400", Resolution.Res640x400, 640, 400),
            new ResToRes("640x480", Resolution.Res640x480, 640, 480),
            new ResToRes("800x600", Resolution.Res800x600, 800, 600),
            new ResToRes("856x480", Resolution.Res856x480, 856, 480),
            new ResToRes("960x720", Resolution.Res960x720, 960, 720)
        };

        protected Resolution _resolution = Resolution.Res640x480;
        protected float _scrWidth = 640;
        protected float _scrHeight = 480;
        protected int _frames = -1;
        protected bool _scrGrab = false;
        protected string? _filename = null;

        private bool _fullScreen = true;
        private HWND _hWndMain;
        protected readonly Queue<int> _queue = new Queue<int>(256);
        protected HwConfiguration _hwConfig;
        private Resolution _res = Resolution.Res640x480;
        private WNDPROC? _wndProc; // prevent window procedure from being garbage collected

        private float _scrXScale;
        private float _scrYScale;

        private readonly FontPoint[] _fontTable = new FontPoint[128];
        private GrState _state;
        private TexInfo _fontInfo;
        private uint _fontAddress;
        private bool _fontInitialized;

        private byte[]? _consoleGrid;
        private int _consoleRows;
        private int _consoleColumns;
        private int _consoleX;
        private int _consoleY;
        private uint _consoleColor;
        private float _consoleOriginX;
        private float _consoleOriginY;
        private float _consoleCharWidth;
        private float _consoleCharHeight;

        private Matrix _currentMatrix;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a value indicating whether rendering is allowed at this time.
        /// </summary>
        protected bool OkToRender { get; private set; } = true;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public bool Execute(string[] args)
        {
            if (!InitApplication())
                return false;

            // Ignored due to pre-selection UI.
            //// Since Console.WriteLine goes into the bit bucket on Win32, put up a message in the window.
            //HDC hDC = GetDC(_hWndMain);
            //const string message = "Press any Key to continue!";
            //GetClientRect(_hWndMain, out RECT rect);
            //SetTextColor(hDC, RGB(0, 255, 255));
            //SetBkColor(hDC, RGB(0, 0, 0));
            //SetTextAlign(hDC, TextAlign.TA_CENTER);
            //ExtTextOut(hDC, rect.right / 2, rect.bottom / 2, ExtTextOptions.ETO_OPAQUE, ref rect, message,
            //    (uint)message.Length, IntPtr.Zero);
            //ReleaseDC(_hWndMain, hDC);
            //GdiFlush();

            Run(args);
            return true;
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Executes the main code and render loop of the test application.
        /// </summary>
        /// <param name="args">The command line arguments to parse.</param>
        protected abstract void Run(string[] args);

        // ---- Virtual Console ----

        /// <summary>
        /// Clears the console.
        /// </summary>
        protected void ConClear()
        {
            if (_consoleGrid == null)
                return;
            for (int i = 0; i < _consoleGrid.Length; i++)
                _consoleGrid[i] = (byte)' ';
            _consoleX = _consoleY = 0;
        }

        /// <summary>
        /// Outputs a <see cref="Console"/> style string to the virtual console.
        /// </summary>
        /// <param name="text">The text to write.</param>
        /// <returns>Number of chars printed.</returns>
        protected int ConOutput(string text)
        {
            void scrollIfNeeded()
            {
                if (_consoleY >= _consoleRows)
                {
                    _consoleY = _consoleRows - 1;
                    consoleScroll();
                }
            }
            void consoleScroll()
            {
                int lastLineStart = (_consoleRows - 1) * _consoleColumns;
                Buffer.BlockCopy(_consoleGrid, _consoleColumns, _consoleGrid, 0, lastLineStart);
                for (int i = lastLineStart; i < lastLineStart + _consoleColumns; i++)
                    _consoleGrid![i] = (byte)' ';
            }

            text = text.Substring(0, Math.Min(text.Length, 1024)).ToUpperInvariant();
            if (_fontInitialized)
            {
                foreach (char c in text)
                {
                    switch (c)
                    {
                        case '\n':
                            _consoleY++;
                            goto case '\r';
                        case '\r':
                            _consoleX = 0;
                            scrollIfNeeded();
                            break;
                        default:
                            if (_consoleX >= _consoleColumns)
                            {
                                _consoleX = 0;
                                _consoleY++;
                                scrollIfNeeded();
                            }
                            _consoleGrid![_consoleY * _consoleColumns + _consoleX] = (byte)c;
                            _consoleX++;
                            break;
                    }
                }
            }
            return text.Length;
        }

        /// <summary>
        /// Renders the console.
        /// </summary>
        protected void ConRender()
        {
            if (!_fontInitialized)
                return;

            Gr.GlideGetState(out _state);

            Gr.ColorCombine(CombineFunction.ScaleOther, CombineFactor.Local, CombineLocal.Constant, CombineOther.Texture, false);
            Gr.AlphaCombine(CombineFunction.ScaleOther, CombineFactor.One, CombineLocal.None, CombineOther.Texture, false);
            Gr.TexCombine(ChipID.Tmu0, CombineFunction.Local, CombineFactor.None, CombineFunction.Local, CombineFactor.None, false, false);
            Gr.AlphaBlendFunction(Blend.SrcAlpha, Blend.OneMinusSrcAlpha, Blend.One, Blend.Zero);
            Gr.AlphaTestFunction(Compare.Always);
            Gr.TexFilterMode(ChipID.Tmu0, TextureFilter.Bilinear, TextureFilter.Bilinear);
            Gr.TexMipMapMode(ChipID.Tmu0, MipMap.Disable, false);
            Gr.DepthBufferFunction(Compare.Always);
            Gr.AlphaTestReferenceValue(0);
            Gr.SstOrigin(Origin.UpperLeft);
            Gr.CullMode(Cull.Disable);
            Gr.TexDownloadMipMap(ChipID.Tmu0, _fontAddress, MipMapLevelMask.Both, ref _fontInfo);
            Gr.TexSource(ChipID.Tmu0, _fontAddress, MipMapLevelMask.Both, ref _fontInfo);
            Gr.ClipWindow((uint)ScaleX(0), (uint)ScaleY(0), (uint)ScaleX(1), (uint)ScaleY(1));

            for (int y = 0; y < _consoleRows; y++)
            {
                float charX = _consoleOriginX;
                float charY = _consoleOriginY + _consoleCharHeight * y;
                for (int x = 0; x < _consoleColumns; x++)
                {
                    DrawChar(_consoleGrid![y * _consoleColumns + x], charX, charY, _consoleCharWidth,
                        _consoleCharHeight);
                    charX += _consoleCharWidth;
                }
            }

            Gr.GlideSetState(ref _state);
        }

        /// <summary>
        /// Initializes console for printing. The console will scroll text 60 column text in the window described by
        /// <paramref name="minX"/>, <paramref name="minY"/>, <paramref name="maxX"/>, <paramref name="maxY"/>.
        /// </summary>
        /// <param name="minX">Upper left corner of console.</param>
        /// <param name="minY">Upper left corner of console.</param>
        /// <param name="maxX">Lower rigiht corner of console.</param>
        /// <param name="maxY">Lower rigiht corner of console.</param>
        /// <param name="columns">Columns to display before scroll.</param>
        /// <param name="rows">Rows of text to display.</param>
        protected void ConSet(float minX, float minY, float maxX, float maxY, int columns, int rows, uint color)
        {
            const string fontString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,;:*-+/_()<>|[]{}! ";
            const int charsPerLine = 14;

            _fontInfo.SmallLod = Lod.Lod128;
            _fontInfo.LargeLod = Lod.Lod128;
            _fontInfo.AspectRatio = Aspect.Ratio2x1;
            _fontInfo.Format = TextureFormat.FormatAlpha8;
            _fontInfo.Data = _fontDataPin;

            if (Environment.GetEnvironmentVariable("FX_GLIDE_NO_FONT") != null)
            {
                _fontInitialized = false;
                return;
            }

            for (int entry = 1; entry < 128; entry++)
            {
                int offset = fontString.IndexOf((char)entry);
                if (offset != -1)
                {
                    _fontTable[entry] = new FontPoint(
                        (byte)(offset % charsPerLine * _fontWidth),
                        (byte)(offset / charsPerLine * _fontHeight));
                }
            }
            _consoleGrid = new byte[rows * columns];
            for (int i = 0; i < _consoleGrid.Length; i++)
                _consoleGrid[i] = (byte)' ';
            _consoleRows = rows;
            _consoleColumns = columns;
            _consoleX = _consoleY = 0;

            _consoleColor = color;
            _consoleOriginX = minX;
            _consoleOriginY = minY;
            _consoleCharWidth = (maxX - minX) / columns;
            _consoleCharHeight = (maxY - minY) / rows;

            _fontAddress = Gr.TexMaxAddress(ChipID.Tmu0) - Gr.TexCalcMemRequired(_fontInfo.SmallLod, _fontInfo.LargeLod,
                _fontInfo.AspectRatio, _fontInfo.Format);
            _fontInitialized = true;
        }

        // ---- Utilities ----

        protected void GetDimsByConst(Resolution resolution, out float w, out float h)
        {
            for (int match = 0; match < _resTable.Length; match++)
            {
                if (_resTable[match].Resolution == resolution)
                {
                    w = _resTable[match].Width;
                    h = _resTable[match].Height;
                    return;
                }
            }
            GetWindowSize(out w, out h);
        }

        /// <summary>
        /// Searches an argument list for parameters in the form of <c>-x arg1 -y arg1 arg2 -z</c>.
        /// </summary>
        /// <param name="args">Command line arguments passed to main entry point method.</param>
        /// <returns>A list of pairs in which the key is the parameter name, and the value the parameter values.</returns>
        protected IEnumerable<KeyValuePair<string, IList<string>>> GetOpt(IList<string> args)
        {
            string? paramKey = null;
            List<string> paramValue = new List<string>();
            foreach (string arg in args)
            {
                if (arg[0] == '-')
                {
                    // Begin new parameter, yield old one if it exists.
                    if (paramKey != null)
                        yield return new KeyValuePair<string, IList<string>>(paramKey, paramValue);
                    paramKey = arg.Substring(1);
                    paramValue.Clear();
                }
                else
                {
                    paramValue.Add(arg);
                }
            }
            // Yield last parameter.
            if (paramKey != null)
                yield return new KeyValuePair<string, IList<string>>(paramKey, paramValue);
        }

        /// <summary>
        /// Returns the <see cref="Resolution"/> for a command line resolution argument.
        /// </summary>
        /// <param name="id">Command line resolution arg.</param>
        /// <param name="width">Storage for floating point screen width.</param>
        /// <param name="height">Storage for floating point screen height.</param>
        /// <returns><see cref="Resolution"/> argument.</returns>
        protected Resolution GetResolutionConstant(string id, out float width, out float height)
        {
            for (int match = 0; match < _resTable.Length; match++)
            {
                if (id == _resTable[match].ID)
                {
                    width = _resTable[match].Width;
                    height = _resTable[match].Height;
                    _fullScreen = true;
                    return _resTable[match].Resolution;
                }
            }

            _fullScreen = false;
            GetWindowSize(out width, out height);
            return _res = Resolution.None;
        }

        /// <summary>
        /// Returns a list of all supported resolutions.
        /// </summary>
        /// <returns><see cref="String"/> to resolution list.</returns>
        protected string GetResolutionList()
        {
            StringBuilder list = new StringBuilder(_resTable[0].ID, 256);
            for (int member = 1; member < _resTable.Length; member++)
            {
                list.Append(" | ");
                list.Append(_resTable[member].ID);
            }
            return list.ToString();
        }

        /// <summary>
        /// Returns a string value for a given resolution constant.
        /// </summary>
        /// <param name="res">Resolution constant.</param>
        /// <returns><see cref="String"/> representing resolution.</returns>
        protected string GetResolutionString(Resolution res)
        {
            for (int match = 0; match < _resTable.Length; match++)
                if (_resTable[match].Resolution == res)
                    return _resTable[match].ID;
            return "unknown";
        }

        /// <summary>
        /// Returns <see langword="true"/> if there are pending characters in the input queue.
        /// </summary>
        /// <returns><see langword="true"/> if keys in queue.</returns>
        protected bool KbHit()
        {
            if (_queue.Count != 0)
                return true;

            MSG msg = new MSG();
            while (PeekMessage(ref msg, IntPtr.Zero, 0, 0, PeekMessageRemove.PM_REMOVE))
            {
                TranslateMessage(ref msg);
                DispatchMessage(ref msg); // this might change the queue
                if (_queue.Count != 0)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// This example loads textures from a 3DF file. 3DF files contain pre-computed mipmap data along with any
        /// necessary supplementary information, for example palettes, ncc-tables, level-of-detail description, aspect
        /// ratio, or format.
        /// </summary>
        /// <remarks>
        /// The <see cref="Gu.TdfGetInfo"/> and <see cref="Gu.TdfLoad"/> APIs load a 3DF file into a
        /// <see cref="TdfInfo"/> structure from a file on disk. Data cna then be extracted from the
        /// <see cref="TdfInfo"/> structure to initialize a <see cref="TexInfo"/> structure used in the Glide texturing
        /// routines. Also not that texture table (either NCC or palette) management is left up to the application
        /// programmer.
        /// </remarks>
        /// <param name="filename">Name of 3DF file on disk.</param>
        /// <param name="info">Pointer to <see cref="TexInfo"/>.</param>
        /// <param name="tableType">Pointer to table type.</param>
        /// <param name="tableData">Pointer to table data.</param>
        /// <returns><see langword="true"/> if passed.</returns>
        protected bool LoadTexture(string filename, out TexInfo info, out TexTableType tableType, out TexTable tableData)
        {
            info = default;
            tableType = default;
            tableData = default;

            if (!Gu.TdfGetInfo(filename, out TdfInfo tdfInfo))
                return false;

            tdfInfo.Data = Marshal.AllocHGlobal((int)tdfInfo.MemRequired);
            if (!Gu.TdfLoad(filename, ref tdfInfo))
                return false;

            info.SmallLod = tdfInfo.Header.SmallLod;
            info.LargeLod = tdfInfo.Header.LargeLod;
            info.AspectRatio = tdfInfo.Header.AspectRatio;
            info.Format = tdfInfo.Header.Format;
            info.Data = tdfInfo.Data;
            tableType = GetTexTableType(info.Format);
            switch (tableType)
            {
                case TexTableType.Ncc0:
                case TexTableType.Ncc1:
                case TexTableType.Palette:
                    tableData = tdfInfo.Table;
                    break;
            }
            return true;
        }

        /// <summary>
        /// Scale X coordinates from normalized device coordinates [0, 1) to screen coordinates
        /// [0, WidthOfScreenInPixels).
        /// </summary>
        /// <param name="coord">X coordinate to scale.</param>
        /// <returns>Scaled X coordinate.</returns>
        protected float ScaleX(float coord) => coord * _scrXScale;

        /// <summary>
        /// Scale Y coordinates from normalized device coordinates [0, 1) to screen coordinates
        /// [0, HeightOfScreenInPixels).
        /// </summary>
        /// <param name="coord">Y coordinate to scale.</param>
        /// <returns>Scaled Y coordinate.</returns>
        protected float ScaleY(float coord) => coord * _scrYScale;

        /// <summary>
        /// Dump the LFB data.
        /// </summary>
        /// <param name="filename">Filename.</param>
        /// <param name="width">Width for frame buffer.</param>
        /// <param name="height">Height for frame buffer.</param>
        /// <returns>Success.</returns>
        protected bool ScreenDump(string filename, ushort width, ushort height)
        {
            // Store as common image file, ignoring the original LFB dump format.
            using Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format16bppRgb565);
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly,
                PixelFormat.Format16bppRgb565);
            Gr.LfbReadRegion(GrBuffer.FrontBuffer, 0, 0, width, height, (uint)bitmapData.Stride, bitmapData.Scan0);
            bitmap.UnlockBits(bitmapData);

            ImageFormat format;
            switch (Path.GetExtension(filename).ToUpper())
            {
                case ".BMP":
                    format = ImageFormat.Bmp;
                    break;
                case ".GIF":
                    format = ImageFormat.Gif;
                    break;
                case ".JPG":
                case ".JPEG":
                    format = ImageFormat.Jpeg;
                    break;
                case ".PNG":
                    format = ImageFormat.Png;
                    break;
                case ".TIF":
                    format = ImageFormat.Tiff;
                    break;
                default:
                    return false;
            }
            bitmap.Save(filename, format);
            return true;
        }

        /// <summary>
        /// Stores the given color in the <paramref name="vertex"/>.
        /// </summary>
        /// <param name="vertex">The <see cref="Vertex"/> to store the values in.</param>
        /// <param name="r">The amount of red color (0..255).</param>
        /// <param name="g">The amount of green color (0..255).</param>
        /// <param name="b">The amount of blue color (0..255).</param>
        protected void SetColor(ref Vertex vertex, float r, float g, float b)
        {
            vertex.R = r;
            vertex.G = g;
            vertex.B = b;
        }

        /// <summary>
        /// Stores the given color in the <paramref name="vertex"/>.
        /// </summary>
        /// <param name="vertex">The <see cref="Vertex"/> to store the values in.</param>
        /// <param name="r">The amount of red color (0..255).</param>
        /// <param name="g">The amount of green color (0..255).</param>
        /// <param name="b">The amount of blue color (0..255).</param>
        /// <param name="a">The opacity (0..255).</param>
        protected void SetColor(ref Vertex vertex, float r, float g, float b, float a)
        {
            vertex.R = r;
            vertex.G = g;
            vertex.B = b;
            vertex.A = a;
        }

        /// <summary>
        /// Sets the current matrix. This matrix translates the object into view space from local coordinates
        /// during calls to <see cref="TransformVertices"/>. All spaces are considered to be -1->1 normalized.
        /// </summary>
        protected void SetMatrix(in Matrix matrix) => _currentMatrix = matrix;

        /// <summary>
        /// Scales the given coordinates and stores the result in the <paramref name="vertex"/>.
        /// </summary>
        /// <param name="vertex">The <see cref="Vertex"/> to store the values in.</param>
        /// <param name="x">The X coordinate to scale and set.</param>
        /// <param name="y">The Y coordinate to scale and set.</param>
        protected void SetPosition(ref Vertex vertex, float x, float y)
        {
            vertex.X = x * _scrXScale;
            vertex.Y = y * _scrYScale;
        }

        /// <summary>
        /// Sets up screen scaling.
        /// </summary>
        /// <param name="scrWidth">Width of screen.</param>
        /// <param name="scrHeight">Height of screen.</param>
        protected void SetScreen(float scrWidth, float scrHeight)
        {
            _scrXScale = scrWidth;
            _scrYScale = scrHeight;
        }

        /// <summary>
        /// SDK test prelude beginning the test with parsing the command line and initializing Glide appropriately.
        /// </summary>
        /// <param name="args">The command line arguments passed to the test.</param>
        /// <returns>Whether the command line was valid and the test can be run.</returns>
        protected bool TestPrelude(string[] args)
        {
            const string usage = "-n <frames> -r <res> -d <filename>";

            // Process command line arguments.
            foreach (var param in GetOpt(args))
            {
                switch (param.Key)
                {
                    case "n":
                        _frames = Int32.Parse(param.Value[0]);
                        break;
                    case "r":
                        _resolution = GetResolutionConstant(param.Value[0], out _scrWidth, out _scrHeight);
                        break;
                    case "d":
                        _scrGrab = true;
                        _frames = 1;
                        _filename = param.Value[0];
                        break;
                    default:
                        Console.WriteLine("Unrecognized command line argument");
                        Console.WriteLine($"{GetType().Name} {usage}");
                        Console.WriteLine("Available resolutions:");
                        Console.WriteLine(GetResolutionList());
                        return false;
                }
            }
            SetScreen(_scrWidth, _scrHeight);
            Console.WriteLine($"Glide version: {Gr.GlideGetVersion()}");
            Console.WriteLine($"Resolution: {GetResolutionString(_resolution)}");
            // Ignored due to pre-selection UI.
            //if (_frames == -1)
            //{
            //    Console.WriteLine("Press A Key To Begin Test.");
            //    Console.ReadKey();
            //}

            // Initialize Glide.
            Gr.GlideInit();
            Trace.Assert(Gr.SstQueryHardware(ref _hwConfig));
            Gr.SstSelect(0);
            Trace.Assert(Gr.SstWinOpen(0, _resolution, Refresh.Ref60Hz, ColorFormat.Abgr, Origin.UpperLeft, 2, 1));
            return true;
        }

        protected void TestScreenGrab()
        {
            // Grab the frame buffer.
            if (_scrGrab)
            {
                if (!ScreenDump(_filename!, (ushort)_scrWidth, (ushort)_scrHeight))
                    Console.WriteLine($"Cannot open {_filename}");
                _scrGrab = false;
            }
        }

        /// <summary>
        /// Transforms a list of vertices from model space into view space.
        /// </summary>
        /// <param name="dstVerts">Memory to store transformed vertices.</param>
        /// <param name="srcVerts">Array of vertices to be transformed.</param>
        /// <param name="length">Number of vertices to transform.</param>
        protected void TransformVertices(Vertex3D[] dstVerts, Vertex3D[] srcVerts, int length)
        {
            ref Matrix m = ref _currentMatrix;
            for (int i = 0; i < length; i++)
            {
                ref Vertex3D v = ref srcVerts[i];
                ref Vertex3D dst = ref dstVerts[i];
                dst = v;
                dst.X = v.X * m.M11 + v.Y * m.M21 + v.Z * m.M31 + v.W * m.M41;
                dst.Y = v.X * m.M12 + v.Y * m.M22 + v.Z * m.M32 + v.W * m.M42;
                dst.Z = v.X * m.M13 + v.Y * m.M23 + v.Z * m.M33 + v.W * m.M43;
                dst.W = v.X * m.M14 + v.Y * m.M24 + v.Z * m.M34 + v.W * m.M44;
            }
        }

        // Simplified perspective projection matrix assumes unit clip volume.
        private static Matrix _projMatrix = new Matrix(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 1,
            0, 0, 0, 0);

        /// <summary>
        /// Perspectivily projects a set of vertices into normalized 2D space (0,1).
        /// </summary>
        /// <param name="dstVerts">Memory to store projected vertices.</param>
        /// <param name="srcVerts">Array of vertices to be transformed.</param>
        /// <param name="length">Number of vertices to transform.</param>
        protected void ProjectVertices(Vertex3D[] dstVerts, Vertex3D[] srcVerts, int length)
        {
            const float offset = 1;
            const float scale = 0.5f;

            ref Matrix m = ref _projMatrix;
            for (int i = 0; i < length; i++)
            {
                ref Vertex3D v = ref srcVerts[i];
                ref Vertex3D dst = ref dstVerts[i];
                dst = v;
                dst.X = v.X * m.M11 + v.Y * m.M21 + v.Z * m.M31 + v.W * m.M41;
                dst.Y = v.X * m.M12 + v.Y * m.M22 + v.Z * m.M32 + v.W * m.M42;
                dst.Z = v.X * m.M13 + v.Y * m.M23 + v.Z * m.M33 + v.W * m.M43;
                dst.W = v.X * m.M14 + v.Y * m.M24 + v.Z * m.M34 + v.W * m.M44;
                dst.X /= dst.W;
                dst.Y /= dst.W;
                dst.Z /= dst.W;
                dst.X += offset;
                dst.X *= scale;
                dst.Y += offset;
                dst.Y *= scale;
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static TexTableType GetTexTableType(TextureFormat format)
        {
            switch (format)
            {
                case TextureFormat.FormatYiq422:
                case TextureFormat.FormatAyiq8422:
                    return TexTableType.Ncc0;
                case TextureFormat.FormatP8:
                case TextureFormat.FormatAp88:
                    return TexTableType.Palette;
                default:
                    return TexTableType.Palette;
            }
        }

        private void DrawChar(byte character, float x, float y, float w, float h)
        {
            if (character == (byte)' ')
                return;

            Vertex a = new Vertex();
            Vertex b = new Vertex();
            Vertex c = new Vertex();
            Vertex d = new Vertex();
            // a---b
            // |\  |
            // | \ |
            // |  \|
            // c---d
            a.OoW = b.OoW = c.OoW = d.OoW = 1;
            a.X = c.X = ScaleX(x);
            a.Y = b.Y = ScaleY(y);
            d.X = b.X = ScaleX(x + w);
            d.Y = c.Y = ScaleY(y + h);

            Gr.ConstantColorValue(_consoleColor);

            a.TmuVtx0.SoW = c.TmuVtx0.SoW = _fontTable[character].X;
            a.TmuVtx0.ToW = b.TmuVtx0.ToW = _fontTable[character].Y;
            d.TmuVtx0.SoW = b.TmuVtx0.SoW = a.TmuVtx0.SoW + _fontWidth;
            d.TmuVtx0.ToW = c.TmuVtx0.ToW = a.TmuVtx0.ToW + _fontHeight;

            Gr.DrawTriangle(ref a, ref d, ref c);
            Gr.DrawTriangle(ref a, ref b, ref d);
        }

        private void GetWindowSize(out float width, out float height)
        {
            RECT rect;
            if (_fullScreen)
                GetWindowRect(_hWndMain, out rect);
            else
                GetClientRect(_hWndMain, out rect);
            width = rect.Width;
            height = rect.Height;
        }

        private bool InitApplication()
        {
            HINSTANCE hInstance = (IntPtr)GetModuleHandle(null);
            _wndProc = MainWndProc;
            WNDCLASS wc = new WNDCLASS
            {
                style = ClassStyle.CS_DBLCLKS,
                lpfnWndProc = Marshal.GetFunctionPointerForDelegate(_wndProc),
                hInstance = hInstance,
                hIcon = LoadIcon(IntPtr.Zero, (int)IconResource.IDI_APPLICATION),
                hCursor = LoadCursor(IntPtr.Zero, (int)CursorResource.IDC_ARROW),
                hbrBackground = (IntPtr)GetStockObject(StockObject.BLACK_BRUSH),
                lpszClassName = "WinGlideClass"
            };
            if (RegisterClass(ref wc) == 0)
                return false;

            _hWndMain = CreateWindowEx(WindowStyleExtended.WS_EX_APPWINDOW, "WinGlideClass", "GlideTest",
                WindowStyle.WS_OVERLAPPEDWINDOW | WindowStyle.WS_VISIBLE | WindowStyle.WS_POPUP,
                CW_USEDEFAULT, CW_USEDEFAULT, 0x110, 0x120, IntPtr.Zero, IntPtr.Zero, hInstance, IntPtr.Zero);
            if (_hWndMain == IntPtr.Zero)
                return false;

            SetCursor(IntPtr.Zero);
            ShowWindow(_hWndMain, ShowWindowCmd.SW_NORMAL);
            UpdateWindow(_hWndMain);
            return true;
        }

        private IntPtr MainWndProc(HWND hWnd, uint message, IntPtr wParam, IntPtr lParam)
        {
            switch ((WindowMessage)message)
            {
                case WindowMessage.WM_SETCURSOR:
                    if (_res != Resolution.None)
                    {
                        SetCursor(IntPtr.Zero);
                        return IntPtr.Zero;
                    }
                    break;

                case WindowMessage.WM_CREATE:
                    break;

                case WindowMessage.WM_PAINT:
                    BeginPaint(hWnd, out PAINTSTRUCT ps);
                    EndPaint(hWnd, ref ps);
                    return new IntPtr(1);

                case WindowMessage.WM_CLOSE:
                    _queue.Enqueue('q');
                    break;

                case WindowMessage.WM_DESTROY:
                    break;

                case WindowMessage.WM_MOVE:
                    if (!Gr.SstControl(GrControl.Move))
                    {
                        PostMessage(_hWndMain, (uint)WindowMessage.WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
                        return IntPtr.Zero;
                    }
                    break;

                case WindowMessage.WM_ACTIVATE:
                    if (_hwConfig.Ssts[0].Type == SstType.Voodoo)
                    {
                        WindowActivateState state = (WindowActivateState)wParam.ToInt32();
                        if ((state & WindowActivateState.Active) != 0)
                            Gr.SstControl(GrControl.Deactivate);
                        else
                            Gr.SstControl(GrControl.Activate);
                    }
                    else
                    {
                        return IntPtr.Zero;
                    }
                    break;

                case WindowMessage.WM_DISPLAYCHANGE:
                case WindowMessage.WM_SIZE:
                    GetWindowSize(out float width, out float height);
                    SetScreen(width, height);
                    if (Gr.SstControl(GrControl.Resize))
                    {
                        OkToRender = true;
                    }
                    else
                    {
                        MessageBox(hWnd, "Resize failed due to lack of sufficient buffer memory.", "Allocation Failure",
                            MessageBoxType.MB_OK | MessageBoxType.MB_APPLMODAL);
                        PostMessage(_hWndMain, (uint)WindowMessage.WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
                        OkToRender = false;
                        return IntPtr.Zero;
                    }
                    break;

                case WindowMessage.WM_CHAR:
                    int value = wParam.ToInt32();
                    if (value < 128)
                        _queue.Enqueue(value);
                    break;
            }
            return DefWindowProc(hWnd, message, wParam, lParam);
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        [StructLayout(LayoutKind.Sequential)]
        protected struct Texture
        {
            internal TexInfo Info;
            internal TexTableType TableType;
            internal TexTable TableData;
        }

        private readonly struct FontPoint
        {
            internal readonly byte X;
            internal readonly byte Y;

            internal FontPoint(byte x, byte y)
            {
                X = x;
                Y = y;
            }
        }

        private readonly struct ResToRes
        {
            internal readonly string ID;
            internal readonly Resolution Resolution;
            internal readonly float Width;
            internal readonly float Height;

            internal ResToRes(string id, Resolution res, float width, float height)
            {
                ID = id;
                Resolution = res;
                Width = width;
                Height = height;
            }
        }

        protected struct Vertex3D
        {
            internal float X;
            internal float Y;
            internal float Z;
            internal float W;
            internal float S;
            internal float T;
            internal float R;
            internal float G;
            internal float B;
            internal float A;

            internal Vertex3D(float x, float y, float z, float w, float s, float t)
            {
                X = x;
                Y = y;
                Z = z;
                W = w;
                S = s;
                T = t;
                R = default;
                G = default;
                B = default;
                A = default;
            }
        }
    }
}
