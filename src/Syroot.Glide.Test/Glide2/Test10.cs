﻿using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test10.
    /// </summary>
    [Test("Culling test - render a red and blue triangle but cull one out - red positive, blue negative.")]
    public class Test10 : SimpleSdkTest
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Cull _cullMode;

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            // Set up render state.
            Gr.ColorCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Constant, CombineOther.None, false);
            _cullMode = Cull.Positive;
            Gr.CullMode(_cullMode);

            ConOutput("Press <SPACE> to toggle cull orientation\n");
            ConOutput("Press any other key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            switch (_cullMode)
            {
                case Cull.Positive:
                    ConOutput("CULL POSITIVE\r");
                    break;
                case Cull.Negative:
                    ConOutput("CULL NEGATIVE\r");
                    break;
            }

            Vertex vtxA = new Vertex();
            Vertex vtxB = new Vertex();
            Vertex vtxC = new Vertex();
            Gr.BufferClear(0, 0, 0);

            SetPosition(ref vtxA, 0.5f, 0.1f);
            SetPosition(ref vtxB, 0.8f, 0.9f);
            SetPosition(ref vtxC, 0.2f, 0.9f);

            Gr.ConstantColorValue(0x0000FF);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);

            Gr.SstOrigin(Origin.LowerLeft);
            Gr.ConstantColorValue(0xFF0000);
            Gr.DrawTriangle(ref vtxA, ref vtxC, ref vtxB);
            Gr.SstOrigin(Origin.UpperLeft);

            return true;
        }

        /// <inheritdoc/>
        protected override void Input(char c)
        {
            switch (c)
            {
                case ' ':
                    _cullMode = _cullMode == Cull.Positive ? Cull.Negative : Cull.Positive;
                    Gr.CullMode(_cullMode);
                    break;
                default:
                    base.Input(c);
                    break;
            }
        }
    }
}
