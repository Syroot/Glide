﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test12.
    /// </summary>
    [Test("LFB write modes, render buffer, pixpipe, Y origin test.")]
    public class Test12 : SdkTest
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Texture _texture;

        private Origin _lfbOrigin;
        private Origin _sstOrigin;
        private LfbWriteMode _writeMode;
        private GrBuffer _lfbBuffer;
        private GrBuffer _sstBuffer;
        private GrBuffer _curBuffer;
        private LfbInfo _info;
        private bool _pixPipe;

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Run(string[] args)
        {
            if (!TestPrelude(args))
                return;
            if (_resolution == Resolution.None)
            {
                Console.Error.WriteLine("Error!: Frontbuffer rendering not supported in a window");
                return;
            }

            ConSet(0, 0, 1, 0.5f, 60, 15, 0xFFFFFF);
            Initialize();

            while (_frames-- != 0 && OkToRender)
            {
                Render();
                while (KbHit())
                    Input((char)_queue.Dequeue());
            }

            Gr.GlideShutdown();
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void Initialize()
        {
            // Load source bitmap from a .3df file.
            Trace.Assert(LoadTexture("decal1.3df", out _texture.Info, out _texture.TableType, out _texture.TableData));

            _lfbOrigin = Origin.UpperLeft;
            _sstOrigin = Origin.UpperLeft;
            _lfbBuffer = GrBuffer.BackBuffer;
            _sstBuffer = GrBuffer.BackBuffer;
            _curBuffer = GrBuffer.BackBuffer;
            _pixPipe = false;
            _writeMode = LfbWriteMode.Rgb565;

            _info.Size = Marshal.SizeOf(typeof(LfbInfo));
        }

        private void Render()
        {
            void outputStatus() => ConOutput(
                $"Current Buffer: {_curBuffer}\n"
                + $"1 - lock yOrigin        ({_lfbOrigin})\n"
                + $"2 - glide yOrigin       ({_sstOrigin})\n"
                + $"3 - lfb render buffer   ({_lfbBuffer})\n"
                + $"4 - glide render buffer ({_sstBuffer})\n"
                + $"5 - pixpipe enable      ({_pixPipe})\n"
                + $"6 - lfb write mode      ({_writeMode})\n"
                + "Press any other key to quit\n");

            if (_hwConfig.Ssts[0].Type == SstType.Sst96)
            {
                GetDimsByConst(_resolution, out _scrWidth, out _scrHeight);
                Gr.ClipWindow(0, 0, (uint)_scrWidth, (uint)_scrHeight);
            }

            Gr.RenderBuffer(GrBuffer.BackBuffer);
            Gr.BufferClear(0, 0, 0);
            Gr.RenderBuffer(GrBuffer.FrontBuffer);
            Gr.BufferClear(0, 0, 0);
            Gr.RenderBuffer(GrBuffer.BackBuffer);

            // Set Glide state.
            Gr.RenderBuffer(_sstBuffer);
            Gr.SstOrigin(_sstOrigin);

            // Attempt lock.
            if (Gr.LfbLock(Lock.WriteOnly, _lfbBuffer, _writeMode, _lfbOrigin, _pixPipe, ref _info))
            {
                unsafe
                {
                    uint* longData = (uint*)_info.LfbPtr;
                    ushort* shortData = (ushort*)_info.LfbPtr;
                    ushort* srcData = (ushort*)_texture.Info.Data;
                    uint longStride = _info.StrideInBytes / sizeof(int);
                    uint shortStride = _info.StrideInBytes / sizeof(short);
                    uint longColor;
                    ushort shortColor;
                    for (int y = 0; y < 256; y++)
                    {
                        for (int x = 0; x < 256; x++)
                        {
                            switch (_writeMode)
                            {
                                case LfbWriteMode.Rgb565:
                                    ushort temp = srcData[y * 256 + x];
                                    shortData[y * shortStride + x] = temp;
                                    break;
                                case LfbWriteMode.Rgb555:
                                case LfbWriteMode.Rgb1555:
                                    shortColor = srcData[y * 256 + x];
                                    shortColor = (ushort)(0b10000000_00000000
                                        | ((shortColor >> 1) & 0b01111100_00000000)
                                        | ((shortColor >> 1) & 0b00000011_11100000)
                                        | ((shortColor >> 0) & 0b00000000_00011111));
                                    shortData[y * shortStride + x] = shortColor;
                                    break;
                                case LfbWriteMode.Rgb888:
                                case LfbWriteMode.Rgb8888:
                                    longColor = srcData[y * 256 + x];
                                    longColor = 0b11111111_00000000_00000000_00000000
                                        | ((longColor << 8) & 0b00000000_11111000_00000000_00000000)
                                        | ((longColor << 5) & 0b00000000_00000000_11111100_00000000)
                                        | ((longColor << 3) & 0b00000000_00000000_00000000_11111000);
                                    longData[y * longStride + x] = longColor;
                                    break;
                                case LfbWriteMode.Rgb565Depth:
                                    longData[y * longStride + x] = srcData[y * 256 + x];
                                    break;
                                case LfbWriteMode.Rgb555Depth:
                                case LfbWriteMode.Rgb1555Depth:
                                    longColor = srcData[y * 256 + x];
                                    longColor = (0b10000000_00000000)
                                        | ((longColor >> 1) & 0b01111100_00000000)
                                        | ((longColor >> 1) & 0b00000011_11100000)
                                        | ((longColor >> 0) & 0b00000000_00011111);
                                    longData[y * longStride + x] = longColor;
                                    break;
                            }
                        }
                    }
                }

                Gr.LfbUnlock(Lock.WriteOnly, _lfbBuffer);

                Gr.RenderBuffer(GrBuffer.BackBuffer);

                ConClear();
                _curBuffer = GrBuffer.BackBuffer;
                outputStatus();
                ConRender();
                Gr.BufferSwap(1);
                Thread.Sleep(1000);

                ConClear();
                _curBuffer = GrBuffer.FrontBuffer;
                outputStatus();
                ConRender();
                Gr.BufferSwap(1);
                Thread.Sleep(1000);
            }
            else
            {
                Gr.RenderBuffer(GrBuffer.BackBuffer);
                ConClear();
                outputStatus();
                ConOutput("\nLock Failed....no output\n");
                ConRender();
                Gr.BufferSwap(1);
                Thread.Sleep(1000);
            }
        }

        private void Input(char c)
        {
            switch (c)
            {
                case '1':
                    _lfbOrigin = _lfbOrigin == Origin.UpperLeft ? Origin.LowerLeft : Origin.UpperLeft;
                    break;
                case '2':
                    _sstOrigin = _sstOrigin == Origin.UpperLeft ? Origin.LowerLeft : Origin.UpperLeft;
                    break;
                case '3':
                    _lfbBuffer = _lfbBuffer == GrBuffer.FrontBuffer ? GrBuffer.BackBuffer : GrBuffer.FrontBuffer;
                    break;
                case '4':
                    _sstBuffer = _lfbBuffer == GrBuffer.FrontBuffer ? GrBuffer.BackBuffer : GrBuffer.FrontBuffer;
                    break;
                case '5':
                    _pixPipe = !_pixPipe;
                    break;
                case '6':
                    _writeMode++;
                    if (_writeMode > LfbWriteMode.ZA16)
                        _writeMode = LfbWriteMode.Rgb565;
                    break;
                default:
                    _frames = 0;
                    break;
            }
        }
    }
}
