﻿using System;
using Syroot.Glide.Glide2;
using Syroot.Glide.Glide2.Utilities;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test23.
    /// </summary>
    [Test("Anti-aliased points test.")]
    public class Test23 : SimpleSdkTest
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Mode _mode;
        private bool _speed;
        private readonly Vertex3D[] _srcVerts = new Vertex3D[100];
        private float _angle;
        private readonly byte[] _fogTable = new byte[Gr.FogTableSize];
        private readonly Vertex3D[] _xfVerts = new Vertex3D[100];
        private readonly Vertex3D[] _prjVerts = new Vertex3D[100];

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            Gr.SstOrigin(Origin.LowerLeft);

            // Set up render state - flat shading - alpha blending.
            Gr.ColorCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Constant, CombineOther.None, false);
            Gr.AlphaCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Iterated, CombineOther.None, false);
            Gr.FogMode(Fog.WithTable);
            Gr.FogColorValue(0);
            Gu.FogGenerateExp(_fogTable, 0.8f);
            Gr.FogTable(_fogTable);
            Gr.AlphaBlendFunction(Blend.SrcAlpha, Blend.OneMinusSrcAlpha, Blend.Zero, Blend.Zero);

            // Initialize source 3D data - one hundred random points within a 1x1 box.
            Random random = new Random();
            for (int i = 0; i < 100; i++)
            {
                _srcVerts[i].X = (float)(random.NextDouble() - 0.5f);
                _srcVerts[i].Y = (float)(random.NextDouble() - 0.5f);
                _srcVerts[i].Z = (float)(random.NextDouble() - 0.5f);
                _srcVerts[i].W = 1;
            }

            _angle = 0;
            _mode = Mode.Antialiased;
            _speed = false;

            ConOutput("a - toggles anti-aliasing\n");
            ConOutput("s - toggles speed of rotation\n");
            ConOutput("Press any key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Gr.BufferClear(0, 0, (ushort)ZDepthValue.Farthest);

            // 3D transformations
            _angle += _speed ? 1 : 0.05f;
            if (_angle >= 360)
                _angle -= 360;

            Matrix.RotationY(_angle, out Matrix rotationMatrix);
            Matrix.Translation(0, 0, 1.5f, out Matrix translationMatrix);
            SetMatrix(Matrix.Identity * rotationMatrix * translationMatrix);

            TransformVertices(_xfVerts, _srcVerts, _srcVerts.Length);
            ProjectVertices(_prjVerts, _xfVerts, _srcVerts.Length);

            Gr.ConstantColorValue(0xFFFFFFFF);

            switch (_mode)
            {
                case Mode.Normal:
                    ConOutput("NORMAL POINTS     \r");
                    break;
                case Mode.Antialiased:
                    ConOutput("ANTIALIASED POINTS\r");
                    break;
            }

            for (int i = 0; i < 100; i++)
            {
                SetPosition(ref vtxA, _prjVerts[i].X, _prjVerts[i].Y);
                vtxA.OoW = 1 / _prjVerts[i].W;
                vtxA.A = 255;
                switch (_mode)
                {
                    case Mode.Normal:
                        Gr.DrawPoint(ref vtxA);
                        break;
                    case Mode.Antialiased:
                        Gr.AADrawPoint(ref vtxA);
                        break;
                }
            }

            return true;
        }

        /// <inheritdoc/>
        protected override void Input(char c)
        {
            switch (c)
            {
                case 'a':
                case 'A':
                    _mode = (Mode)((int)++_mode % 2);
                    break;
                case 's':
                case 'S':
                    _speed = !_speed;
                    break;
                default:
                    base.Input(c);
                    break;
            }
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        private enum Mode
        {
            Normal,
            Antialiased
        }
    }
}
