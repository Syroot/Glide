﻿using System.Diagnostics;
using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test20.
    /// </summary>
    [Test("Mipmap modes.")]
    public class Test20 : SimpleSdkTest
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const float _maxDist = 2.5f;
        private const float _minDist = 1;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Texture _texture = new Texture();
        private readonly Vertex3D[] _srcVerts = new Vertex3D[4];
        private float _distance;
        private float _dDelta;
        private MipMapMode _mipMapMode;

        private readonly Vertex3D[] _xfVerts = new Vertex3D[4];
        private readonly Vertex3D[] _prjVerts = new Vertex3D[4];

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            Gr.SstOrigin(Origin.LowerLeft);

            // Set up render state - decal texture - bilinear.
            Gr.ColorCombine(CombineFunction.ScaleOther, CombineFactor.One, CombineLocal.None, CombineOther.Texture, false);
            Gr.TexFilterMode(ChipID.Tmu0, TextureFilter.Bilinear, TextureFilter.Bilinear);

            // Load texture data into system RAM.
            Trace.Assert(LoadTexture("decal1.3df", out _texture.Info, out _texture.TableType, out _texture.TableData));
            // Download texture data to TMU.
            Gr.TexDownloadMipMap(ChipID.Tmu0, Gr.TexMinAddress(ChipID.Tmu0), MipMapLevelMask.Both, ref _texture.Info);
            if (_texture.TableType != NoTable)
                Gr.TexDownloadTable(ChipID.Tmu0, _texture.TableType, ref _texture.TableData);

            // Select texture as source of all texturing operations.
            Gr.TexSource(ChipID.Tmu0, Gr.TexMinAddress(ChipID.Tmu0), MipMapLevelMask.Both, ref _texture.Info);

            // Initialize source 3D data - rectangle on X/Z plane, centered about Y axis.
            // 0--1  Z+
            // |  |  |
            // 2--3   - X+
            _srcVerts[0] = new Vertex3D(-0.5f, 0, 0.5f, 1, 0, 0);
            _srcVerts[1] = new Vertex3D(0.5f, 0, 0.5f, 1, 1, 0);
            _srcVerts[2] = new Vertex3D(-0.5f, 0, -0.5f, 1, 0, 1);
            _srcVerts[3] = new Vertex3D(0.5f, 0, -0.5f, 1, 1, 1);

            _distance = 1;
            _dDelta = 0.01f;
            _mipMapMode = MipMapMode.Disable;

            ConOutput("m - change mipmapping mode\n");
            ConOutput("Press any other key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB;
            Vertex vtxC;
            Vertex vtxD;
            Gr.BufferClear(0x404040, 0, (ushort)ZDepthValue.Farthest);

            switch (_mipMapMode)
            {
                case MipMapMode.Disable:
                    Gr.TexMipMapMode(ChipID.Tmu0, MipMap.Disable, false);
                    Gr.TexCombine(ChipID.Tmu0,
                        CombineFunction.Local, CombineFactor.None,
                        CombineFunction.Local, CombineFactor.None,
                        false, false);
                    ConOutput("GR_MIPMAP_DISABLE  \r");
                    break;
                case MipMapMode.Nearest:
                    Gr.TexMipMapMode(ChipID.Tmu0, MipMap.Nearest, false);
                    Gr.TexCombine(ChipID.Tmu0,
                        CombineFunction.Local, CombineFactor.None,
                        CombineFunction.Local, CombineFactor.None,
                        false, false);
                    ConOutput("GR_MIPMAP_NEAREST  \r");
                    break;
                case MipMapMode.Trilinear:
                    Gr.TexMipMapMode(ChipID.Tmu0, MipMap.Nearest, true);
                    Gr.TexCombine(ChipID.Tmu0,
                        CombineFunction.BlendLocal, CombineFactor.LodFraction,
                        CombineFunction.BlendLocal, CombineFactor.LodFraction,
                        false, false);
                    ConOutput("GR_MIPMAP_TRILINEAR\r");
                    break;
            }

            // A-B
            // |\|
            // C-D
            vtxA.OoW = 1;
            vtxB = vtxC = vtxD = vtxA;

            _distance += _dDelta;
            if (_distance > _maxDist || _distance < _minDist)
            {
                _dDelta *= -1f;
                _distance += _dDelta;
            }

            Matrix.RotationX(-20, out Matrix rotationMtx);
            Matrix.Translation(0, -0.3f, _distance, out Matrix distanceMtx);
            SetMatrix(Matrix.Identity * rotationMtx * distanceMtx);

            TransformVertices(_xfVerts, _srcVerts, _srcVerts.Length);
            ProjectVertices(_prjVerts, _xfVerts, _srcVerts.Length);

            SetPosition(ref vtxA, _prjVerts[0].X, _prjVerts[0].Y);
            vtxA.OoW = 1 / _prjVerts[0].W;

            SetPosition(ref vtxB, _prjVerts[1].X, _prjVerts[1].Y);
            vtxB.OoW = 1 / _prjVerts[1].W;

            SetPosition(ref vtxC, _prjVerts[2].X, _prjVerts[2].Y);
            vtxC.OoW = 1 / _prjVerts[2].W;

            SetPosition(ref vtxD, _prjVerts[3].X, _prjVerts[3].Y);
            vtxD.OoW = 1 / _prjVerts[3].W;

            vtxA.TmuVtx0.SoW = _prjVerts[0].S * 255 * vtxA.OoW;
            vtxA.TmuVtx0.ToW = _prjVerts[0].T * 255 * vtxA.OoW;

            vtxB.TmuVtx0.SoW = _prjVerts[1].S * 255 * vtxB.OoW;
            vtxB.TmuVtx0.ToW = _prjVerts[1].T * 255 * vtxB.OoW;

            vtxC.TmuVtx0.SoW = _prjVerts[2].S * 255 * vtxC.OoW;
            vtxC.TmuVtx0.ToW = _prjVerts[2].T * 255 * vtxC.OoW;

            vtxD.TmuVtx0.SoW = _prjVerts[3].S * 255 * vtxD.OoW;
            vtxD.TmuVtx0.ToW = _prjVerts[3].T * 255 * vtxD.OoW;

            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxD);
            Gr.DrawTriangle(ref vtxA, ref vtxD, ref vtxC);

            if (_mipMapMode == MipMapMode.Trilinear)
            {
                Gr.AlphaBlendFunction(Blend.One, Blend.One, Blend.Zero, Blend.Zero);
                Gr.TexCombine(ChipID.Tmu0,
                    CombineFunction.BlendLocal, CombineFactor.OneMinusLodFraction,
                    CombineFunction.BlendLocal, CombineFactor.OneMinusLodFraction,
                    false, false);
                Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxD);
                Gr.DrawTriangle(ref vtxA, ref vtxD, ref vtxC);
                Gr.AlphaBlendFunction(Blend.One, Blend.Zero, Blend.Zero, Blend.Zero);
            }

            return true;
        }

        /// <inheritdoc/>
        protected override void Input(char c)
        {
            switch (c)
            {
                case 'm':
                case 'M':
                    _mipMapMode = (MipMapMode)((int)++_mipMapMode % 3);
                    break;
                default:
                    base.Input(c);
                    break;
            }
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        private enum MipMapMode
        {
            Disable,
            Nearest,
            Trilinear
        }
    }
}
