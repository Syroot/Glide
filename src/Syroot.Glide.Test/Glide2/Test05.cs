﻿using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test05.
    /// </summary>
    [Test("Renders two interpenetrating triangles with Z-buffering.")]
    public class Test05 : SimpleSdkTest
    {
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            // Set up render state - flat shading + Z-buffering.
            Gr.ColorCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Constant, CombineOther.None, false);
            Gr.DepthBufferMode(DepthBuffer.ZBuffer);
            Gr.DepthBufferFunction(Compare.Greater);
            Gr.DepthMask(true);

            ConOutput("Press a key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB = new Vertex();
            Vertex vtxC = new Vertex();
            Gr.BufferClear(0, 0, (ushort)ZDepthValue.Farthest);

            // Depth values should be scaled from reciprocated depth value, then scaled from 0 to 65535f.
            // OoZ = (1f / Z) * 65535 = 65535f / Z
            float zDist;

            SetPosition(ref vtxA, 0.25f, 0.21f);
            SetPosition(ref vtxB, 0.75f, 0.21f);
            SetPosition(ref vtxC, 0.5f, 0.79f);
            zDist = 10f;
            vtxA.OoZ = vtxB.OoZ = vtxC.OoZ = 65535 / zDist;
            Gr.ConstantColorValue(0x808080);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);

            SetPosition(ref vtxA, 0.86f, 0.21f);
            SetPosition(ref vtxB, 0.86f, 0.79f);
            SetPosition(ref vtxC, 0.14f, 0.5f);
            zDist = 12.5f;
            vtxA.OoZ = vtxB.OoZ = 65535 / zDist;
            zDist = 7.5f;
            vtxC.OoZ = 65535 / zDist;
            Gr.ConstantColorValue(0x00FF00);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);

            return true;
        }
    }
}
