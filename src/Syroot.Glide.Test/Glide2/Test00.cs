﻿using Syroot.Glide.Glide2;
using Syroot.Glide.Test.Glide2;

namespace Syroot.Glide.Test
{
    /// <summary>
    /// Represents the Glide 2 SDK Test00.
    /// </summary>
    [Test("Clears the screen to blue.")]
    public class Test00 : SimpleSdkTest
    {
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize() => ConOutput("Press a key to quit\n");

        /// <inheritdoc/>
        protected override bool Render()
        {
            Gr.BufferClear(0xFF0000, 0, (ushort)WDepthValue.Farthest);
            return true;
        }
    }
}
