﻿using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents a simplified <see cref="SdkTest"/> sharing additional logic to focus on actual test code.
    /// </summary>
    public abstract class SimpleSdkTest : SdkTest
    {
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override sealed void Run(string[] args)
        {
            if (!TestPrelude(args))
                return;

            ConSet(0, 0, 1, 0.5f, 60, 15, 0xFFFFFF);
            Initialize();

            while (_frames-- != 0 && OkToRender)
            {
                if (_hwConfig.Ssts[0].Type == SstType.Sst96)
                {
                    GetDimsByConst(_resolution, out _scrWidth, out _scrHeight);
                    Gr.ClipWindow(0, 0, (uint)_scrWidth, (uint)_scrHeight);
                }

                if (!Render())
                    break;
                ConRender();

                Gr.BufferSwap(1);
                TestScreenGrab();

                while (KbHit())
                    Input((char)_queue.Dequeue());
            }

            Gr.GlideShutdown();
        }

        /// <summary>
        /// Called when initial render state and resources have to be set up. The base method does nothing.
        /// </summary>
        protected virtual void Initialize() { }

        /// <summary>
        /// Called for every frame to render graphics, before rendering the console and flipping the buffers. The base
        /// method does nothing and always succeeds.
        /// </summary>
        /// <returns>Whether to continue rendering.</returns>
        protected virtual bool Render() => true;

        /// <summary>
        /// Called when input was detected. The base method exits the application.
        /// </summary>
        /// <param name="c">The input character.</param>
        protected virtual void Input(char c) => _frames = 0;
    }
}
