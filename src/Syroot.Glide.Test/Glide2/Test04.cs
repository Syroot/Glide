﻿using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test04.
    /// </summary>
    [Test("Draws a gouraud shaded triangle.")]
    public class Test04 : SimpleSdkTest
    {
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            // Set up render state - gouraud shading.
            Gr.ColorCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Iterated, CombineOther.None, false);

            ConOutput("Press a key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB = new Vertex();
            Vertex vtxC = new Vertex();
            Gr.BufferClear(0, 0, (ushort)WDepthValue.Farthest);

            SetPosition(ref vtxA, 0.3f, 0.3f);
            SetColor(ref vtxA, 255, 0, 0);

            SetPosition(ref vtxB, 0.8f, 0.4f);
            SetColor(ref vtxB, 0, 255, 0);

            SetPosition(ref vtxC, 0.5f, 0.8f);
            SetColor(ref vtxC, 0, 0, 255);

            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);
            return true;
        }
    }
}
