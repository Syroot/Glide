﻿using System;
using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test15.
    /// </summary>
    [Test("Clip rectangle testing - clip rectangle travels around screen.")]
    public class Test15 : SimpleSdkTest
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const float _clipSizeDelta = 0.005f;
        private const float _clipPosDelta = 0.01f;

        private const float _clipSizeMin = 0.05f;
        private const float _clipSizeMax = 0.6f;

        private const uint _gray = 0x808080;
        private const uint _black = 0x000000;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private float _clipX = 0.2f;
        private float _clipY = 0.5f;
        private float _clipSize = 0.3f;

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            // Set up render state.
            Gr.ColorCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Constant, CombineOther.None, false);

            ConOutput("+/- - grow/shrink clip rectangle\n");
            ConOutput("a/d - clip window left/right\n");
            ConOutput("w/s clip window up/down\n");
            ConOutput("Press any other key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB = new Vertex();
            Vertex vtxC = new Vertex();
            Gr.ClipWindow((uint)ScaleX(0), (uint)ScaleY(0), (uint)ScaleX(1), (uint)ScaleY(1));
            Gr.BufferClear(_black, 0, (ushort)ZDepthValue.Farthest);

            // Set clipping rectangle.
            uint minX = (uint)ScaleX(_clipX);
            uint minY = (uint)ScaleY(_clipY);
            uint maxX = (uint)ScaleX(Math.Min(1, _clipX + _clipSize));
            uint maxY = (uint)ScaleY(Math.Min(1, _clipY + _clipSize));
            Gr.ClipWindow(minX, minY, maxX, maxY);

            // Draw 10x10 grid of triangles.
            for (int y = 0; y < 10; y++)
            {
                for (int x = 0; x < 10; x++)
                {
                    // A
                    // |\
                    // B-C
                    vtxA.X = vtxB.X = ScaleX(x / 10f);
                    vtxA.Y = ScaleY(y / 10f);
                    vtxB.Y = vtxC.Y = ScaleY(y / 10f + 0.1f);
                    vtxC.X = ScaleX(x / 10f + 0.1f);
                    Gr.ConstantColorValue(_gray);
                    Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);
                }
            }

            return true;
        }

        /// <inheritdoc/>
        protected override void Input(char c)
        {
            switch (c)
            {
                case '+':
                    if (_clipSize < _clipSizeMax)
                        _clipSize += _clipSizeDelta;
                    break;
                case '-':
                    if (_clipSize > _clipSizeMin)
                        _clipSize -= _clipSizeDelta;
                    break;
                case 'a':
                case 'A':
                    if (_clipX > 0)
                        _clipX -= _clipPosDelta;
                    if (_clipX < 0)
                        _clipX = 0;
                    break;
                case 'd':
                case 'D':
                    if (_clipX < 1)
                        _clipX += _clipPosDelta;
                    break;
                case 'w':
                case 'W':
                    if (_clipY > 0)
                        _clipY -= _clipPosDelta;
                    if (_clipY < 0)
                        _clipY = 0;
                    break;
                case 's':
                case 'S':
                    if (_clipY < 1)
                        _clipY += _clipPosDelta;
                    break;
                default:
                    base.Input(c);
                    break;
            }
        }
    }
}
