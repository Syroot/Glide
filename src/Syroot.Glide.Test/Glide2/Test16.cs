﻿using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test16.
    /// </summary>
    [Test("Test grShamelessPlug and grSplash.")]
    public class Test16 : SimpleSdkTest
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const uint _blue = 0xFF0000;
        private const uint _red = 0x0000FF;
        private const uint _black = 0x000000;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _plug;
        private bool _render;
        private uint _frame = 1;

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            // Set up render state.
            Gr.ColorCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Constant, CombineOther.None, false);

            ConOutput("p - toggle shameless plug on/off\n");
            ConOutput("s - run the splash screen\n");
            ConOutput("r - render splash continuously\n");
            ConOutput("Press any other key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB = new Vertex();
            Vertex vtxC = new Vertex();
            Vertex vtxD = new Vertex();
            Gr.BufferClear(_black, 0, (ushort)ZDepthValue.Farthest);

            // Draw 10x10 grid of triangles.
            for (int y = 0; y < 10; y++)
            {
                for (int x = 0; x < 10; x++)
                {
                    // A-D
                    // |\|
                    // B-C
                    vtxA.X = vtxB.X = ScaleX(x / 10f);
                    vtxA.Y = vtxD.Y = ScaleY(y / 10f);
                    vtxB.Y = vtxC.Y = ScaleY(y / 10f + 1);
                    vtxC.X = vtxD.X = ScaleX(x / 10f + 1);

                    Gr.ConstantColorValue(_red);
                    Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);
                    Gr.ConstantColorValue(_blue);
                    Gr.DrawTriangle(ref vtxA, ref vtxC, ref vtxD);
                }
            }

            if (_render)
                Gr.Splash(ScaleX(0), ScaleY(0.79f), ScaleX(0.2f), ScaleY(0.2f), _frame++);

            return true;
        }

        /// <inheritdoc/>
        protected override void Input(char c)
        {
            switch (c)
            {
                case 'p':
                case 'P':
                    _plug = !_plug;
                    Gr.GlideShamelessPlug(_plug);
                    break;
                case 's':
                case 'S':
                    Gr.Splash(0, 0, _scrWidth, _scrHeight, 0);
                    break;
                case 'r':
                case 'R':
                    _render = !_render;
                    break;
                default:
                    base.Input(c);
                    break;
            }
        }
    }
}
