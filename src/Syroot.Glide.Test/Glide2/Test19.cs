﻿using System.Diagnostics;
using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test19.
    /// </summary>
    [Test("Texture filter modes test.")]
    public class Test19 : SimpleSdkTest
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Texture _texture = new Texture();
        private bool _minify;
        private bool _bilerp;

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            // Set up render state - decal texture - disable mipmapping.
            Gr.ColorCombine(CombineFunction.ScaleOther, CombineFactor.One, CombineLocal.Constant, CombineOther.Texture, false);
            Gr.TexCombine(ChipID.Tmu0, CombineFunction.Local, CombineFactor.None, CombineFunction.Local, CombineFactor.None, false, false);
            Gr.TexMipMapMode(ChipID.Tmu0, MipMap.Disable, false);

            // Load texture data into system RAM.
            Trace.Assert(LoadTexture("miro.3df", out _texture.Info, out _texture.TableType, out _texture.TableData));
            // Download texture data to TMU.
            Gr.TexDownloadMipMap(ChipID.Tmu0, Gr.TexMinAddress(ChipID.Tmu0), MipMapLevelMask.Both, ref _texture.Info);
            if (_texture.TableType != NoTable)
                Gr.TexDownloadTable(ChipID.Tmu0, _texture.TableType, ref _texture.TableData);

            // Select texture as source of all texturing opreations.
            Gr.TexSource(ChipID.Tmu0, Gr.TexMinAddress(ChipID.Tmu0), MipMapLevelMask.Both, ref _texture.Info);

            ConOutput("m - toggle magnify/minify texture\n");
            ConOutput("f - toggle pointSample/Bilinear\n");
            ConOutput("Press any other key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB;
            Vertex vtxC;
            Vertex vtxD;
            Gr.BufferClear(0, 0, (ushort)ZDepthValue.Farthest);

            // A-B
            // |\|
            // C-D
            vtxA.OoW = 1;
            vtxB = vtxC = vtxD = vtxA;

            if (_minify)
            {
                vtxA.X = vtxC.X = ScaleX(0);
                vtxB.X = vtxD.X = ScaleX(1);
                vtxA.Y = vtxB.Y = ScaleY(0);
                vtxC.Y = vtxD.Y = ScaleY(1);
            }
            else
            {
                vtxA.X = vtxC.X = ScaleX(0.45f);
                vtxB.X = vtxD.X = ScaleX(0.55f);
                vtxA.Y = vtxB.Y = ScaleY(0.45f);
                vtxC.Y = vtxD.Y = ScaleY(0.55f);
            }

            vtxA.TmuVtx0.SoW = vtxC.TmuVtx0.SoW = 0f;
            vtxB.TmuVtx0.SoW = vtxD.TmuVtx0.SoW = 255;
            vtxA.TmuVtx0.ToW = vtxB.TmuVtx0.ToW = 0;
            vtxC.TmuVtx0.ToW = vtxD.TmuVtx0.ToW = 255;

            if (_bilerp)
            {
                Gr.TexFilterMode(ChipID.Tmu0, TextureFilter.Bilinear, TextureFilter.Bilinear);
                ConOutput("GR_TEXTUREFILTER_BILINEAR     \r");
            }
            else
            {
                Gr.TexFilterMode(ChipID.Tmu0, TextureFilter.PointSampled, TextureFilter.PointSampled);
                ConOutput("GR_TEXTUREFILTER_POINT_SAMPLED\r");
            }

            Gr.DrawTriangle(ref vtxA, ref vtxD, ref vtxC);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxD);

            return true;
        }

        /// <inheritdoc/>
        protected override void Input(char c)
        {
            switch (c)
            {
                case 'm':
                case 'M':
                    _minify = !_minify;
                    break;
                case 'f':
                case 'F':
                    _bilerp = !_bilerp;
                    break;
                default:
                    base.Input(c);
                    break;
            }
        }
    }
}
