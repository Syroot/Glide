﻿using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test13.
    /// </summary>
    [Test("Iterated alpha test - blue triangle fades towards one vertex.")]
    public class Test13 : SimpleSdkTest
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const uint _red = 0x0000FF;
        private const uint _blue = 0xFF0000;

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            // Set up render state.
            Gr.ColorCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Constant, CombineOther.None, false);
            Gr.AlphaCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Iterated, CombineOther.None, false);
            Gr.AlphaBlendFunction(Blend.SrcAlpha, Blend.OneMinusSrcAlpha, Blend.Zero, Blend.Zero);

            ConOutput("Press any key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB = new Vertex();
            Vertex vtxC = new Vertex();
            Gr.BufferClear(0, 0, 0);

            SetPosition(ref vtxA, 0.5f, 0.1f);
            SetPosition(ref vtxB, 0.8f, 0.9f);
            SetPosition(ref vtxC, 0.2f, 0.9f);
            vtxA.A = vtxB.A = vtxC.A = 255;

            Gr.ConstantColorValue(_red);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);

            Gr.SstOrigin(Origin.LowerLeft);

            vtxA.A = 0;
            Gr.ConstantColorValue(_blue);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);

            Gr.SstOrigin(Origin.UpperLeft);

            return true;
        }
    }
}
