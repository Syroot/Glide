﻿using System.Diagnostics;
using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test18.
    /// </summary>
    [Test("Alpha texture test.")]
    public class Test18 : SimpleSdkTest
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const uint _red = 0x0000FF;
        private const uint _blue = 0xFF0000;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Texture _texture = new Texture();

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            // Set up render state - decal texture - alpha blend.
            Gr.TexCombine(ChipID.Tmu0, CombineFunction.Local, CombineFactor.None, CombineFunction.Local, CombineFactor.None, false, false);

            // Load texture data into system RAM.
            Trace.Assert(LoadTexture("alpha.3df", out _texture.Info, out _texture.TableType, out _texture.TableData));
            // Download texture data to TMU.
            Gr.TexDownloadMipMap(ChipID.Tmu0, Gr.TexMinAddress(ChipID.Tmu0), MipMapLevelMask.Both, ref _texture.Info);
            if (_texture.TableType != NoTable)
                Gr.TexDownloadTable(ChipID.Tmu0, _texture.TableType, ref _texture.TableData);

            // Select texture as source of all texturing operations.
            Gr.TexSource(ChipID.Tmu0, Gr.TexMinAddress(ChipID.Tmu0), MipMapLevelMask.Both, ref _texture.Info);

            // Enable bilinear filtering + mipmapping.
            Gr.TexFilterMode(ChipID.Tmu0, TextureFilter.Bilinear, TextureFilter.Bilinear);
            Gr.TexMipMapMode(ChipID.Tmu0, MipMap.Nearest, false);

            ConOutput("Press any key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB = new Vertex();
            Vertex vtxC = new Vertex();
            Vertex vtxD = new Vertex();
            Gr.BufferClear(0, 0, (ushort)ZDepthValue.Farthest);

            // Draw 10x10 grid of triangles.
            Gr.ColorCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Constant, CombineOther.None, false);
            Gr.AlphaCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Constant, CombineOther.None, false);
            Gr.AlphaBlendFunction(Blend.One, Blend.Zero, Blend.Zero, Blend.Zero);
            for (int y = 0; y < 10; y++)
            {
                for (int x = 0; x < 10; x++)
                {
                    // A-D
                    // |\|
                    // B-C
                    vtxA.X = vtxB.X = ScaleX(x / 10f);
                    vtxA.Y = vtxD.Y = ScaleY(y / 10f);
                    vtxB.Y = vtxC.Y = ScaleY(y / 10f + 0.1f);
                    vtxC.X = vtxD.X = ScaleX(x / 10f + 0.1f);

                    Gr.ConstantColorValue(_red);
                    Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);
                    Gr.ConstantColorValue(_blue);
                    Gr.DrawTriangle(ref vtxA, ref vtxC, ref vtxD);
                }
            }

            // A-B
            // |\|
            // C-D
            vtxA.OoW = 1;
            vtxB = vtxC = vtxD = vtxA;

            vtxA.X = vtxC.X = ScaleX(0.2f);
            vtxB.X = vtxD.X = ScaleX(0.8f);
            vtxA.Y = vtxB.Y = ScaleY(0.2f);
            vtxC.Y = vtxD.Y = ScaleY(0.8f);

            vtxA.TmuVtx0.SoW = vtxC.TmuVtx0.SoW = 0;
            vtxB.TmuVtx0.SoW = vtxD.TmuVtx0.SoW = 255;
            vtxA.TmuVtx0.ToW = vtxB.TmuVtx0.ToW = 0;
            vtxC.TmuVtx0.ToW = vtxD.TmuVtx0.ToW = 255;

            Gr.ColorCombine(CombineFunction.ScaleOther, CombineFactor.One, CombineLocal.None, CombineOther.Texture, false);
            Gr.AlphaCombine(CombineFunction.ScaleOther, CombineFactor.One, CombineLocal.None, CombineOther.Texture, false);
            Gr.AlphaBlendFunction(Blend.SrcAlpha, Blend.OneMinusSrcAlpha, Blend.Zero, Blend.Zero);

            Gr.DrawTriangle(ref vtxA, ref vtxD, ref vtxC);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxD);

            return true;
        }
    }
}
