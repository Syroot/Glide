﻿using System.Diagnostics;
using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test21.
    /// </summary>
    [Test("Texture compositing.")]
    public class Test21 : SimpleSdkTest
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const float _maxDist = 2.5f;
        private const float _minDist = 1;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Texture _baseTexture = new Texture();
        private uint _baseTextureAddr;
        private Texture _lightTexture = new Texture();
        private uint _lightTextureAddr;
        private Texture _detailTexture = new Texture();
        private uint _detailTextureAddr;
        private readonly Vertex3D[] _srcVerts = new Vertex3D[4];
        private float _distance;
        private float _dDelta;
        private Mode _mode;

        private readonly Vertex3D[] _xfVerts = new Vertex3D[4];
        private readonly Vertex3D[] _prjVerts = new Vertex3D[4];

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            Gr.SstOrigin(Origin.LowerLeft);

            // Set up render state - decal - bilinear - nearest mipmapping.
            Gr.ColorCombine(CombineFunction.ScaleOther, CombineFactor.One, CombineLocal.None, CombineOther.Texture, false);
            Gr.TexMipMapMode(ChipID.Tmu0, MipMap.Nearest, false);
            Gr.TexFilterMode(ChipID.Tmu0, TextureFilter.Bilinear, TextureFilter.Bilinear);

            // Load texture data into system RAM.
            Trace.Assert(LoadTexture("decal1.3df", out _baseTexture.Info, out _baseTexture.TableType, out _baseTexture.TableData));
            Trace.Assert(LoadTexture("light.3df", out _lightTexture.Info, out _lightTexture.TableType, out _lightTexture.TableData));
            Trace.Assert(LoadTexture("lava.3df", out _detailTexture.Info, out _detailTexture.TableType, out _detailTexture.TableData));

            // Download texture data to TMU.
            _baseTextureAddr = Gr.TexMinAddress(ChipID.Tmu0);
            Gr.TexDownloadMipMap(ChipID.Tmu0, _baseTextureAddr, MipMapLevelMask.Both, ref _baseTexture.Info);
            if (_baseTexture.TableType != NoTable)
                Gr.TexDownloadTable(ChipID.Tmu0, _baseTexture.TableType, ref _baseTexture.TableData);

            _lightTextureAddr = _baseTextureAddr + Gr.TexTextureMemRequired(MipMapLevelMask.Both, ref _baseTexture.Info);
            Gr.TexDownloadMipMap(ChipID.Tmu0, _lightTextureAddr, MipMapLevelMask.Both, ref _lightTexture.Info);
            if (_lightTexture.TableType != NoTable)
                Gr.TexDownloadTable(ChipID.Tmu0, _lightTexture.TableType, ref _lightTexture.TableData);

            _detailTextureAddr = _lightTextureAddr + Gr.TexTextureMemRequired(MipMapLevelMask.Both, ref _lightTexture.Info);
            Gr.TexDownloadMipMap(ChipID.Tmu0, _detailTextureAddr, MipMapLevelMask.Both, ref _detailTexture.Info);
            if (_detailTexture.TableType != NoTable)
                Gr.TexDownloadTable(ChipID.Tmu0, _detailTexture.TableType, ref _detailTexture.TableData);

            // Initialize source 3D data - rectangle on X/Z plane, centered about Y axis.
            // 0--1  Z+
            // |  |  |
            // 2--3   - X+
            _srcVerts[0] = new Vertex3D(-0.5f, 0, 0.5f, 1, 0, 0);
            _srcVerts[1] = new Vertex3D(0.5f, 0, 0.5f, 1, 1, 0);
            _srcVerts[2] = new Vertex3D(-0.5f, 0, -0.5f, 1, 0, 1);
            _srcVerts[3] = new Vertex3D(0.5f, 0, -0.5f, 1, 1, 1);

            _distance = 1;
            _dDelta = 0.01f;
            _mode = Mode.Lightmap;

            ConOutput("m - change texture compositing mode\n");
            ConOutput("Press any other key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB;
            Vertex vtxC;
            Vertex vtxD;
            Gr.BufferClear(0x404040, 0, (ushort)ZDepthValue.Farthest);

            // 3D transformations
            // A-B
            // |\|
            // C-D
            vtxA.OoW = 1;
            vtxB = vtxC = vtxD = vtxA;

            _distance += _dDelta;
            if (_distance > _maxDist || _distance < _minDist)
            {
                _dDelta *= -1f;
                _distance += _dDelta;
            }

            Matrix.RotationX(-20, out Matrix rotationMtx);
            Matrix.Translation(0, -0.3f, _distance, out Matrix distanceMtx);
            SetMatrix(Matrix.Identity * rotationMtx * distanceMtx);

            TransformVertices(_xfVerts, _srcVerts, _srcVerts.Length);
            ProjectVertices(_prjVerts, _xfVerts, _srcVerts.Length);

            SetPosition(ref vtxA, _prjVerts[0].X, _prjVerts[0].Y);
            vtxA.OoW = 1 / _prjVerts[0].W;

            SetPosition(ref vtxB, _prjVerts[1].X, _prjVerts[1].Y);
            vtxB.OoW = 1 / _prjVerts[1].W;

            SetPosition(ref vtxC, _prjVerts[2].X, _prjVerts[2].Y);
            vtxC.OoW = 1 / _prjVerts[2].W;

            SetPosition(ref vtxD, _prjVerts[3].X, _prjVerts[3].Y);
            vtxD.OoW = 1 / _prjVerts[3].W;

            vtxA.TmuVtx0.SoW = _prjVerts[0].S * 255 * vtxA.OoW;
            vtxA.TmuVtx0.ToW = _prjVerts[0].T * 255 * vtxA.OoW;

            vtxB.TmuVtx0.SoW = _prjVerts[1].S * 255 * vtxB.OoW;
            vtxB.TmuVtx0.ToW = _prjVerts[1].T * 255 * vtxB.OoW;

            vtxC.TmuVtx0.SoW = _prjVerts[2].S * 255 * vtxC.OoW;
            vtxC.TmuVtx0.ToW = _prjVerts[2].T * 255 * vtxC.OoW;

            vtxD.TmuVtx0.SoW = _prjVerts[3].S * 255 * vtxD.OoW;
            vtxD.TmuVtx0.ToW = _prjVerts[3].T * 255 * vtxD.OoW;

            // Render first pass.
            switch (_mode)
            {
                case Mode.Lightmap:
                case Mode.Specular:
                    Gr.TexCombine(ChipID.Tmu0,
                        CombineFunction.Local, CombineFactor.None,
                        CombineFunction.Local, CombineFactor.None,
                        false, false);
                    break;
                case Mode.Detail:
                    Gr.TexCombine(ChipID.Tmu0,
                         CombineFunction.BlendLocal, CombineFactor.DetailFactor,
                         CombineFunction.Local, CombineFactor.None,
                         false, false);
                    Gr.TexDetailControl(ChipID.Tmu0, 2, 7, 1);
                    break;
            }
            Gr.AlphaBlendFunction(Blend.One, Blend.Zero, Blend.One, Blend.Zero);
            Gr.TexSource(ChipID.Tmu0, _baseTextureAddr, MipMapLevelMask.Both, ref _baseTexture.Info);

            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxD);
            Gr.DrawTriangle(ref vtxA, ref vtxD, ref vtxC);

            // Render second pass.
            switch (_mode)
            {
                case Mode.Lightmap:
                    Gr.AlphaBlendFunction(Blend.DstColor, Blend.Zero, Blend.Zero, Blend.Zero);
                    Gr.TexSource(ChipID.Tmu0, _lightTextureAddr, MipMapLevelMask.Both, ref _lightTexture.Info);
                    ConOutput("LIGHTMAP - TEXTURE MODULATE  \r");
                    break;
                case Mode.Detail:
                    Gr.TexCombine(ChipID.Tmu0,
                        CombineFunction.BlendLocal, CombineFactor.OneMinusDetailFactor,
                        CombineFunction.Local, CombineFactor.None,
                        false, false);
                    Gr.AlphaBlendFunction(Blend.One, Blend.One, Blend.Zero, Blend.Zero);
                    Gr.TexSource(ChipID.Tmu0, _detailTextureAddr, MipMapLevelMask.Both, ref _detailTexture.Info);
                    ConOutput("DETAIL   - BLEND ON LOD      \r");
                    break;
                case Mode.Specular:
                    Gr.AlphaBlendFunction(Blend.One, Blend.One, Blend.Zero, Blend.Zero);
                    Gr.TexSource(ChipID.Tmu0, _lightTextureAddr, MipMapLevelMask.Both, ref _lightTexture.Info);
                    ConOutput("SPECULAR - TEXTURE ACCUMULATE\r");
                    break;
            }

            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxD);
            Gr.DrawTriangle(ref vtxA, ref vtxD, ref vtxC);

            return true;
        }

        /// <inheritdoc/>
        protected override void Input(char c)
        {
            switch (c)
            {
                case 'm':
                case 'M':
                    _mode = (Mode)((int)++_mode % 3);
                    break;
                default:
                    base.Input(c);
                    break;
            }
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        private enum Mode
        {
            Lightmap,
            Specular,
            Detail
        }
    }
}
