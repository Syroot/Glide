﻿using Syroot.Glide.Glide2;
using Syroot.Glide.Glide2.Utilities;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test08.
    /// </summary>
    [Test("Tests fogging.")]
    public class Test08 : SimpleSdkTest
    {
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            byte[] fogTable = new byte[Gr.FogTableSize];

            // Set up render state - gouraud shading + fog.
            Gr.ColorCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Iterated, CombineOther.None, false);
            Gr.FogMode(Fog.WithTable);
            Gr.FogColorValue(0xFF00FF00);
            Gu.FogGenerateExp(fogTable, 0.01f);
            Gr.FogTable(fogTable);

            ConOutput("Press a key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB = new Vertex();
            Vertex vtxC = new Vertex();
            Gr.BufferClear(0, 0, 0);

            float wDist;

            SetPosition(ref vtxA, 0, 0);
            SetColor(ref vtxA, 0, 0, 64);
            wDist = 20;
            vtxA.OoW = 1 / wDist;

            SetPosition(ref vtxB, 0.016f, 1);
            SetColor(ref vtxB, 0, 0, 128);
            wDist = 2000;
            vtxB.OoW = 1 / wDist;

            SetPosition(ref vtxC, 1, 0.0208f);
            SetColor(ref vtxC, 0, 0, 64);
            wDist = 20;
            vtxC.OoW = 1 / wDist;

            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);
            return true;
        }
    }
}
