﻿using System;
using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test
{
    /// <summary>
    /// Represents a test application printing hardware information.
    /// </summary>
    [Test("Writes hardware information to the console.")]
    public class PrintHwInfo : ITest
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public bool Execute(string[] args)
        {
            HwConfiguration hwConfig = new HwConfiguration();
            if (!Gr.SstQueryBoards(ref hwConfig))
                throw new InvalidOperationException("Could not detect Glide hardware.");
            Console.WriteLine($"Found {hwConfig.NumSst} boards.");

            if (!Gr.SstQueryHardware(ref hwConfig))
                throw new InvalidOperationException("Could not query Glide hardware info.");
            for (int i = 0; i < hwConfig.NumSst; i++)
            {
                SstConfig sst = hwConfig.Ssts[i];
                Console.WriteLine();
                Console.Write($"{sst.Type}: ");
                switch (sst.Type)
                {
                    case SstType.Voodoo:
                    case SstType.Voodoo2:
                        VoodooConfig voodoo = sst.VoodooConfig;
                        string sli = voodoo.SliDetect ? "SLI, " : String.Empty;
                        Console.WriteLine($"{voodoo.FbRam} MB RAM, Rev {voodoo.FbIRev}, {sli}{voodoo.NTexelFx} TMUs");
                        for (int j = 0; j < voodoo.NTexelFx; j++)
                        {
                            TmuConfig tmu = voodoo.TmuConfig[j];
                            Console.WriteLine($"  TMU{j}: {tmu.TmuRam} MB RAM, Rev {tmu.TmuRev}");
                        }
                        break;
                    case SstType.Sst96:
                        Sst96Config sst96 = sst.Sst96Config;
                        Console.WriteLine($"{sst96.FbRam} MB RAM, {sst96.NTexelFx} TMUs");
                        Console.WriteLine($"  TMU0: {sst96.TmuConfig.TmuRam} MB RAM, Rev {sst96.TmuConfig.TmuRev}");
                        break;
                    case SstType.AT3D:
                        AT3DConfig at3D = sst.AT3DConfig;
                        Console.WriteLine($"Rev {at3D.Rev}");
                        break;
                }
                Console.WriteLine();
            }

            return true;
        }
    }
}
