﻿using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test07.
    /// </summary>
    [Test("Tests alpha blending.")]
    public class Test07 : SimpleSdkTest
    {
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            // Set up render state - flat shading + alpha blend on constant alpha.
            Gr.ColorCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Constant, CombineOther.None, false);
            Gr.AlphaCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Constant, CombineOther.None, false);
            Gr.AlphaBlendFunction(Blend.SrcAlpha, Blend.OneMinusSrcAlpha, Blend.Zero, Blend.Zero);

            ConOutput("Press a key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB = new Vertex();
            Vertex vtxC = new Vertex();
            Gr.BufferClear(0, 0, 0);

            SetPosition(ref vtxA, 0.25f, 0.21f);
            SetPosition(ref vtxB, 0.75f, 0.21f);
            SetPosition(ref vtxC, 0.5f, 0.79f);
            Gr.ConstantColorValue(0xFF0000FF);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);

            SetPosition(ref vtxA, 0.86f, 0.21f);
            SetPosition(ref vtxB, 0.86f, 0.79f);
            SetPosition(ref vtxC, 0.14f, 0.5f);
            Gr.ConstantColorValue(0x80FF0000);
            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxC);

            return true;
        }
    }
}
