﻿using System.Diagnostics;
using Syroot.Glide.Glide2;
using Syroot.Glide.Glide2.Utilities;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test22.
    /// </summary>
    [Test("Fog with multi-pass texturing.")]
    public class Test22 : SimpleSdkTest
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const float _maxDist = 10;
        private const float _minDist = 1;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Texture _baseTexture = new Texture();
        private uint _baseTextureAddr;
        private Texture _lightTexture = new Texture();
        private uint _lightTextureAddr;
        private readonly byte[] _fogTable = new byte[Gr.FogTableSize];
        private readonly Vertex3D[] _srcVerts = new Vertex3D[4];
        private float _distance;
        private float _dDelta;

        private readonly Vertex3D[] _xfVerts = new Vertex3D[4];
        private readonly Vertex3D[] _prjVerts = new Vertex3D[4];

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            Gr.SstOrigin(Origin.LowerLeft);

            // Set up render state - decal - bilinear - nearest mipmapping - fogging.
            Gr.ColorCombine(CombineFunction.ScaleOther, CombineFactor.One, CombineLocal.None, CombineOther.Texture, false);
            Gr.TexMipMapMode(ChipID.Tmu0, MipMap.Nearest, false);
            Gr.TexFilterMode(ChipID.Tmu0, TextureFilter.Bilinear, TextureFilter.Bilinear);
            Gr.FogColorValue(0x404040);
            Gu.FogGenerateExp(_fogTable, 0.2f);
            Gr.FogTable(_fogTable);

            // Load texture data into system RAM.
            Trace.Assert(LoadTexture("decal1.3df", out _baseTexture.Info, out _baseTexture.TableType, out _baseTexture.TableData));
            Trace.Assert(LoadTexture("light.3df", out _lightTexture.Info, out _lightTexture.TableType, out _lightTexture.TableData));

            // Download texture data to TMU.
            _baseTextureAddr = Gr.TexMinAddress(ChipID.Tmu0);
            Gr.TexDownloadMipMap(ChipID.Tmu0, _baseTextureAddr, MipMapLevelMask.Both, ref _baseTexture.Info);
            if (_baseTexture.TableType != NoTable)
                Gr.TexDownloadTable(ChipID.Tmu0, _baseTexture.TableType, ref _baseTexture.TableData);

            _lightTextureAddr = _baseTextureAddr + Gr.TexTextureMemRequired(MipMapLevelMask.Both, ref _baseTexture.Info);
            Gr.TexDownloadMipMap(ChipID.Tmu0, _lightTextureAddr, MipMapLevelMask.Both, ref _lightTexture.Info);
            if (_lightTexture.TableType != NoTable)
                Gr.TexDownloadTable(ChipID.Tmu0, _lightTexture.TableType, ref _lightTexture.TableData);

            // Initialize source 3D data - rectangle on X/Z plane, centered about Y axis.
            // 0--1  Z+
            // |  |  |
            // 2--3   - X+
            _srcVerts[0] = new Vertex3D(-0.5f, 0, 0.5f, 1, 0, 0);
            _srcVerts[1] = new Vertex3D(0.5f, 0, 0.5f, 1, 1, 0);
            _srcVerts[2] = new Vertex3D(-0.5f, 0, -0.5f, 1, 0, 1);
            _srcVerts[3] = new Vertex3D(0.5f, 0, -0.5f, 1, 1, 1);

            _distance = 1;
            _dDelta = 0.05f;

            ConOutput("Press any other key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB;
            Vertex vtxC;
            Vertex vtxD;
            Gr.BufferClear(0x404040, 0, (ushort)ZDepthValue.Farthest);

            // 3D transformations
            // A-B
            // |\|
            // C-D
            vtxA.OoW = 1;
            vtxB = vtxC = vtxD = vtxA;

            _distance += _dDelta;
            if (_distance > _maxDist || _distance < _minDist)
            {
                _dDelta *= -1f;
                _distance += _dDelta;
            }

            Matrix.RotationX(-90, out Matrix rotationMtx);
            Matrix.Translation(0, -0.3f, _distance, out Matrix distanceMtx);
            SetMatrix(Matrix.Identity * rotationMtx * distanceMtx);

            TransformVertices(_xfVerts, _srcVerts, _srcVerts.Length);
            ProjectVertices(_prjVerts, _xfVerts, _srcVerts.Length);

            SetPosition(ref vtxA, _prjVerts[0].X, _prjVerts[0].Y);
            vtxA.OoW = 1 / _prjVerts[0].W;

            SetPosition(ref vtxB, _prjVerts[1].X, _prjVerts[1].Y);
            vtxB.OoW = 1 / _prjVerts[1].W;

            SetPosition(ref vtxC, _prjVerts[2].X, _prjVerts[2].Y);
            vtxC.OoW = 1 / _prjVerts[2].W;

            SetPosition(ref vtxD, _prjVerts[3].X, _prjVerts[3].Y);
            vtxD.OoW = 1 / _prjVerts[3].W;

            vtxA.TmuVtx0.SoW = _prjVerts[0].S * 255 * vtxA.OoW;
            vtxA.TmuVtx0.ToW = _prjVerts[0].T * 255 * vtxA.OoW;

            vtxB.TmuVtx0.SoW = _prjVerts[1].S * 255 * vtxB.OoW;
            vtxB.TmuVtx0.ToW = _prjVerts[1].T * 255 * vtxB.OoW;

            vtxC.TmuVtx0.SoW = _prjVerts[2].S * 255 * vtxC.OoW;
            vtxC.TmuVtx0.ToW = _prjVerts[2].T * 255 * vtxC.OoW;

            vtxD.TmuVtx0.SoW = _prjVerts[3].S * 255 * vtxD.OoW;
            vtxD.TmuVtx0.ToW = _prjVerts[3].T * 255 * vtxD.OoW;

            // Render first pass.
            Gr.TexCombine(ChipID.Tmu0,
                CombineFunction.Local, CombineFactor.None,
                CombineFunction.Local, CombineFactor.None,
                false, false);
            Gr.AlphaBlendFunction(Blend.One, Blend.Zero, Blend.One, Blend.Zero);
            Gr.TexSource(ChipID.Tmu0, _baseTextureAddr, MipMapLevelMask.Both, ref _baseTexture.Info);

            Gr.FogMode(Fog.Add2 | Fog.WithTable);

            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxD);
            Gr.DrawTriangle(ref vtxA, ref vtxD, ref vtxC);

            // Render second pass.
            Gr.AlphaBlendFunction(Blend.One, Blend.PreFogColor, Blend.Zero, Blend.Zero);
            Gr.TexSource(ChipID.Tmu0, _lightTextureAddr, MipMapLevelMask.Both, ref _lightTexture.Info);

            Gr.FogMode(Fog.Mult2 | Fog.WithTable);

            Gr.DrawTriangle(ref vtxA, ref vtxB, ref vtxD);
            Gr.DrawTriangle(ref vtxA, ref vtxD, ref vtxC);

            return true;
        }
    }
}
