﻿using Syroot.Glide.Glide2;

namespace Syroot.Glide.Test.Glide2
{
    /// <summary>
    /// Represents the Glide 2 SDK Test03.
    /// </summary>
    [Test("Draws a gouraud shaded line.")]
    public class Test03 : SimpleSdkTest
    {
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Initialize()
        {
            // Set up render state - gouraud shading.
            Gr.ColorCombine(CombineFunction.Local, CombineFactor.None, CombineLocal.Iterated, CombineOther.None, false);

            ConOutput("Press a key to quit\n");
        }

        /// <inheritdoc/>
        protected override bool Render()
        {
            Vertex vtxA = new Vertex();
            Vertex vtxB = new Vertex();
            Gr.BufferClear(0, 0, (ushort)WDepthValue.Farthest);

            SetPosition(ref vtxA, 0, 0);
            SetColor(ref vtxA, 255, 0, 0);

            SetPosition(ref vtxB, 1, 1);
            SetColor(ref vtxB, 0, 255, 0);

            Gr.DrawLine(ref vtxA, ref vtxB);
            return true;
        }
    }
}
