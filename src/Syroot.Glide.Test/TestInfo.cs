﻿using System;

namespace Syroot.Glide.Test
{
    /// <summary>
    /// Represents cached information on a test.
    /// </summary>
    internal class TestInfo
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        /// <summary><see cref="System.Type"/> of the class to instantiate for the test.</summary>
        internal readonly Type Type;
        /// <summary>Description of the test.</summary>
        internal readonly string Purpose;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="TestInfo"/> class with the given information.
        /// </summary>
        /// <param name="type"><see cref="System.Type"/> of the class to instantiate for the test.</param>
        /// <param name="purpose">Description of the test.</param>
        internal TestInfo(Type type, string purpose)
        {
            Type = type;
            Purpose = purpose;
        }
    }
}
