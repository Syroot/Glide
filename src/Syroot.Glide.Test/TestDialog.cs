﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Syroot.Glide.Test
{
    /// <summary>
    /// Represents the main window of the application.
    /// </summary>
    public partial class TestDialog : Form
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="TestDialog"/> class.
        /// </summary>
        public TestDialog() => InitializeComponent();

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the <see cref="TestInfo"/> of the test which was selected.
        /// </summary>
        internal TestInfo? SelectedTest { get; private set; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Sets the list of <see cref="TestInfo"/> which will be visualized in the list.
        /// </summary>
        /// <param name="tests">The tests which will be visualized in the list.</param>
        internal void SetAvailableTests(IList<TestInfo> tests)
        {
            _lvTests.BeginUpdate();
            for (int i = 0; i < tests.Count; i++)
            {
                TestInfo test = tests[i];
                ListViewItem item = new ListViewItem(test.Type.Name);
                item.Selected = i == 0;
                item.SubItems.Add(test.Purpose);
                item.Tag = test;
                _lvTests.Items.Add(item);
            }
            _lvTests.EndUpdate();
            LvTests_SelectedIndexChanged(this, EventArgs.Empty);
        }

        /// <summary>
        /// Sets the command line which is displayed at the top.
        /// </summary>
        internal void SetCommandLine(string commandLine)
        {
            if (String.IsNullOrEmpty(commandLine))
            {
                _lbCommandLine.Visible = false;
            }
            else
            {
                _lbCommandLine.Text = $"Command Line: {commandLine}";
                _lbCommandLine.Visible = true;
            }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void OnClientSizeChanged(EventArgs e)
        {
            ResizeListColumns();
            base.OnClientSizeChanged(e);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void LvTests_DoubleClick(object sender, EventArgs e)
        {
            if (SelectedTest != null)
                DialogResult = DialogResult.OK;
        }

        private void LvTests_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedTest = _lvTests.SelectedItems.Count == 1 ? (TestInfo)_lvTests.SelectedItems[0].Tag : null;
            _btOK.Enabled = SelectedTest != null;
        }

        private void LvTests_ClientSizeChanged(object sender, EventArgs e) => ResizeListColumns();

        private void ResizeListColumns() => _colPurpose.Width = _lvTests.ClientSize.Width - _colName.Width;
    }
}
