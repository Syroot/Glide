﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Syroot.Glide.Test
{
    internal interface ITest
    {
        // ---- METHODS ------------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes and executes the test application code.
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        /// <returns>Whether the application ended successfully.</returns>
        bool Execute(string[] args);
    }
}
