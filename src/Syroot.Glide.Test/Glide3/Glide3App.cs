﻿using System;
using System.Windows.Forms;
using Syroot.Glide.Glide3;

namespace Syroot.Glide.Test
{
    internal class Glide3App
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Run()
        {
            // Get hardware info.
            Console.WriteLine($"Found {Gr.Get(ParamName.NumBoards)[0]} boards");
            Gr.GlideInit();
            Gr.SstSelect(0);

            // Display available resolutions.
            ScreenResolution[] modes = Gr.QueryResolutions(new ScreenResolution
            {
                Resolution = Resolution.None,
                Refresh = Refresh.None,
                NumColorBuffers = -1,
                NumAuxBuffers = -1
            });
            Console.WriteLine($"Found {modes.Length} video modes");
            if (modes.Length > 0)
            {
                for (int i = 0; i < modes.Length; i++)
                {
                    Console.Write($"\t{i} {modes[i].Resolution} {modes[i].Refresh}, ");
                    Console.WriteLine($"{modes[i].NumColorBuffers} color buffers, {modes[i].NumAuxBuffers} aux buffers");
                }
                Console.WriteLine("Press any key to call SstWinOpen with the first resolution from above.");
                Console.ReadLine();

                // Create a context.
                Form form = new Form();
                form.Show();
                uint context = Gr.SstWinOpen((uint)form.Handle,
                    modes[0].Resolution, modes[0].Refresh,
                    ColorFormat.Rgba, Origin.UpperLeft,
                    modes[0].NumColorBuffers, modes[0].NumAuxBuffers);
                if (context != 0)
                    Gr.SstWinClose(context);
            }

            Gr.GlideShutdown();
        }
    }
}
