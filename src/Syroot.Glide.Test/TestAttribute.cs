﻿using System;

namespace Syroot.Glide.Test
{
    /// <summary>
    /// Represents an <see cref="Attribute"/> decorating classes which can be run as tests.
    /// </summary>
    public class TestAttribute : Attribute
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="TestAttribute"/> class with the given
        /// <paramref name="purpose"/>.
        /// </summary>
        /// <param name="purpose">A description of what the test does.</param>
        public TestAttribute(string purpose) => Purpose = purpose;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the description of what the test does.
        /// </summary>
        public string Purpose { get; set; }
    }
}
